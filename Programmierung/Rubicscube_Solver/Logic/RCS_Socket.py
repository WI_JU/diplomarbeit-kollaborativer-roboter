import socket
from time import sleep

import numpy as np

import Utilities.Utilits
from Camera.CameraMain import CameraPrediction
from Logic.RubicscubeLogicalSolver import RubicscubeSolverLogic
from Utilities.GenerateRandom import GenerateRandom
from Utilities.SolvedArrays import solvedArrays


class Communication:
    _clientSocket = socket.socket()
    _ipAddress = "192.168.125.1"
    _port = 5516
    _virtualIPAddress = '127.0.0.1'
    _virtualPort = 55000
    # Classes
    _cameraPrediction = CameraPrediction()
    _rubicscubeSolverLogic = RubicscubeSolverLogic()

    # keep track of connection status
    _connected = False
    _handleCases = ["GetSolvedCube", "CloseConnection", "CameraPosition", "NextStep", "PreScrambled"]
    _VALID_CASE = "VALID"
    _INVALID_CASE = "INVALID"
    _FINISHED_SEQUENCE = "FINISH"
    communicationState = {
        'state': 1,
        'stepIndex': 0,
        'solvedArray': [],
        'virtualCopy': solvedArrays().getSolved(),
        'isRunning': False,
        'validate': 0,
    }
    # Camera-handling
    _FRAME = []
    PreScrambleActivation = True
    PreScrambles = [
        [[['G', 'R', 'R'], ['W', 'G', 'O'], ['W', 'G', 'G']],
         [['O', 'O', 'O'], ['B', 'W', 'G'], ['Y', 'W', 'W']],
         [['G', 'B', 'G'], ['B', 'R', 'Y'], ['O', 'R', 'B']],
         [['W', 'G', 'O'], ['W', 'Y', 'O'], ['B', 'R', 'R']],
         [['R', 'O', 'B'], ['B', 'B', 'G'], ['W', 'W', 'B']],
         [['Y', 'Y', 'Y'], ['Y', 'O', 'R'], ['Y', 'Y', 'R']]]
        ,
        [[['G', 'G', 'R'], ['G', 'G', 'W'], ['Y', 'Y', 'R']],
         [['R', 'G', 'G'], ['R', 'W', 'R'], ['B', 'B', 'B']],
         [['Y', 'Y', 'Y'], ['R', 'R', 'R'], ['Y', 'Y', 'B']],
         [['G', 'G', 'O'], ['O', 'Y', 'O'], ['O', 'B', 'B']],
         [['O', 'B', 'W'], ['Y', 'B', 'W'], ['W', 'B', 'R']],
         [['O', 'O', 'W'], ['O', 'O', 'W'], ['W', 'W', 'G']]]
        ,
        [[['Y', 'Y', 'Y'], ['R', 'G', 'G'], ['Y', 'O', 'W']],
         [['G', 'G', 'W'], ['G', 'W', 'O'], ['G', 'W', 'O']],
         [['R', 'R', 'G'], ['W', 'R', 'W'], ['O', 'O', 'G']],
         [['B', 'B', 'R'], ['Y', 'Y', 'Y'], ['R', 'G', 'Y']],
         [['W', 'W', 'R'], ['B', 'B', 'R'], ['W', 'B', 'B']],
         [['B', 'O', 'O'], ['B', 'O', 'Y'], ['O', 'R', 'B']]]
        ,
        [[['W', 'R', 'G'], ['W', 'G', 'G'], ['B', 'Y', 'Y']],
         [['B', 'W', 'B'], ['G', 'W', 'B'], ['O', 'W', 'Y']],
         [['W', 'B', 'R'], ['O', 'R', 'R'], ['O', 'B', 'W']],
         [['G', 'Y', 'G'], ['G', 'Y', 'B'], ['O', 'O', 'W']],
         [['G', 'R', 'R'], ['Y', 'B', 'W'], ['R', 'O', 'B']],

         [['Y', 'G', 'O'], ['R', 'O', 'O'], ['R', 'Y', 'Y']]]
        ,
        [[['O', 'R', 'R'], ['B', 'G', 'Y'], ['Y', 'Y', 'G']],
         [['B', 'W', 'W'], ['B', 'W', 'W'], ['B', 'O', 'O']],
         [['Y', 'W', 'W'], ['O', 'R', 'R'], ['O', 'G', 'Y']],
         [['W', 'R', 'G'], ['G', 'Y', 'G'], ['G', 'Y', 'G']],
         [['B', 'R', 'R'], ['Y', 'B', 'G'], ['O', 'W', 'W']],
         [['B', 'B', 'Y'], ['O', 'O', 'O'], ['R', 'B', 'R']]]
    ]
    WUERFEL = 1

    def communicate(self, frame=[], DEBUG=False):
        """
        Starts the communication with the Roboter if there is a connection
        :param frame: the current Frame of the camera
        :param DEBUG: switches the IP and Port true == simulation
        :return:
        """
        self._FRAME = frame
        if DEBUG:
            self._ipAddress = self._virtualIPAddress
            self._port = self._virtualPort
        try:
            msg = self._clientSocket.recv(1024).decode("UTF-8")
            if msg != "":
                self._inputHandler(msg)
        except socket.error:
            # set connection status and recreate socket
            self._connected = False
            self._clientSocket = socket.socket()
            # print("connection lost... reconnecting")
            # while not self._connected:
            # attempt to reconnect, otherwise sleep for 2 seconds
            try:
                self._clientSocket.connect((self._ipAddress, self._port))
                self._connected = True
                # print("re-connection successful")
                self.communicationState['isRunning'] = True
            except socket.error:
                self.communicationState['isRunning'] = False
        return self.communicationState

    def _inputHandler(self, inputMessage):
        """
        Handles all InputMessages from the socket
        :param inputMessage: string message from socket
        :return:
        """
        # print(f"Received: {inputMessage}")
        if inputMessage == self._handleCases[0]:
            self.__solveHandler()
            self.communicationState['state'] = 0
        elif inputMessage == self._handleCases[1]:
            self._clientSend(self._VALID_CASE)
            self._clientSocket.close()
            self._clientSocket.close()
            self.communicationState['state'] = 1
        elif inputMessage == self._handleCases[3]:
            self.communicationState['state'] = 3
            self.communicationState['stepIndex'] = self._currentStepIndex
            self.communicationState['solvedArray'] = self._solvedArray
            if self._activateSolvedStates and self._currentStepIndex < len(self._solvedArray):
                self._clientSend(str(self._solvedArray[self._currentStepIndex]))
                self._currentStepIndex += 1
            elif self._activateSolvedStates and self._currentStepIndex >= len(self._solvedArray):
                self._clientSend(self._FINISHED_SEQUENCE)
                self.communicationState['state'] = 5
                self.communicationState['validate'] = 0
        else:
            inputMessage = inputMessage.split(",")
            useCase = inputMessage[0]
            if useCase == self._handleCases[2]:
                self.communicationState['state'] = 2
                self._cameraHandler(inputMessage)
            elif useCase == self._handleCases[4]:
                self.communicationState['state'] = 4
                self.PreScrambled = self.PreScrambles[inputMessage[1]]
                self.PreScrambleActivation = True
            else:
                # print("Unknown Case")
                pass

    def _cameraHandler(self, inputMessage):
        """
        Start taking pictures from current frame, response with Valid or Invalid
        :param inputMessage: string HandelCase[2]
        :return:
        """
        number = int(inputMessage[1])
        try:
            invalid = self._cameraPrediction.takePicture(number, self._FRAME)
        except:
            invalid = True
        if not invalid:
            self._clientSend(self._VALID_CASE)
        else:
            self._clientSend(self._INVALID_CASE)

    def __solveHandler(self):
        """
        From all Pictures generates imageArray and then solve the virtual cube
        :return:
        """
        if self.PreScrambleActivation:
            cameraArray = self.PreScrambles[self.WUERFEL - 1]
        else:
            # cameraArray = self._cameraPrediction.generatePictureArray()
            cameraArray = GenerateRandom().generateNewCube().copy()
            # cameraArray = np.rot90(self.PreScrambles[1], 0).copy()
        self.communicationState['virtualCopy'] = cameraArray.copy()
        if Utilities.Utilits.findMissingStones(cameraArray.copy()) == []:
            self.communicationState['validate'] = 2
            self.executeSovleHandler(cameraArray.copy())
        else:
            self.communicationState['validate'] = 1

    def executeSovleHandler(self, cameraArray):
        # print("Start Solve Handler")
        self._clientSend(self._VALID_CASE)
        if self.WUERFEL == 1:
            # Würfel 1
            steps = ['U', 'Li', 'U', 'B', 'B', 'U', 'Ri', 'Li', 'B', 'F']
        elif self.WUERFEL == 2:
            # Würfel 2
            steps = ['F', 'L', 'Fi', 'F', 'B', 'F', 'F', 'F', 'F', 'Ri']
        elif self.WUERFEL == 3:
            # Würfel 3
            steps = ['Di', 'R', 'F', 'Di', 'Di', 'D', 'Fi', 'Li', 'B', 'D']
        elif self.WUERFEL == 4:
            # Würfel 4
            steps = ['D', 'L', 'U', 'F', 'B', 'R', 'Ri', 'U', 'L', 'U']
        elif self.WUERFEL == 5:
            # Würfel 5
            steps = ['Ui', 'Di', 'F', 'Ui', 'Ui', 'Bi', 'Li', 'Bi']
        else:
            steps = self._rubicscubeSolverLogic.generateSolvingSteps(cameraArray)
        if steps != False:
            self._solvedArray = steps
            self._activateSolvedStates = True
            self._currentStepIndex = 0
            self._clientSend(self._VALID_CASE)
        else:
            self._activateSolvedStates = False
            self._clientSend(self._INVALID_CASE)

    def _clientSend(self, sendMessage):
        """
        Sends message to the socket
        :param sendMessage: strinsg message
        :return:
        """
        # print(f"Sent: {sendMessage}")
        sleep(0.05)
        self._clientSocket.send(bytes(sendMessage, "UTF-8"))
