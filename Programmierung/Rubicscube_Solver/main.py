import numpy as np

from GUI.GUI import startGui
import subprocess

# Start UserInterface
from Logic.RubicscubeLogicalSolver import RubicscubeSolverLogic

try:
    p = subprocess.Popen(['onInit.cmd'])
except:
    pass

startGui(False)
