import numpy as np

import Utilities.UseFunctions
from Utilities.Positions import Positions
import itertools
from copy import deepcopy


class Edge:
    def __init__(self, piece, Layer, position, Orientation1):
        self.piece = piece
        self.positionLayer = Layer
        self.position = position
        self.Orientation1Layer = Orientation1[0]
        self.Orientation1 = [Orientation1[1], Orientation1[2]]


class WhiteCross:
    def __init__(self, UF: Utilities.UseFunctions.UseFunctions, superMinimum=5):
        self._UF = UF
        self._position = Positions()
        self._PositionZero = ['WG', 'WO', 'WB', 'WR']
        self._whitePositions = [[0, 1], [1, 2], [2, 1], [1, 0]]
        self._superMinimum = superMinimum
        self._length = 0
        self._messages = ""

    def getLen(self):
        """
        adds up all moves
        :return: int of moves
        """
        return self._length

    def getMessages(self):
        """
        the messages of the solver
        :return: string of messages
        """
        return self._messages

    def canSolveCube(self):
        """
        checks if can solve virtual cube
        :return: boolean can/cannot solve
        """
        steps = self.__generateStepsToSolve()
        if not steps:
            return False
        else:
            self._length += len(steps)
            return True

    def getSteps(self):
        """
        solves Cube and return steps to solve
        :return: string[] of steps
        """
        steps = self.__generateStepsToSolve()
        return steps

    def getSolvedCube(self):
        """
        gives a copy of partly solved cube
        :return: string[] of virtual cube
        """
        return self._UF.getCube()

    def __generateStepsToSolve(self):
        """
        solves the cube of permutation of Stones
        :return: string[] of steps
        """
        minLenght = 100000
        solveSteps = []
        bevoreCube = deepcopy(self._UF.getCube())
        permutationObject = list(itertools.permutations(self._PositionZero))
        for perm in permutationObject:
            self._messages = ""
            self._UF.setCube(bevoreCube.copy())
            steps = self.__solveStepsByPieceOrder(perm)
            if 1 < len(steps) < minLenght:
                minLenght = len(steps)
                if minLenght < self._superMinimum:
                    return steps
                solveSteps = steps.copy()
        self._UF.setCube(bevoreCube)
        self._UF.turnCubeByArray(solveSteps)
        return solveSteps

    def __solveStepsByPieceOrder(self, orderOfPieces):
        """
        solves cube by order of stone positions
        :param orderOfPieces: string[] of Positions
        :return: string[] of steps
        """
        self._messages = str(orderOfPieces)
        allsteps = []
        for stone in orderOfPieces:
            steps = []
            self._stone = stone
            self._messages += str('\n') + str(stone) + " "
            slPositions = self._position.searchSLPosition(stone, self._UF.getCube())
            self._messages += str(slPositions)
            self._edge = Edge(slPositions[0], slPositions[1], slPositions[2], slPositions[3])
            slaveColor = self._edge.piece[:][1]

            if self._edge.positionLayer == 1:
                self._messages += str('\nCorrect Layer')
                steps.extend(self.__solveCorrectLayer(self.__getFinishSideByColor(slaveColor)))
            elif self._edge.positionLayer == 3:
                self._messages += str('\nWork Layer')
                steps.extend(self.__solveBottomLayer(self.__getFinishSideByColor(slaveColor)))
            else:
                self._messages += str('\nSide Layer')
                steps.extend(self.__solveSideLayer(self.__getFinishSideByColor(slaveColor)))
            allsteps.extend(steps)
            self._messages += "\n" + str(steps) + "\n"
        return allsteps

    def __solveCorrectLayer(self, finishSide):
        """
        solve virtual cube if piece on white layer
        :param finishSide: int index of finish sode
        :return: string[] of steps
        """
        steps = []
        if self._edge.Orientation1Layer != finishSide:
            steps.extend(self.__stepsOfSideByIndex(self._edge.Orientation1Layer, False, 2))
            steps.extend(self.__turnOnWorkLayer(finishSide, self._edge.Orientation1Layer, 100))
            steps.extend(self.__stepsOfSideByIndex(finishSide, False, 2))
        return steps

    def __solveBottomLayer(self, finishSide):
        """
        solve virtual cube if piece on yellow layer
        :param finishSide: int index of finish sode
        :return: string[] of steps
        """
        steps = []
        steps.extend(self.__turnOnWorkLayer(finishSide, self._edge.Orientation1Layer, 100))
        steps.extend(self.__stepsOfSideByIndex(finishSide, False, 2))
        return steps

    def __solveSideLayer(self, finishSide):
        """
        solve virtual cube if piece on side layer
        :param finishSide: int index of finish sode
        :return: string[] of steps
        """
        steps = []
        sideLayer = [0, 2, 4, 5]
        TopLayerActivated = False
        if self._edge.positionLayer == sideLayer[3]:
            rightLayer = sideLayer[0]
        else:
            index = sideLayer.index(self._edge.positionLayer)
            rightLayer = sideLayer[index + 1]

        if self._edge.Orientation1Layer == 1:
            self._messages += "\nSide Layer Top Layer"
            steps.extend(self.__stepsOfSideByIndex(self._edge.positionLayer, False, 2))
            TopLayerActivated = True

        if self._edge.Orientation1Layer == 3 or TopLayerActivated:
            self._messages += "\nSide Layer Bottom Layer"
            safeLayer = self._edge.positionLayer
            steps.extend(self.__stepsOfSideByIndex(rightLayer, False, 1))
            steps.extend(self.__stepsOfSideByIndex(self._edge.positionLayer, True, 1))
            steps.extend(self.__stepsOfSideByIndex(self._edge.Orientation1Layer, True, 1))
            steps.extend(self.__stepsOfSideByIndex(safeLayer, False, 1))
            steps.extend(self.__turnOnWorkLayer(finishSide, self._edge.Orientation1Layer, 100))
            steps.extend(self.__stepsOfSideByIndex(finishSide, False, 2))
        else:
            self._messages += "\nSide Layer Side Layer"

            safeLayer = self._edge.Orientation1Layer
            if finishSide == safeLayer:
                if self._edge.position == [1, 2]:
                    steps.extend(self.__stepsOfSideByIndex(finishSide, False, 1))
                else:
                    steps.extend(self.__stepsOfSideByIndex(finishSide, True, 1))
            else:
                invert = True
                if self._edge.position == [1, 0]:
                    invert = False
                steps.extend(self.__stepsOfSideByIndex(self._edge.Orientation1Layer, invert, 1))
                steps.extend(self.__turnOnWorkLayer(finishSide, self._edge.Orientation1Layer, 100))
                steps.extend(self.__stepsOfSideByIndex(safeLayer, not invert, 1))
                steps.extend(self.__stepsOfSideByIndex(finishSide, False, 2))

        return steps

    def __stepsOfSideByIndex(self, index, invert, addSteps):
        """
        turns the virtual cube
        :param index: int side index
        :param invert: boolean false = clockwise
        :param addSteps: int amount of turns default 1
        :return: string[] of steps
        """
        steps = []
        if index == 0:
            steps = ['L']
        elif index == 1:
            steps = ['U']
        elif index == 2:
            steps = ['F']
        elif index == 3:
            steps = ['D']
        elif index == 4:
            steps = ['R']
        elif index == 5:
            steps = ['B']
        output = []
        for index in range(addSteps):
            output.extend(steps)
        if invert:
            output = self.__invertedSteps(output)
        self.__changeOrientations(output)
        return output

    @staticmethod
    def __invertedSteps(steps):
        """
        invert steps from clockwise to anticlockwise or anticlockwise to clockwise
        :param steps: string[]
        :return: string[] of inverted steps
        """
        for index in range(len(steps)):
            if len(steps[index]) != 2:
                steps[index] += 'i'
            else:
                steps[index] = steps[index].split('i')[0]
        return steps

    def __changeOrientations(self, steps):
        """
         Turns the cube by steps and overrides the stone positions
         :param steps: string[] of steps
         :return:
         """
        self._UF.turnCubeByArray(steps)
        slPositions = self._position.searchSLPosition(self._stone, self._UF.getCube())
        self._edge = Edge(slPositions[0], slPositions[1], slPositions[2], slPositions[3])

    def __getOffsetTurnWorkLayer(self, currentLayer, positionOffset):
        """
        get the offset turnposition from currentlayer
        :param currentLayer: int currentlayer index
        :param positionOffset: int offset to finish layer
        :return:
        """
        roundLayers = [0, 2, 4, 5]
        index = roundLayers.index(currentLayer)
        if positionOffset >= 0:
            newIndex = index + positionOffset
            if newIndex >= len(roundLayers) - 1:
                return roundLayers[newIndex - len(roundLayers)]
            else:
                return roundLayers[newIndex]
        else:
            newIndex = index + positionOffset
            if newIndex < 0:
                return roundLayers[len(roundLayers) + newIndex]
            else:
                return roundLayers[newIndex]

    def __turnOnWorkLayer(self, solveIndex, currentIndex, correctCorection, **kwargs):
        """
        universal turn on work layer
        :param solveIndex: int finishside position
        :param currentIndex: int current postion
        :param correctCorection: int turn correction
        :param kwargs: config, offset
        :return: string[] of steps
        """
        defaultConfig = [['D'], ['Di'], ['D', 'D']]

        configOpposit = [[0, 4], [2, 5], [5, 2], [4, 0]]
        configLeft = [[0, 2], [2, 4], [4, 5], [5, 0]]
        configRight = [[0, 5], [5, 4], [4, 2], [2, 0]]
        config = kwargs.get('config', defaultConfig)
        offset = kwargs.get('offset', 0)

        con = [solveIndex, currentIndex]
        if self.__arrayInclude(configRight, con):
            steps = self.__offsetTurnOnWorkLayer(config, 0, offset)
        elif self.__arrayInclude(configLeft, con):
            steps = self.__offsetTurnOnWorkLayer(config, 1, offset)
        elif self.__arrayInclude(configOpposit, con):
            steps = self.__offsetTurnOnWorkLayer(config, 2, offset)
        else:
            if correctCorection == 100:
                steps = []
            else:
                steps = self.__offsetTurnOnWorkLayer(config, correctCorection, 0)
        self.__changeOrientations(steps)
        return steps

    def __offsetTurnOnWorkLayer(self, config, index, offset):
        """
        turns offset to finish layer
        :param config: string[] of steps config
        :param index: int current index
        :param offset: int turn offset
        :return: string[] of step/s
        """
        if offset >= 0:
            newIndex = index + offset
            if newIndex < len(config):
                return config[newIndex]
            else:
                return config[newIndex - len(config)]
        else:
            newIndex = index + offset
            if newIndex < 0:
                return config[len(config) + newIndex]
            else:
                return config[newIndex]

    @staticmethod
    def __getFinishSideByColor(color):
        """
        get finish index by main color
        :param color: string of color
        :return: int index of color
        """
        side = [0, 1, 2, 3, 4, 5]
        colors = ['G', 'W', 'R', 'Y', 'B', 'O']
        return side[colors.index(color)]

    @staticmethod
    def __getColorBySide(side):
        """
        get color of side by index
        :param side: int of side
        :return:
        """
        sides = [0, 1, 2, 3, 4, 5]
        colors = ['G', 'W', 'R', 'Y', 'B', 'O']
        return colors[sides.index(side)]

    @staticmethod
    def __getStepBySide(side):
        """
        get move of side index
        :param side: int of side
        :return: string of step
        """
        steps = ['L', 'U', 'F', 'D', 'R', 'B']
        sides = [0, 1, 2, 3, 4, 5]
        return steps[sides.index(side)]

    def __arrayInclude(self, array, search):
        """
        checks if mutliarray include search
        :param array: any[]
        :param search: search type
        :return: boolean if found
        """
        for ar in array:
            if ar == search:
                return True
        return False
