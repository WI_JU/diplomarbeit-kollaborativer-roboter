import Utilities.UseFunctions
from Utilities.Positions import Positions
import numpy as np


class TLMuster():
    def __init__(self):
        self.Finish = [
            ['Y', 'Y', 'Y'],
            ['Y', 'Y', 'Y'],
            ['Y', 'Y', 'Y']
        ]
        self.Fish = [
            ['X', 'Y', 'X'],
            ['Y', 'Y', 'Y'],
            ['Y', 'Y', 'X']
        ]
        self.thadeus = [
            ['Y', 'Y', 'Y'],
            ['Y', 'Y', 'Y'],
            ['X', 'Y', 'X']
        ]
        self.eigth = [
            ['X', 'Y', 'Y'],
            ['Y', 'Y', 'Y'],
            ['Y', 'Y', 'X']
        ]
        self.cross = [
            ['X', 'Y', 'X'],
            ['Y', 'Y', 'Y'],
            ['X', 'Y', 'X']
        ]
        self.littleL = [
            ['X', 'X', 'X'],
            ['Y', 'Y', 'X'],
            ['X', 'Y', 'X']
        ]
        self.street = [
            ['X', 'Y', 'X'],
            ['X', 'Y', 'X'],
            ['X', 'Y', 'X']
        ]
        self.empty = [
            ['X', 'X', 'X'],
            ['X', 'Y', 'X'],
            ['X', 'X', 'X']
        ]
        self.AllMuster = [self.Finish, self.eigth, self.thadeus, self.Fish, self.cross, self.street, self.littleL,
                          self.empty]


class TopLayer:
    def __init__(self, UF: Utilities.UseFunctions.UseFunctions):
        self._UF = UF
        self._position = Positions()
        self._length = 0
        self._messages = ""
        self._TLMuster = TLMuster()
        self.TopLayer = self._UF.getCube()[3]

    def getLen(self):
        """
        adds up all moves
        :return: int of moves
        """
        return self._length

    def getMessages(self):
        """
        the messages of the solver
        :return: string of messages
        """
        return self._messages

    def canSolveCube(self):
        """
        checks if can solve virtual cube
        :return: boolean can/cannot solve
        """
        steps = self.__generateSteps()
        if not steps:
            return False
        else:
            self._length += len(steps)
            return True

    def getSteps(self):
        """
        solves Cube and return steps to solve
        :return: string[] of steps
        """
        steps = self.__generateSteps()
        return steps

    def getSolvedCube(self):
        """
        gives a copy of partly solved cube
        :return: string[] of virtual cube
        """
        steps = self.__generateSteps()
        return self._UF.getCube()


    @staticmethod
    def __getStepBySide(side):
        """
        get move of side index
        :param side: int of side
        :return: string of step
        """
        steps = ['L', 'U', 'F', 'D', 'R', 'B']
        sides = [0, 1, 2, 3, 4, 5]
        return steps[sides.index(side)]

    def __generateSteps(self):
        """
        solves all patterns of the cube
        :return: string[] of steps
        """
        self._messages = ""
        allsteps = []
        self.TopLayer = self._UF.getCube()[3]
        for i in range(300):
            steps = []
            [musterIndex, self._rotationIndex] = self.__getMusterIndexAndRotationIndex()
            if musterIndex == 0:
                self._messages += str('\n') + str("Finish") + str('\n')
                steps.append([''])
            elif musterIndex == 1:
                steps.extend(self._eigth())
            elif musterIndex == 2:
                steps.extend(self._thadeus())
            elif musterIndex == 3:
                steps.extend(self._fish())
            elif musterIndex == 4:
                steps.extend(self._cross())
            elif musterIndex == 5:
                steps.extend(self._street())
            elif musterIndex == 6:
                steps.extend(self._littleL())
            elif musterIndex == 7:
                steps.extend(self._empty())
            self.__changeOrientations(steps)
            self.TopLayer = self._UF.getCube()[3]
            allsteps.extend(steps)
            self._messages += str(steps)
            if self.isEqual(self.TopLayer, self._TLMuster.Finish):
                return allsteps
        return allsteps

    def _eigth(self):
        """
        solving the pattern eigth
        :return: string[] of steps
        """
        self._messages += str('\n') + str("Eigth") + str('\n')
        steps = []
        steps.extend(self._cross())
        return steps

    def _thadeus(self):
        """
        solving the pattern thadeus
        :return: string[] of steps
        """
        self._messages += str('\n') + str("Thadeus") + str('\n')
        steps = []
        steps.extend(self._fish())
        return steps

    def _fish(self):
        """
        solving the pattern fish
        :return: string[] of steps
        """
        self._messages += str('\n') + str("Fish") + str('\n')
        steps = []
        turnLayer = self.__getTurnLayerByRotationIndex(self._rotationIndex)
        steps.extend(self.__stepsOfSideByIndex(turnLayer,False,1))
        steps.extend(self.__stepsOfSideByIndex(3, False, 1))
        steps.extend(self.__stepsOfSideByIndex(turnLayer, True, 1))
        steps.extend(self.__stepsOfSideByIndex(3, False, 1))
        steps.extend(self.__stepsOfSideByIndex(turnLayer, False, 1))
        steps.extend(self.__stepsOfSideByIndex(3, False, 2))
        steps.extend(self.__stepsOfSideByIndex(turnLayer, True, 1))
        return steps

    def _cross(self):
        """
        solving the pattern cross
        :return: string[] of steps
        """
        self._messages += str('\n') + str("Cross") + str('\n')
        steps = []
        turnLayer = self.__getTurnLayerBySideOptions(self._rotationIndex)
        steps.extend(self.__stepsOfSideByIndex(turnLayer, False, 1))
        steps.extend(self.__stepsOfSideByIndex(3, False, 1))
        steps.extend(self.__stepsOfSideByIndex(turnLayer, True, 1))
        steps.extend(self.__stepsOfSideByIndex(3, False, 1))
        steps.extend(self.__stepsOfSideByIndex(turnLayer, False, 1))
        steps.extend(self.__stepsOfSideByIndex(3, False, 2))
        steps.extend(self.__stepsOfSideByIndex(turnLayer, True, 1))
        return steps

    def _street(self):
        """
        solving the pattern street
        :return: string[] of steps
        """
        self._messages += str('\n') + str("Street") + str('\n')
        steps = []
        turnLayer = self.__getTurnLayerByRotationIndex(self._rotationIndex)
        steps.extend(self._leftTurnSide(turnLayer))
        return steps

    def _littleL(self):
        """
        solving the pattern little L
        :return: string[] of steps
        """
        self._messages += str('\n') + str("Little L") + str('\n')
        steps = []
        turnLayer = self.__getTurnLayerByRotationIndex(self._rotationIndex)
        turnLayerOffset = self.__getOffsetTurnWorkLayer(turnLayer, -1)
        steps.extend(self.__stepsOfSideByIndex(turnLayer, False, 1))
        steps.extend(self.__stepsOfSideByIndex(3, False, 1))
        steps.extend(self.__stepsOfSideByIndex(turnLayerOffset, False, 1))
        steps.extend(self.__stepsOfSideByIndex(3, True, 1))
        steps.extend(self.__stepsOfSideByIndex(turnLayerOffset, True, 1))
        steps.extend(self.__stepsOfSideByIndex(turnLayer, True, 1))
        return steps

    def _empty(self):
        """
        solving the pattern only mid stone
        :return: string[] of steps
        """
        self._messages += str('\n') + str("Empty") + str('\n')
        steps = []
        steps.extend(self._leftTurnSide(0))
        return steps

    def _leftTurnSide(self,turnLayer):
        """
        turns to the left side of turnlayer
        :param turnLayer: int layer index
        :return: string[] of steps
        """
        steps = []
        turnLayerOffset = self.__getOffsetTurnWorkLayer(turnLayer,-1)
        steps.extend(self.__stepsOfSideByIndex(turnLayer,False,1))
        steps.extend(self.__stepsOfSideByIndex(turnLayerOffset, False, 1))
        steps.extend(self.__stepsOfSideByIndex(3,False,1))
        steps.extend(self.__stepsOfSideByIndex(turnLayerOffset, True, 1))
        steps.extend(self.__stepsOfSideByIndex(3, True, 1))
        steps.extend(self.__stepsOfSideByIndex(turnLayer, True, 1))
        return  steps

    def __getMusterIndexAndRotationIndex(self):
        """
        checks which pattern is active and in which rotation it is
        :return: [int, int] index of pattern / rotation index
        """
        for Muster in self._TLMuster.AllMuster:
            for i in range(4):
                turnedMuster = np.rot90(Muster, i, (1, 0))
                if self.isEqual(self.TopLayer, turnedMuster):
                    return [self._TLMuster.AllMuster.index(Muster), i]

    def __getTurnLayerByRotationIndex(self, rotationIndex):
        """
        returns the index of the main turnlayer by the roation index
        :param rotationIndex: int
        :return: int turn layer index
        """
        turnLayer = 4
        if rotationIndex == 1:
            turnLayer = 5
        elif rotationIndex == 2:
            turnLayer = 0
        elif rotationIndex == 3:
            turnLayer = 2
        return turnLayer

    def __getTurnLayerBySideOptions(self, rotationIndex):
        """
        checks all side options and returns the turnlayer index
        :param rotationIndex: int rotations index
        :return: int turn layer index
        """
        option = [
            ['X', 'X', 'X'],
            ['X', 'X', 'Y'],
            ['Y', 'X', 'X'],
            ['Y', 'X', 'Y']
        ]
        sideIndexs = []
        sideArray = self.__generateSideArray()
        for side in sideArray:
            for i in range(len(option)):
                if np.array_equal(side, option[i]):
                    sideIndexs.append(i)
        for sideIndex in sideIndexs:
            index = sideIndexs.index(sideIndex)
            if sideIndex == 1 or sideIndex == 3:
                if index == 0:
                    return 0
                elif index == 1:
                    return 2
                elif index == 2:
                    return 4
                elif index == 2:
                    return 5
        return 0

    def __generateSideArray(self):
        """
        generates a Yellow based matrix of layer and replace all others with X
        :return: string[] of cleaned side
        """
        cube = self._UF.getCube().copy()
        sideArray = [cube[0][2],cube[2][2],cube[4][2],cube[5][2]]
        for i in range(len(sideArray)):
            for j in range(len(sideArray[i])):
                if sideArray[i][j] != 'Y':
                    sideArray[i][j] = 'X'
        return sideArray

    @staticmethod
    def isEqual(state1, musterState):
        """
        checks if two arrays are equal (X == all true)
        :param state1: search option
        :param musterState: base option
        :return: boolean
        """
        for i in range(len(musterState)):
            for j in range(len(musterState[i])):
                if musterState[i][j] == 'X':
                    musterState[i][j] = state1[i][j]
        solvedStateArray = state1.copy()
        if np.array_equal(musterState, solvedStateArray):
            return True
        else:
            return False

    def __stepsOfSideByIndex(self, index, invert, addSteps):
        """
        turns the virtual cube
        :param index: int side index
        :param invert: boolean false = clockwise
        :param addSteps: int amount of turns default 1
        :return: string[] of steps
        """
        steps = []
        if index == 0:
            steps = ['L']
        elif index == 1:
            steps = ['U']
        elif index == 2:
            steps = ['F']
        elif index == 3:
            steps = ['D']
        elif index == 4:
            steps = ['R']
        elif index == 5:
            steps = ['B']
        output = []
        for index in range(addSteps):
            output.extend(steps)
        if invert:
            output = self.__invertedSteps(output)
        return output

    @staticmethod
    def __invertedSteps(steps):
        """
        invert steps from clockwise to anticlockwise or anticlockwise to clockwise
        :param steps: string[]
        :return: string[] of inverted steps
        """
        for index in range(len(steps)):
            if len(steps[index]) != 2:
                steps[index] += 'i'
            else:
                steps[index] = steps[index].split('i')[0]
        return steps

    def __changeOrientations(self, steps):
        """
         Turns the cube by steps and overrides the stone positions
         :param steps: string[] of steps
         :return:
         """
        self._UF.turnCubeByArray(steps)

    def __getOffsetTurnWorkLayer(self, currentLayer, positionOffset):
        """
        get the offset turnposition from currentlayer
        :param currentLayer: int currentlayer index
        :param positionOffset: int offset to finish layer
        :return:
        """
        roundLayers = [0, 2, 4, 5]
        index = roundLayers.index(currentLayer)
        if positionOffset >= 0:
            newIndex = index + positionOffset
            if newIndex >= len(roundLayers) - 1:
                return roundLayers[newIndex - len(roundLayers)]
            else:
                return roundLayers[newIndex]
        else:
            newIndex = index + positionOffset
            if newIndex < 0:
                return roundLayers[len(roundLayers) + newIndex]
            else:
                return roundLayers[newIndex]

    def __turnOnWorkLayer(self, solveIndex, currentIndex, correctCorection, **kwargs):
        """
        universal turn on work layer
        :param solveIndex: int finishside position
        :param currentIndex: int current postion
        :param correctCorection: int turn correction
        :param kwargs: config, offset
        :return: string[] of steps
        """
        defaultConfig = [['D'], ['Di'], ['D', 'D']]

        configOpposit = [[0, 4], [2, 5], [5, 2], [4, 0]]
        configLeft = [[0, 2], [2, 4], [4, 5], [5, 0]]
        configRight = [[0, 5], [5, 4], [4, 2], [2, 0]]
        config = kwargs.get('config', defaultConfig)
        offset = kwargs.get('offset', 0)

        con = [solveIndex, currentIndex]
        if self.__arrayInclude(configRight, con):
            steps = self.__offsetTurnOnWorkLayer(config, 0, offset)
        elif self.__arrayInclude(configLeft, con):
            steps = self.__offsetTurnOnWorkLayer(config, 1, offset)
        elif self.__arrayInclude(configOpposit, con):
            steps = self.__offsetTurnOnWorkLayer(config, 2, offset)
        else:
            if correctCorection == 100:
                steps = []
            else:
                steps = self.__offsetTurnOnWorkLayer(config, correctCorection, 0)
        self.__changeOrientations(steps)
        return steps

    def __offsetTurnOnWorkLayer(self, config, index, offset):
        """
        turns offset to finish layer
        :param config: string[] of steps config
        :param index: int current index
        :param offset: int turn offset
        :return: string[] of step/s
        """
        if offset >= 0:
            newIndex = index + offset
            if newIndex < len(config):
                return config[newIndex]
            else:
                return config[newIndex - len(config)]
        else:
            newIndex = index + offset
            if newIndex < 0:
                return config[len(config) + newIndex]
            else:
                return config[newIndex]

    def __arrayInclude(self, array, search):
        """
        checks if mutliarray include search
        :param array: any[]
        :param search: search type
        :return: boolean if found
        """
        for ar in array:
            if ar == search:
                return True
        return False


