from copy import deepcopy

import Utilities.UseFunctions
from Utilities.Positions import Positions
import itertools


class Corner:
    def __init__(self, piece, Layer, position, Orientation1, Orientation2):
        self.piece = piece
        self.positionLayer = Layer
        self.position = position
        self.Orientation1Layer = Orientation1[0]
        self.Orientation1 = [Orientation1[1], Orientation1[2]]
        self.Orientation2Layer = Orientation2[0]
        self.Orientation2 = [Orientation2[1], Orientation2[2]]


class FirstLayer():
    def __init__(self, UF: Utilities.UseFunctions.UseFunctions, superMinimum=5):
        self._UF = UF
        self._position = Positions()
        self._length = 0
        self._messages = ""
        self._PositionZero = ['WRG', 'WBR', 'WGO', 'WOB']
        self._superMinimum = superMinimum

    def getLen(self):
        """
        adds up all moves
        :return: int of moves
        """
        return self._length

    def getMessages(self):
        """
        the messages of the solver
        :return: string of messages
        """
        return self._messages

    def canSolveCube(self):
        """
        checks if can solve virtual cube
        :return: boolean can/cannot solve
        """
        steps = self.__generateStepsToSolve()
        if not steps:
            return False
        else:
            self._length += len(steps)
            return True

    def getSolvedCube(self):
        """
        gives a copy of partly solved cube
        :return: string[] of virtual cube
        """
        steps = self.__generateStepsToSolve()
        return self._UF.getCube()

    def getSteps(self):
        """
        solves Cube and return steps to solve
        :return: string[] of steps
        """
        steps = self.__generateStepsToSolve()
        return steps

    @staticmethod
    def __getFinishSideByColor(color):
        """
        get finish index by main color
        :param color: string of color
        :return: int index of color
        """
        side = [0, 1, 2, 3, 4, 5]
        colors = ['G', 'W', 'R', 'Y', 'B', 'O']
        return side[colors.index(color)]

    @staticmethod
    def __getColorBySide(side):
        """
        get color of side by index
        :param side: int of side
        :return:
        """
        sides = [0, 1, 2, 3, 4, 5]
        colors = ['G', 'W', 'R', 'Y', 'B', 'O']
        return colors[sides.index(side)]

    @staticmethod
    def __getStepBySide(side):
        """
        get move of side index
        :param side: int of side
        :return: string of step
        """
        steps = ['L', 'U', 'F', 'D', 'R', 'B']
        sides = [0, 1, 2, 3, 4, 5]
        return steps[sides.index(side)]

    def __generateStepsToSolve(self):
        """
        solves the cube of permutation of Stones
        :return: string[] of steps
        """
        minLenght = 100000
        solveSteps = []
        bevoreCube = deepcopy(self._UF.getCube())
        permutationObject = list(itertools.permutations(self._PositionZero))
        for perm in permutationObject:
            self._messages = ""
            self._UF.setCube(bevoreCube.copy())
            steps = self.__solveStepsByPieceOrder(perm)
            if 1 < len(steps) < minLenght:
                minLenght = len(steps)
                if minLenght < self._superMinimum:
                    return steps
                solveSteps = steps.copy()
        self._UF.setCube(bevoreCube)
        self._UF.turnCubeByArray(solveSteps)
        return solveSteps

    def __solveStepsByPieceOrder(self, orderOfPieces):
        """
        solves cube by order of stone positions
        :param orderOfPieces: string[] of Positions
        :return: string[] of steps
        """
        self._messages = ""
        allsteps = []
        for stone in orderOfPieces:
            steps = []
            self._stone = stone
            self._messages += str('\n') + str(stone) + str('\n')
            flPositions = self._position.searchFLPosition(stone, self._UF.getCube())
            self._messages += str('\n') + str(flPositions) + str('\n')
            self._corner = Corner(flPositions[0], flPositions[1], flPositions[2], flPositions[3], flPositions[4])
            masterColor = self._corner.piece[:][1]
            slaveColor = self._corner.piece[:][2]
            if self._corner.positionLayer == 1:
                if self._corner.Orientation1Layer == self.__getFinishSideByColor(masterColor):
                    steps.extend(self.__fLCP())
                else:
                    steps.extend(self.__fLFP())

            elif self._corner.positionLayer == 3:
                if self._corner.Orientation1Layer == self.__getFinishSideByColor(masterColor):
                    steps.extend(self.__wLCP())
                else:
                    steps.extend(self.__wLFP())

            elif self._corner.Orientation2Layer == 3 or self._corner.Orientation1Layer == 3:
                mCLayer = self.__getFinishSideByColor(masterColor)
                sCLayer = self.__getFinishSideByColor(slaveColor)
                if self._corner.Orientation2Layer == 3 and self._corner.Orientation1Layer == sCLayer:
                    steps.extend(self.__sLCP())
                elif self._corner.Orientation2Layer == 3 and self._corner.Orientation1Layer != sCLayer:
                    steps.extend(self.__sLFP())
                elif self._corner.Orientation1Layer == 3 and self._corner.Orientation2Layer == mCLayer:
                    steps.extend(self.__sL2CP())
                elif self._corner.Orientation1Layer == 3 and self._corner.Orientation2Layer != mCLayer:
                    steps.extend(self.__sL2FP())
            else:
                mCLayer = self.__getFinishSideByColor(masterColor)
                sCLayer = self.__getFinishSideByColor(slaveColor)
                if self._corner.Orientation1Layer == 1 and self._corner.Orientation2Layer == mCLayer:
                    steps.extend(self.__bLCP())
                elif self._corner.Orientation1Layer == 1 and self._corner.Orientation2Layer != mCLayer:
                    steps.extend(self.__bLFP())
                elif self._corner.Orientation2Layer == 1 and self._corner.Orientation1Layer == sCLayer:
                    steps.extend(self.__bL2CP())
                elif self._corner.Orientation2Layer == 1 and self._corner.Orientation1Layer != sCLayer:
                    steps.extend(self.__bL2FP())
                else:
                    # print("Unkown Case")
                    pass
            allsteps.extend(steps)
            self._messages += str(steps)
        return allsteps

    def __fLCP(self):
        """
        solving steps for first layer correct position
        :return: steps to solve current position
        """
        self._messages += "First Layer Correct Position" + str('\n')
        steps = []
        return steps

    def __fLFP(self):
        """
        solving steps for first Layer False Position
        :return: steps to solve current position
        """
        self._messages += "First Layer False Position" + str('\n')
        steps = []
        returnLayer = self._corner.Orientation1Layer
        config = [True, False]
        steps.extend(self.__stepsOfSideByIndex(returnLayer, config[0], 1))
        steps.extend(self.__stepsOfSideByIndex(3, config[0], 1))
        steps.extend(self.__stepsOfSideByIndex(returnLayer, config[1], 1))

        steps.extend(self.__sLFP())

        return steps

    def __wLCP(self):
        """
        solving steps for work Layer Correct Position
        :return: steps to solve current position
        """
        self._messages += "Work Layer Correct Position" + str('\n')
        steps = []
        steps.extend(self.__turnOnWorkLayer(self.__getFinishSideByColor(self._corner.piece[:][1]),
                                            self._corner.Orientation2Layer, 1,
                                            offset=0))
        returnLayer = self._corner.Orientation2Layer
        steps.extend(self.__stepsOfSideByIndex(self._corner.Orientation2Layer, True, 1))
        steps.extend(self.__stepsOfSideByIndex(3, False, 2))
        steps.extend(self.__stepsOfSideByIndex(returnLayer, False, 1))
        steps.extend(self.__sL2FP())
        return steps

    def __wLFP(self):
        """
        solving steps for Work Layer False Positon
        :return: steps to solve current position
        """
        self._messages += "Work Layer False Positon" + str('\n')
        steps = []
        if self.__getFinishSideByColor(self._corner.piece[:][1]) != self._corner.Orientation2Layer:
            steps.extend(self.__turnOnWorkLayer(self.__getFinishSideByColor(self._corner.piece[:][1]),
                                                self._corner.Orientation2Layer, 1,
                                                offset=0))
        returnLayer = self._corner.Orientation2Layer
        steps.extend(self.__stepsOfSideByIndex(self._corner.Orientation2Layer, True, 1))
        steps.extend(self.__stepsOfSideByIndex(3, False, 1))
        steps.extend(self.__stepsOfSideByIndex(returnLayer, False, 1))
        if self.__getFinishSideByColor(self._corner.piece[:][1]) != self._corner.Orientation2Layer:
            steps.extend(self.__turnOnWorkLayer(self.__getFinishSideByColor(self._corner.piece[:][2]),
                                                self._corner.Orientation2Layer, 1,
                                                offset=1, config=[['Di'], ['D'], ['D', 'D']]))
        steps.extend(self.__sL2CP())
        return steps

    def __sLCP(self):
        """
        solving steps for Side Layer Correct Position
        :return: steps to solve current position
        """
        self._messages += "Side Layer Correct Position" + str('\n')
        steps = []
        if self._stone != 'WGO':
            turnLayer = self.__getOffsetTurnWorkLayer(self._corner.Orientation1Layer, 1)
        else:
            if self.__getFinishSideByColor(self._corner.piece[:][2]) != self._corner.Orientation1Layer:
                steps.extend(self.__turnOnWorkLayer(self.__getFinishSideByColor(self._corner.piece[:][1]),
                                                    self._corner.Orientation1Layer, 1,
                                                    offset=0))
            turnLayer = self.__getFinishSideByColor(self._corner.piece[:][1])
        steps.extend(self.__stepsOfSideByIndex(turnLayer, True, 1))
        steps.extend(self.__stepsOfSideByIndex(3, False, 1))
        steps.extend(self.__stepsOfSideByIndex(turnLayer, False, 1))
        return steps

    def __sLFP(self):
        """
        solving steps for Side Layer False Position
        :return: steps to solve current position
        """
        self._messages += "Side Layer False Position" + str('\n')
        steps = []
        if self.__getFinishSideByColor(self._corner.piece[:][1]) != self._corner.Orientation2Layer:
            steps.extend(self.__turnOnWorkLayer(self.__getFinishSideByColor(self._corner.piece[:][1]),
                                                self._corner.Orientation1Layer, 1,
                                                offset=1))
        if self._stone == 'WGO':
            steps.extend(self.__sLCP())
        else:
            steps.extend(self.__sLCP())
        return steps

    def __sL2CP(self):
        """
        solving steps for Side Layer 2 Correct Position
        :return: steps to solve current position
        """
        self._messages += "Side Layer 2 Correct Position" + str('\n')
        steps = []
        turnLayer = self.__getOffsetTurnWorkLayer(self._corner.Orientation2Layer, -1)

        steps.extend(self.__stepsOfSideByIndex(turnLayer, False, 1))
        steps.extend(self.__stepsOfSideByIndex(3, True, 1))
        steps.extend(self.__stepsOfSideByIndex(turnLayer, True, 1))
        return steps

    def __sL2FP(self):
        """
        solving steps for Side Layer 2 False Position
        :return: steps to solve current position
        """
        self._messages += "Side Layer 2 False Position" + str('\n')
        steps = []
        if self.__getFinishSideByColor(self._corner.piece[:][2]) != self._corner.Orientation1Layer:
            steps.extend(self.__turnOnWorkLayer(self.__getFinishSideByColor(self._corner.piece[:][2]),
                                                self._corner.Orientation2Layer, 0,
                                                offset=-1))
        steps.extend(self.__sL2CP())
        return steps

    def __bLCP(self):
        """
        solving steps for bottom Layer Correct Position
        :return: steps to solve current position
        """
        self._messages += "bottom Layer Correct Position" + str('\n')
        steps = []
        returnLayer = self._corner.positionLayer
        steps.extend(self.__stepsOfSideByIndex(returnLayer, False, 1))
        steps.extend(self.__stepsOfSideByIndex(3, False, 1))
        steps.extend(self.__stepsOfSideByIndex(returnLayer, True, 1))
        steps.extend(self.__stepsOfSideByIndex(3, True, 2))
        steps.extend(self.__sLCP())
        return steps

    def __bLFP(self):
        """
        solving steps for bottom Layer False Position
        :return: steps to solve current position
        """
        self._messages += "bottom Layer False Position" + str('\n')
        steps = []
        returnLayer = self._corner.positionLayer
        steps.extend(self.__stepsOfSideByIndex(returnLayer, False, 1))
        steps.extend(self.__stepsOfSideByIndex(3, False, 1))
        steps.extend(self.__stepsOfSideByIndex(returnLayer, True, 1))
        if self.__getFinishSideByColor(self._corner.piece[:][2]) != self._corner.Orientation1Layer:
            steps.extend(self.__turnOnWorkLayer(self.__getFinishSideByColor(self._corner.piece[:][1]),
                                                self._corner.Orientation1Layer, 1,
                                                offset=1))
        steps.extend(self.__sLCP())
        return steps

    def __bL2CP(self):
        """
        solving steps for bottom Layer 2 Correct Postion
        :return: steps to solve current position
        """
        self._messages += "bottom Layer 2 Correct Postion" + str('\n')
        steps = []
        returnLayer = self._corner.positionLayer
        steps.extend(self.__stepsOfSideByIndex(returnLayer, True, 1))
        steps.extend(self.__stepsOfSideByIndex(3, True, 1))
        steps.extend(self.__stepsOfSideByIndex(returnLayer, False, 1))
        steps.extend(self.__sL2FP())
        return steps

    def __bL2FP(self):
        """
        solving steps for bottom Layer 2 False Postion
        :return: steps to solve current position
        """
        self._messages += "bottom Layer 2 False Postion" + str('\n')
        steps = []
        returnLayer = self._corner.positionLayer
        steps.extend(self.__stepsOfSideByIndex(returnLayer, True, 1))
        steps.extend(self.__stepsOfSideByIndex(3, True, 1))
        steps.extend(self.__stepsOfSideByIndex(returnLayer, False, 1))
        if self.__getFinishSideByColor(self._corner.piece[:][1]) != self._corner.Orientation2Layer:
            steps.extend(self.__turnOnWorkLayer(self.__getFinishSideByColor(self._corner.piece[:][2]),
                                                self._corner.Orientation2Layer, 0,
                                                offset=-1))
        steps.extend(self.__sL2CP())
        return steps

    def __stepsOfSideByIndex(self, index, invert, addSteps):
        """
        turns the virtual cube
        :param index: int side index
        :param invert: boolean false = clockwise
        :param addSteps: int amount of turns default 1
        :return: string[] of steps
        """
        steps = []
        if index == 0:
            steps = ['L']
        elif index == 1:
            steps = ['U']
        elif index == 2:
            steps = ['F']
        elif index == 3:
            steps = ['D']
        elif index == 4:
            steps = ['R']
        elif index == 5:
            steps = ['B']
        output = []
        for index in range(addSteps):
            output.extend(steps)
        if invert:
            output = self.__invertSteps(output)
        self.__changeOrientations(output)
        return output

    @staticmethod
    def __invertSteps(steps):
        """
        invert steps from clockwise to anticlockwise or anticlockwise to clockwise
        :param steps: string[]
        :return: string[] of inverted steps
        """
        for index in range(len(steps)):
            if len(steps[index]) != 2:
                steps[index] += 'i'
            else:
                steps[index] = steps[index].split('i')[0]
        return steps

    def __changeOrientations(self, steps):
        """
        Turns the cube by steps and overrides the stone positions
        :param steps: string[] of steps
        :return:
        """
        self._UF.turnCubeByArray(steps)
        flPositions = self._position.searchFLPosition(self._stone, self._UF.getCube())
        self._corner = Corner(flPositions[0], flPositions[1], flPositions[2], flPositions[3], flPositions[4])

    def __getOffsetTurnWorkLayer(self, currentLayer, positionOffset):
        """
        get the offset turnposition from currentlayer
        :param currentLayer: int currentlayer index
        :param positionOffset: int offset to finish layer
        :return:
        """
        roundLayers = [0, 2, 4, 5]
        index = roundLayers.index(currentLayer)
        if positionOffset >= 0:
            newIndex = index + positionOffset
            if newIndex > len(roundLayers):
                return roundLayers[newIndex - len(roundLayers)]
            else:
                return roundLayers[newIndex]
        else:
            newIndex = index + positionOffset
            if newIndex < 0:
                return roundLayers[len(roundLayers) + newIndex]
            else:
                return roundLayers[newIndex]

    def __turnOnWorkLayer(self, solveIndex, currentIndex, correctCorection, **kwargs):
        """
        universal turn on work layer
        :param solveIndex: int finishside position
        :param currentIndex: int current postion
        :param correctCorection: int turn correction
        :param kwargs: config, offset
        :return: string[] of steps
        """
        defaultConfig = [['D'], ['Di'], ['D', 'D']]

        configOpposit = [[0, 4], [2, 5], [5, 2], [4, 0]]
        configLeft = [[0, 2], [2, 4], [4, 5], [5, 0]]
        configRight = [[0, 5], [5, 4], [4, 2], [2, 0]]
        config = kwargs.get('config', defaultConfig)
        offset = kwargs.get('offset', 0)

        con = [solveIndex, currentIndex]
        if self.__arrayInclude(configRight, con):
            steps = self.__offsetTurnOnWorkLayer(config, 0, offset)
        elif self.__arrayInclude(configLeft, con):
            steps = self.__offsetTurnOnWorkLayer(config, 1, offset)
        elif self.__arrayInclude(configOpposit, con):
            steps = self.__offsetTurnOnWorkLayer(config, 2, offset)
        else:
            if correctCorection == 100:
                steps = []
            else:
                steps = self.__offsetTurnOnWorkLayer(config, correctCorection, 0)
        self.__changeOrientations(steps)
        return steps

    def __offsetTurnOnWorkLayer(self, config, index, offset):
        """
        turns offset to finish layer
        :param config: string[] of steps config
        :param index: int current index
        :param offset: int turn offset
        :return: string[] of step/s
        """
        if offset >= 0:
            newIndex = index + offset
            if newIndex < len(config):
                return config[newIndex]
            else:
                return config[newIndex - len(config)]
        else:
            newIndex = index + offset
            if newIndex < 0:
                return config[len(config) + newIndex]
            else:
                return config[newIndex]

    def __arrayInclude(self, array, search):
        """
        checks if mutliarray include search
        :param array: any[]
        :param search: search type
        :return: boolean if found
        """
        for ar in array:
            if ar == search:
                return True
        return False
