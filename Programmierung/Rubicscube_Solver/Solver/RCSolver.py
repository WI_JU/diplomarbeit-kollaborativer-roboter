import Utilities.UseFunctions
from Solver.WhiteCross import WhiteCross
from Solver.FirstLayer import FirstLayer
from Solver.SecondLayer import SecondLayer
from Solver.TopLayer import TopLayer
from Solver.LastLayer import LastLayer


class RubicsCubeSolver():
    def __init__(self, UF: Utilities.UseFunctions.UseFunctions):
        self._UF = UF
        _superMinimum = 10
        self._WC = WhiteCross(self._UF, superMinimum = _superMinimum)
        self._FL = FirstLayer(self._UF, superMinimum = _superMinimum)
        self._SL = SecondLayer(self._UF, superMinimum = _superMinimum)
        self._TL = TopLayer(self._UF)
        self._LL = LastLayer(self._UF)
        self._WhiteCross = []
        self._FirstLayer = []
        self._SecondLayer = []
        self._TopLayer = []

    def canSolveCube(self, unsolvedCube):
        """
        checks if can solve virtual cube
        :param unsolvedCube: string[] of virtual cube
        # :return: boolean can/cannot solve
        """
        self._UF.setCube(unsolvedCube)
        states = []
        states.append(self._WC.canSolveCube())
        self._WhiteCross = self._WC.getSolvedCube().copy()
        states.append(self._FL.canSolveCube())
        self._FirstLayer = self._FL.getSolvedCube().copy()
        states.append(self._SL.canSolveCube())
        self._SecondLayer = self._SL.getSolvedCube().copy()
        states.append(self._TL.canSolveCube())
        self._TopLayer = self._TL.getSolvedCube().copy()
        states.append(self._LL.canSolveCube())
        for state in states:
            if not state:
                return False
        return True

    def generateArrayOfSteps(self, stepsArray):
        """
        Converts multiple Array of steps to one Array of steps
        :param stepsArray: string[] of Steps
        :return: string[] of Steps
        """
        steps = []
        for stepArray in stepsArray:
            for step in stepArray:
                if step != '' or step != ['']:
                    steps.append(step)
        return steps

    def generateSolvingSteps(self, unsolvedCube):
        """
        generates solving steps of virtual cube
        :param unsolvedCube: string[] of virtual cube
        :return: string[] of steps
        """
        self._UF.setCube(unsolvedCube)
        steps = []
        steps.append(self._WC.getSteps())
        self._WhiteCross = self._WC.getSolvedCube().copy()
        steps.append(self._FL.getSteps())
        self._FirstLayer = self._FL.getSolvedCube().copy()
        steps.append(self._SL.getSteps())
        self._SecondLayer = self._SL.getSolvedCube().copy()
        steps.append(self._TL.getSteps())
        self._TopLayer = self._TL.getSolvedCube().copy()
        steps.append(self._LL.getSteps())
        return steps

    def getCube(self):
        return self._UF.getCube()

    def _getCross(self):
        """
        gives virtual cube after whitecross solved
        :return: string[] of virtual cube
        """
        return self._WhiteCross

    def _getFirstLayer(self):
        """
        gives virtual cube after firstlayer solved
        :return: string[] of virtual cube
        """
        return self._FirstLayer

    def _getSecondLayer(self):
        """
        gives virtual cube after secondlayer solved
        :return: string[] of virtual cube
        """
        return self._SecondLayer

    def _getTopLayer(self):
        """
        gives virtual cube after toplayer solved
        :return: string[] of virtual cube
        """
        return self._TopLayer

    def getMessages(self):
        """
        summarize all Messages from each solving steps
        :return: string of messages
        """
        message = ""
        message += self._WC.getMessages()
        message += self._FL.getMessages()
        message += self._SL.getMessages()
        message += self._TL.getMessages()
        message += self._LL.getMessages()
        message += "\n"
        return message

    def getLen(self):
        """
        adds up all moves
        :return: int of moves
        """
        length = 0
        length += self._WC.getLen()
        length += self._FL.getLen()
        length += self._SL.getLen()
        length += self._TL.getLen()
        length += self._LL.getLen()
        return length

    def _solveWC(self):
        """
        solves whitecross
        :return:
        """
        self._WC.canSolveCube()

    def _solveFL(self):
        """
        solves firstlayer
        :return:
        """
        self._FL.canSolveCube()

    def _solveSL(self):
        """
        solves secondlayer
        :return:
        """
        self._SL.canSolveCube()

    def _solveTL(self):
        """
        solves toplayer
        :return:
        """
        self._TL.canSolveCube()

    def _solveLL(self):
        """
        solves lastlayer
        :return:
        """
        self._LL.canSolveCube()
