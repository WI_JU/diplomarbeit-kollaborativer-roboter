import os
import cv2
import numpy as np

from Camera.CamerScan import CameraScan


class CameraPrediction:
    """ Tries to find all stones from the camera"""
    _camHeight = 1080
    _camWidth = 1920
    _imagesPath = r"Camera\SafeImages"
    _cameraScan = CameraScan()

    def takePicture(self, number=0, frame={'img': []}):
        """
        Makes picture at defined position
        :param number is the number of the current Picture default = 1
        :return bool If picture has been made succesfully
        """
        invalid = False
        img = frame.get('img')
        try:
            # print("got frame", number)
            cv2.imwrite(self._imagesPath + r"\\" + str(number - 1) + ".png", img)
        except:
            invalid = True
        return invalid

    def generatePictureArray(self):
        inputArray = []
        sortedArray = []
        list = os.listdir(self._imagesPath)
        if len(list) == 7:
            for pictureName in list:
                if pictureName != "0.png":
                    inputArray.append(self._cameraScan.cropImage(pictureName))
                os.remove(self._imagesPath+r"\\"+pictureName)
        # White -> Yellow -> Red -> Orange -> Blue -> Green
        # Green -> White -> Red -> Yellow -> Blue -> Orange
        sortedArray.append(np.rot90(inputArray[5], -1))  # rot 90deg
        sortedArray.append(inputArray[0])
        sortedArray.append(inputArray[2])
        sortedArray.append(inputArray[1])
        sortedArray.append(np.rot90(inputArray[4], -1))  # rot 90deg
        sortedArray.append(np.rot90(inputArray[3], 2))  # Rot 180deg
        return sortedArray

    def __cubeReorientation(self, virtualCube):
        steps = []
        whiteTurnPosition = 0
        for layer in virtualCube:
            if layer[1][1] == 'W':
                whiteTurnPosition = virtualCube.index(layer)

        steps.extend(self.__whiteTurnStepsOfPosition(whiteTurnPosition))
        turnedCube = self.__turnTotalVirtualCube(virtualCube, steps)

        greenTurnPosition = 0
        for layer in turnedCube:
            if layer[1][1] == 'G':
                greenTurnPosition = virtualCube.index(layer)
        steps.extend(self.__greenTurnStepsOfPosition(greenTurnPosition))

        return steps

    def __whiteTurnStepsOfPosition(self, currentPosition):
        steps = []
        if currentPosition == 0:
            steps.append('F')
        elif currentPosition == 2:
            steps.extend(['Li'])
        elif currentPosition == 3:
            steps.extend(['L', 'L'])
        elif currentPosition == 4:
            steps.extend(['Fi'])
        elif currentPosition == 5:
            steps.extend(['L'])
        return steps

    def __greenTurnStepsOfPosition(self, currentPosition):
        steps = []
        if currentPosition == 2:
            steps.extend(['U'])
        elif currentPosition == 4:
            steps.extend(['U', 'U'])
        elif currentPosition == 5:
            steps.extend(['Ui'])
        return steps

    def __turnTotalVirtualCube(self, virtualCube, steps):
        turnedCube = virtualCube.copy()
        for step in steps:
            if step == 'L':
                safeCube = turnedCube[1].copy()
                turnedCube[1] = turnedCube[5]
                turnedCube[5] = turnedCube[3]
                turnedCube[3] = turnedCube[2]
                turnedCube[2] = safeCube
            elif step == 'U':
                safeCube = turnedCube[0].copy()
                turnedCube[0] = turnedCube[2]
                turnedCube[2] = turnedCube[4]
                turnedCube[4] = turnedCube[5]
                turnedCube[5] = safeCube
            elif step == 'F':
                safeCube = turnedCube[0].copy()
                turnedCube[0] = turnedCube[3]
                turnedCube[3] = turnedCube[4]
                turnedCube[4] = turnedCube[1]
                turnedCube[1] = safeCube
            elif step == 'Li':
                safeCube = turnedCube[1].copy()
                turnedCube[1] = turnedCube[2]
                turnedCube[2] = turnedCube[3]
                turnedCube[3] = turnedCube[5]
                turnedCube[5] = safeCube
            elif step == 'Ui':
                safeCube = turnedCube[0].copy()
                turnedCube[0] = turnedCube[5]
                turnedCube[5] = turnedCube[4]
                turnedCube[4] = turnedCube[2]
                turnedCube[2] = safeCube
            elif step == 'Fi':
                safeCube = turnedCube[0].copy()
                turnedCube[0] = turnedCube[1]
                turnedCube[1] = turnedCube[4]
                turnedCube[4] = turnedCube[3]
                turnedCube[3] = safeCube
        return turnedCube

    def __sortVirtualCube(self, virtualCube):
        sortedCube = virtualCube.copy()
        for layer in virtualCube:
            if layer[1][1] == 'G':
                sortedCube[0] = layer
            elif layer[1][1] == 'W':
                sortedCube[1] = layer
            elif layer[1][1] == 'R':
                sortedCube[2] = layer
            elif layer[1][1] == 'Y':
                sortedCube[3] = layer
            elif layer[1][1] == 'B':
                sortedCube[4] = layer
            elif layer[1][1] == 'O':
                sortedCube[5] = layer

        return sortedCube
