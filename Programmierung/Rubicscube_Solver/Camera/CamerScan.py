import configparser
import os
import cv2
import numpy as np


class CameraScan:
    _camWidth = 1920
    _camHeight = 1080
    _picturePath = r".\Camera\SafeImages"
    _configPath = r".\Config\Config.ini"
    _config = configparser.ConfigParser()
    _config.read(_configPath)

    def __init__(self):
        self.DEBUGRGB = []
        self._basicCenter = []

    def _getDelta(sellf, countour):
        X = []
        Y = []
        for cnt in countour:
            X.append(cnt[0][0])
            Y.append(cnt[0][1])
        X = np.array(X)
        Y = np.array(Y)
        dx = np.max(X) - np.min(X)
        dy = np.max(Y) - np.min(Y)
        return dx, dy

    def _imageToRGB(self, image, piece=(0, 0)):
        """
        :return: rgb dominant color
        """

        data = np.reshape(image, (-1, 3))
        data = np.float32(data)
        criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 10, 1.0)
        flags = cv2.KMEANS_RANDOM_CENTERS
        compactness, labels, centers = cv2.kmeans(data, 1, None, criteria, 10, flags)
        centers = centers[0]
        self._basicCenter.append([int(centers[2]), int(centers[1]), int(centers[0])])
        return self._normalizeRGB([int(centers[2]), int(centers[1]), int(centers[0])], piece)  #

    def _normalizeRGB(self, imageRGB, piece=(0, 0)):
        """
            Converts name of file which represents stone on the side of the rubicscube to an array of RGB solution
            white   1   =   255 255 255
            green   2   =   0   255 0
            red     3   =   255 0   0
            orange  4   =   255 127 0
            blue    5   =   0   0   255
            yellow  6   =   255 255 0
            :param name: name of the file
            :return: neural Network output as array
            """
        span = 10
        upperMark = 135
        lowerMark = 90
        scale = 1
        add = 0
        r = imageRGB[0]
        g = imageRGB[1]
        b = imageRGB[2]
        self.DEBUGRGB.append(imageRGB)
        if r > upperMark and g > upperMark and b > upperMark:
            rgb = [255, 255, 255]  # White
        else:
            if max(imageRGB) < 255:
                scale = 255 / max(imageRGB)
            r = int(r * scale)
            g = int(g * scale)
            b = int(b * scale)
            if piece == (1, 1):
                b += -15
            rgb = [-1, -1, -1]

            if r > upperMark and g > upperMark and b < lowerMark:
                rgb = [255, 255, 0]  # Yellow
            elif r > lowerMark and g > lowerMark and r > g + 20:
                rgb = [255, 127, 0]  # Orange
            elif r > upperMark and g < lowerMark and b < lowerMark:
                rgb = [255, 0, 0]  # Red
            elif r < lowerMark and g > upperMark and b < lowerMark:
                rgb = [0, 255, 0]  # Green
            elif r < lowerMark and g < lowerMark and b > upperMark:
                rgb = [0, 0, 255]  # Blue
            else:
                if max(imageRGB) < 255:
                    add = 255 - max(imageRGB)
                r = imageRGB[0] + add
                g = imageRGB[1] + add
                b = imageRGB[2] + add
                if r > g + span and r > b + span:
                    rgb = [255, 0, 0]  # Red
                elif g > r + span and g > b + span:
                    rgb = [0, 255, 0]  # Green
                elif b > r + span and b > g + span:
                    rgb = [0, 0, 255]  # Blue
                elif r - g <= span and (b < r or b < g):
                    rgb = [255, 255, 0]  # Yellow
                elif r > g + span and b < r and b < g:
                    rgb = [255, 127, 0]  # Orange
                elif (0 <= r - g < span or 0 <= g - r < span) and (0 <= r - b < span or 0 <= g - b < span) and (
                        0 <= g - b < span or 0 <= b - g < span):
                    rgb = [255, 255, 255]  # White

        return rgb

    def _fileNameToRGBArray(self, fileName):
        """
        Converts name of file which represents stone on the side of the rubicscube to an array of RGB solution
        white   1   =   255 255 255
        green   2   =   0   255 0
        red     3   =   255 0   0
        orange  4   =   255 127 0
        blue    5   =   0   0   255
        yellow  6   =   255 255 0
        :param fileName: name of the file
        :return: neural Network output as array
        """
        name = fileName.split('_')[0][:]
        fileNameArray = []
        name = name.split(' ')[0]
        name = [int(numeric_string) for numeric_string in name]

        WHITE = [255, 255, 255]
        GREEN = [0, 255, 0]
        RED = [255, 0, 0]
        ORANGE = [255, 127, 0]
        BLUE = [0, 0, 255]
        YELLOW = [255, 255, 0]

        for number in name:
            if number == 1:
                fileNameArray.extend(WHITE)
            elif number == 2:
                fileNameArray.extend(GREEN)
            elif number == 3:
                fileNameArray.extend(RED)
            elif number == 4:
                fileNameArray.extend(ORANGE)
            elif number == 5:
                fileNameArray.extend(BLUE)
            elif number == 6:
                fileNameArray.extend(YELLOW)
        return fileNameArray

    def _cropCenter(self, image):
        x = int(image.shape[0] / 6)
        y = int(image.shape[1] / 6)
        w = int(image.shape[0] / 3) * 2
        h = int(image.shape[1] / 3) * 2
        return image[x:(w + x), y:(h + y)]

    def checkAllImages(self, PATH, DEBUG=False):
        # Scan all the directories and create a list of labels
        # labels = os.listdir(os.path.join(path, dataset))
        # Create lists for samples and labels
        labels = os.listdir(PATH)
        Returns = []
        # For each label folder
        # print("started Checking: ")
        for label in labels:
            # And for each image in given folder
            labelReturns = []
            for file in os.listdir(os.path.join(PATH, label)):
                labelReturns.append(self.checkImage(os.path.join(PATH, label), file, DEBUG=DEBUG))
            Wrongs = 0
            for returns in labelReturns:
                if not returns:
                    Wrongs += 1
            acc = ((len(labelReturns) - Wrongs) / len(labelReturns)) * 100
            # print("Done " + str(label) + " Accuracy: " + str(acc) + " wrongs: " + str(Wrongs) + "/" + str(
            #     len(labelReturns)))
            Returns.extend(labelReturns)
            break
        Wrongs = 0
        for returns in Returns:
            if not returns:
                Wrongs += 1
        acc = ((len(Returns) - Wrongs) / len(Returns)) * 100
        # print("Accuracy :" + str(acc))

    def checkImage(self, PATH, filename, DEBUG=False):
        imageColor = cv2.imread(os.path.join(PATH, filename))
        image = cv2.imread(os.path.join(PATH, filename))
        image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        x, y = cv2.Sobel(image, cv2.CV_16S, 1, 0), cv2.Sobel(image, cv2.CV_16S, 0, 1)
        image = cv2.convertScaleAbs(cv2.subtract(x, y))
        image = cv2.blur(image, (1, 1))

        ret, image = cv2.threshold(image, 30, 255, cv2.THRESH_BINARY_INV)
        ret, image = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY_INV)
        ret, image = cv2.threshold(image, 127, 255, cv2.THRESH_BINARY_INV)

        image = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_MEAN_C, \
                                      cv2.THRESH_BINARY, 11, 2)

        image = cv2.adaptiveThreshold(image, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, \
                                      cv2.THRESH_BINARY, 11, 2)
        mask = image.copy()

        contours, _ = cv2.findContours(mask, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)

        MainGrid = contours[0]
        grDx = 0
        grDy = 0
        for cnt in contours:
            dx, dy = self._getDelta(cnt)
            if dx <= len(mask[0]) - 5 and dy <= len(mask[1]) - 5:
                if dx > grDx and dy > grDy:
                    MainGrid = cnt
                    grDx = dx
                    grDy = dy
        x, y, w, h = cv2.boundingRect(MainGrid)
        cropped_image = imageColor[y:(y + h), x:(x + w)]  # Slicing to crop the image

        w = int(cropped_image.shape[0] / 3)
        h = int(cropped_image.shape[1] / 3)

        singlePieces = []
        ColorsArray = []
        self._basicCenter = []
        # crop into 9 pieces
        for i in range(3):
            for j in range(3):
                image_piece = cropped_image[w * i:(w + w * i), h * j:(h + h * j)]

                increase = 30
                v = image_piece[:, :, 2]
                v = np.where(v <= 255 - increase, v + increase, 255)
                image_piece[:, :, 2] = v
                image_piece = self._cropCenter(image_piece)
                singlePieces.append(image_piece)
                ColorsArray.extend(self._imageToRGB(image_piece, (i, j)))
        ColorsArray = np.array(ColorsArray)
        ColorsArray = np.split(ColorsArray, 9)
        fileArray = np.array(self._fileNameToRGBArray(filename))
        fileArray = np.split(fileArray, 9)
        if not DEBUG:
            for equals in np.equal(ColorsArray, fileArray):
                if not equals:
                    return False
            return True
        else:
            valid = True
            index = []
            for equals in np.equal(ColorsArray, fileArray):
                for eq in equals:
                    if not eq:
                        valid = False
            if not valid:
                for i in range(len(singlePieces)):
                    # print(self._basicCenter[i], fileArray[i], ColorsArray[i], i)
                    pass
                # with open('colors.txt', "a") as fhandle:
                #     for line in colorText:
                #         fhandle.write(f'{line}\n')
                cv2.destroyAllWindows()
                cv2.imshow("image", imageColor)
                for i in range(len(singlePieces)):
                    cv2.imshow("sP" + str(i), cv2.resize(singlePieces[i], (200, 200)))
                if len(index) != 0:
                    for i in index:
                        # print(int(i / 3), self.DEBUGRGB[int(i / 3)])
                        # print(int(i / 3), fileArray[i], fileArray[i + 1], fileArray[i + 1])
                        # print(int(i / 3), self._basicCenter[int(i / 3)])
                        pass
                self._basicCenter = []
                cv2.waitKey(0)
            return valid

    @staticmethod
    def _createRechtangleOf2Points(P1, P2):
        x1, x2, y1, y2 = 0, 0, 0, 0
        if P1[0] < P2[0]:
            x1, x2 = P1[0], P2[0]
        else:
            x1, x2 = P2[0], P1[0]
        if P1[1] < P2[1]:
            y1, y2 = P1[1], P2[1]
        else:
            y1, y2 = P2[1], P1[1]
        return x1, x2, y1, y2

    def _getConfigPoints(self):
        Point1 = self._config['camera']['point1'].split(',')
        Point2 = self._config['camera']['point2'].split(',')
        Point1 = [int(Point1[0]),int(Point1[1])]
        Point2 = [int(Point2[0]), int(Point2[1])]
        return Point1, Point2

    def cropImage(self, pictureName):
        Point1, Point2 = self._getConfigPoints()
        img = cv2.imread(self._picturePath +r"\\" +pictureName)

        x1, x2, y1, y2 = self._createRechtangleOf2Points(Point1, Point2)
        cropped_image = img[y1:y2, x1:x2]  # Slicing to crop the image
        w = int(cropped_image.shape[0] / 3)
        h = int(cropped_image.shape[1] / 3)

        singlePieces = []
        ColorsArray = []
        # crop into 9 pieces
        for i in range(3):
            for j in range(3):
                image_piece = cropped_image[w * i:(w + w * i), h * j:(h + h * j)]

                increase = 30
                v = image_piece[:, :, 2]
                v = np.where(v <= 255 - increase, v + increase, 255)
                image_piece[:, :, 2] = v
                image_piece = self._cropCenter(image_piece)
                singlePieces.append(image_piece)
                ColorsArray.extend(self._imageToRGB(image_piece, (i, j)))
        ColorsArray = np.array(ColorsArray)
        ColorsArray = np.split(ColorsArray, 9)
        return self._convertRGBtoSideColor(ColorsArray)

    @staticmethod
    def _convertRGBtoSideColor(ColorsArray):
        SideColors = []

        WHITE = [255, 255, 255]
        GREEN = [0, 255, 0]
        RED = [255, 0, 0]
        ORANGE = [255, 127, 0]
        BLUE = [0, 0, 255]
        YELLOW = [255, 255, 0]

        for i in range(len(ColorsArray)):
            color = ColorsArray[i]
            if (color == np.array(WHITE)).all():
                SideColors.append("W")
            elif (color == np.array(GREEN)).all():
                SideColors.append("G")
            elif (color == np.array(RED)).all():
                SideColors.append("R")
            elif(color == np.array(ORANGE)).all():
                SideColors.append("O")
            elif (color == np.array(BLUE)).all():
                SideColors.append("B")
            elif (color == np.array(YELLOW)).all():
                SideColors.append("Y")
        SideColors = np.array(SideColors)
        SideColors = np.split(SideColors, 3)
        return SideColors

