import configparser
import gc
import queue
import sys
from time import sleep

import cv2
import numpy as np
from PyQt6 import QtCore, QtGui, QtWidgets
from PyQt6.QtCore import QThread, QSettings
from PyQt6.QtWidgets import QDialog, QDialogButtonBox, QVBoxLayout, QLabel, QMessageBox

from GUI.Dialog import Ui_Dialog
from Utilities.SolvedArrays import solvedArrays
from Logic.RCS_Socket import Communication

ConfigPATH = r"Config\Config.ini"
settings = QSettings(ConfigPATH, QSettings.Format.IniFormat)

config = configparser.ConfigParser()
config.read(ConfigPATH)
camWidth = int(config["cameraSystem"]['camWidth'])
camHeight = int(config["cameraSystem"]['camHeight'])
global isCalibrated, cantGrabCam
global cameraRunning, communicationRunning, communicationData, communicationObject, hasOpendDialog
global FrameUsed, Frame
Frame = []
cameraRunning, communicationRunning, hasOpendDialog, FrameUsed = False, True, False, True
communicationData = {
        'state': 1,
        'stepIndex': 0,
        'solvedArray': [],
        'virtualCopy': solvedArrays().getSolved(),
        'isRunning': False,
        'validate': 0
    }
isCalibrated, cantGrabCam = True, True
DEBUG = False

# App init
app = QtWidgets.QApplication(sys.argv)
screen = app.primaryScreen()

class cameraThread(QThread):
    def run(self):
        global cantGrabCam
        global FrameUsed, Frame
        # print("Starting Camera")
        cam = 0
        capture = cv2.VideoCapture(cam)
        capture.set(cv2.CAP_PROP_FRAME_WIDTH, camWidth)
        capture.set(cv2.CAP_PROP_FRAME_HEIGHT, camHeight)
        capture.set(cv2.CAP_PROP_FPS, 30)
        while cameraRunning:
            ret, img = capture.read()
            if ret:
                cantGrabCam = False
                capture.grab()
                retval, img = capture.retrieve(cam)
                if FrameUsed:
                    Frame = img.copy()
                    FrameUsed = False
            else:
                cantGrabCam = True


    def quit(self):
        global cameraRunning
        cameraRunning = False
        # print("Camera stopped")


class communicationThread(QThread):
    def run(self):
        global isCalibrated, communicationRunning, communicationData, communicationObject, Frame
        # print("Starting Communication")
        communication = Communication()
        communicationObject = communication
        while communicationRunning:
            if isCalibrated:
                communicationData = communication.communicate(frame=Frame, DEBUG=DEBUG)

    def quit(self):
        # print("CommunicationWorker stopped")
        pass


class OwnImageWidget(QtWidgets.QWidget):
    def __init__(self, parent=None):
        super(OwnImageWidget, self).__init__(parent)
        self.image = None

    def setImage(self, image):
        self.image = image
        sz = image.size()
        self.setMinimumSize(sz)
        self.update()

    def paintEvent(self, event):
        qp = QtGui.QPainter()
        qp.begin(self)
        if self.image:
            qp.drawImage(QtCore.QPoint(0, 0), self.image)
        qp.end()

    def __cubeInit(self):
        faktorWidth, faktorHeight = self.faktorWidth, self.faktorHeight
        self.virtualCube = QtWidgets.QFrame()
        self.virtualCube.setGeometry(QtCore.QRect(int(0 * faktorWidth), int(0 * faktorHeight),
                                                  int(600 * faktorWidth), int(450 * faktorHeight)))
        self.virtualCube.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.virtualCube.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.virtualCube.setObjectName("cubeArray")

        self.A1 = QtWidgets.QFrame(self.virtualCube)
        self.A1.setGeometry(QtCore.QRect(0, int(150 * faktorHeight),
                                         int(150 * faktorWidth), int(150 * faktorHeight)))
        self.A1.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.A1.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.A1.setObjectName("A1")

        self.A2 = QtWidgets.QFrame(self.virtualCube)
        self.A2.setGeometry(QtCore.QRect(int(150 * faktorWidth), 0,
                                         int(150 * faktorWidth), int(150 * faktorHeight)))
        self.A2.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.A2.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.A2.setObjectName("A2")

        self.A3 = QtWidgets.QFrame(self.virtualCube)
        self.A3.setGeometry(QtCore.QRect(int(150 * faktorWidth), int(150 * faktorHeight),
                                         int(150 * faktorWidth), int(150 * faktorHeight)))
        self.A3.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.A3.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.A3.setObjectName("A3")

        self.A4 = QtWidgets.QFrame(self.virtualCube)
        self.A4.setGeometry(QtCore.QRect(int(150 * faktorWidth), int(300 * faktorHeight),
                                         int(150 * faktorWidth), int(150 * faktorHeight)))
        self.A4.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.A4.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.A4.setObjectName("A4")

        self.A5 = QtWidgets.QFrame(self.virtualCube)
        self.A5.setGeometry(QtCore.QRect(int(300 * faktorWidth), int(150 * faktorHeight),
                                         int(150 * faktorWidth), int(150 * faktorHeight)))
        self.A5.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.A5.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.A5.setObjectName("A5")

        self.A6 = QtWidgets.QFrame(self.virtualCube)
        self.A6.setGeometry(QtCore.QRect(int(450 * faktorWidth), int(150 * faktorHeight),
                                         int(150 * faktorWidth), int(150 * faktorHeight)))
        self.A6.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.A6.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.A6.setObjectName("A6")

        self.A1_Parts = self.__generateGUIVirtualCubeArray("A1", self.A1, faktorWidth, faktorHeight)
        self.A2_Parts = self.__generateGUIVirtualCubeArray("A2", self.A2, faktorWidth, faktorHeight)
        self.A3_Parts = self.__generateGUIVirtualCubeArray("A3", self.A3, faktorWidth, faktorHeight)
        self.A4_Parts = self.__generateGUIVirtualCubeArray("A4", self.A4, faktorWidth, faktorHeight)
        self.A5_Parts = self.__generateGUIVirtualCubeArray("A5", self.A5, faktorWidth, faktorHeight)
        self.A6_Parts = self.__generateGUIVirtualCubeArray("A6", self.A6, faktorWidth, faktorHeight)

        for i in range(len(self.A1_Parts)):
            self.A1_Parts[i][0].setStyleSheet(f'color: white; background:  white')
            self.A2_Parts[i][0].setStyleSheet(f'color: white; background:  white')
            self.A3_Parts[i][0].setStyleSheet(f'color: white; background:  white')
            self.A4_Parts[i][0].setStyleSheet(f'color: white; background:  white')
            self.A5_Parts[i][0].setStyleSheet(f'color: white; background:  white')
            self.A6_Parts[i][0].setStyleSheet(f'color: white; background:  white')
        self.A_Parts = [self.A1_Parts, self.A2_Parts, self.A3_Parts, self.A4_Parts, self.A5_Parts, self.A6_Parts]
        testCube = [[['R', 'Y', 'R'],
                     ['Y', 'G', 'Y'],
                     ['R', 'O', 'O']],
                    [['Y', 'R', 'W'],
                     ['B', 'W', 'G'],
                     ['Y', 'Y', 'G']],
                    [['G', 'G', 'O'],
                     ['O', 'R', 'B'],
                     ['W', 'W', 'B']],
                    [['G', 'B', 'O'],
                     ['W', 'Y', 'W'],
                     ['W', 'W', 'O']],
                    [['Y', 'R', 'G'],
                     ['O', 'B', 'G'],
                     ['W', 'R', 'Y']],
                    [['R', 'B', 'B'],
                     ['O', 'O', 'R'],
                     ['B', 'G', 'B']]]
        # testCube = Utilities.SolvedArrays.solvedArrays().getSolved()
        self.__drawCube(testCube)
        self.layout.addWidget(self.virtualCube)

    def __buttonInit(self):
        faktorWidth, faktorHeight = self.faktorWidth, self.faktorHeight
        self.validateBtn = QtWidgets.QPushButton()
        self.validateBtn.setGeometry(QtCore.QRect(int(100 * faktorWidth), int(50 * faktorHeight),
                                                  int(200 * faktorWidth), int(100 * faktorHeight)))

        self.validateBtn.setCursor(QtGui.QCursor(QtCore.Qt.CursorShape.PointingHandCursor))
        self.validateBtn.setObjectName("validate")
        self.layout.addWidget(self.validateBtn)
        self.saveBtn = QtWidgets.QPushButton()
        self.saveBtn.setGeometry(QtCore.QRect(int(100 * faktorWidth), int(50 * faktorHeight),
                                                  int(500 * faktorWidth), int(50 * faktorHeight)))

        self.saveBtn.setCursor(QtGui.QCursor(QtCore.Qt.CursorShape.PointingHandCursor))
        self.saveBtn.setObjectName("save")
        self.validateBtn.clicked.connect(self.__validate)
        self.validateBtn.setText("Validate Cube")
        self.saveBtn.setEnabled(False)
        self.layout.addWidget(self.saveBtn)

    def __validate(self):
        pass


    def __drawCube(self, virtualCube):
        colors = {
            'G': 'green',
            'W': 'lightgray',
            'R': 'red',
            'Y': 'yellow',
            'B': 'blue',
            'O': 'orange'
        }
        for i in range(len(virtualCube)):
            drawPart = np.array(np.array(virtualCube[i]).reshape(9, 1))
            for j in range(len(drawPart)):
                self.A_Parts[i][j][0].setStyleSheet(f'color: white; background:  {colors[drawPart[j][0]]}')

    def __generateGUIVirtualCubeArray(self, name, frame, faktorWidth, faktorHeight):
            parts = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
            for i in range(3):
                for j in range(3):
                    parts[i][j] = QtWidgets.QLabel(frame)
                    parts[i][j].setGeometry(QtCore.QRect(int(50 * j * faktorWidth), int(50 * i * faktorHeight),
                                                         int(50 * faktorWidth), int(50 * faktorHeight)))
                    parts[i][j].setText("")
                    parts[i][j].setObjectName(name + "_" + str(i) + str(j))
            return np.array(parts).reshape((9, 1))


class Ui_MainWindow(object):
    global isCalibrated
    try:
        Point1 = [int(point) for point in settings.value('camera/point1')]
        Point2 = [int(point) for point in settings.value('camera/point2')]
    except:
        isCalibrated = False
        Point1 = [100, 100]
        Point2 = [camWidth - 100, camHeight - 100]
    RectangleColor = (0, 0, 255)
    RectangleThikness = 5
    activeP1 = False
    activeP2 = False
    frozen = False

    def setupUi(self, MainWindow, screenWidth, screenHeight):
        global cameraRunning, calibPoint

        calibPoint = 0
        cameraRunning = True
        # print(screenWidth, screenHeight)
        faktorWidth = screenWidth / 1920
        faktorHeight = screenHeight / 1080
        faktorScale = (screenWidth / screenHeight) / (1920 / 1080)
        self.faktorWidth = faktorWidth
        self.faktorHeight = faktorHeight
        self.faktorScale = faktorScale
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(int(1920 * faktorWidth), int(1080 * faktorHeight))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Expanding,
                                           QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(MainWindow.sizePolicy().hasHeightForWidth())
        MainWindow.setSizePolicy(sizePolicy)
        MainWindow.setMouseTracking(True)
        MainWindow.setWindowOpacity(2.0)
        MainWindow.setLayoutDirection(QtCore.Qt.LayoutDirection.LeftToRight)
        MainWindow.setAutoFillBackground(True)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.tabWidget = QtWidgets.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(0, 0, int(1920 * faktorWidth), int(1080 * faktorHeight)))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Expanding,
                                           QtWidgets.QSizePolicy.Policy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.tabWidget.sizePolicy().hasHeightForWidth())
        self.tabWidget.setSizePolicy(sizePolicy)
        font = QtGui.QFont()
        font.setPointSize(int(12 * faktorScale))
        self.tabWidget.setFont(font)
        self.tabWidget.setObjectName("tabWidget")
        self.tab = QtWidgets.QWidget()
        self.tab.setObjectName("tab")
        self.label = QtWidgets.QLabel(self.tab)
        self.label.setGeometry(QtCore.QRect(int(660 * faktorWidth), int(290 * faktorHeight),
                                            int(600 * faktorWidth), int(500 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(26)
        font.setBold(True)
        font.setWeight(75)
        font.setStrikeOut(False)
        font.setStyleStrategy(QtGui.QFont.StyleStrategy.NoAntialias)
        self.label.setFont(font)
        self.label.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.label.setAutoFillBackground(False)
        self.label.setLocale(QtCore.QLocale(QtCore.QLocale.Language.English,
                                            QtCore.QLocale.Country.Germany))
        self.label.setTextFormat(QtCore.Qt.TextFormat.AutoText)
        self.label.setScaledContents(False)
        self.label.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.label.setWordWrap(False)
        self.label.setObjectName("label")
        self.tabWidget.addTab(self.tab, "")
        self.tab_2 = QtWidgets.QWidget()
        self.tab_2.setObjectName("tab_2")
        self.Buttons = QtWidgets.QFrame(self.tab_2)
        self.Buttons.setGeometry(QtCore.QRect(0, 0, int(1920 * faktorWidth),
                                              int(100 * faktorHeight)))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Expanding,
                                           QtWidgets.QSizePolicy.Policy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.Buttons.sizePolicy().hasHeightForWidth())
        self.Buttons.setSizePolicy(sizePolicy)
        self.Buttons.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.Buttons.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.Buttons.setObjectName("Buttons")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.Buttons)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.takePicture = QtWidgets.QPushButton(self.Buttons)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Preferred,
                                           QtWidgets.QSizePolicy.Policy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.takePicture.sizePolicy().hasHeightForWidth())
        self.takePicture.setSizePolicy(sizePolicy)
        self.takePicture.setMaximumSize(QtCore.QSize(int(200 * faktorWidth), int(50 * faktorHeight)))
        self.takePicture.setCursor(QtGui.QCursor(QtCore.Qt.CursorShape.PointingHandCursor))
        self.takePicture.setObjectName("validateBtn")
        self.horizontalLayout.addWidget(self.takePicture)
        self.setPoint1 = QtWidgets.QPushButton(self.Buttons)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Minimum,
                                           QtWidgets.QSizePolicy.Policy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.setPoint1.sizePolicy().hasHeightForWidth())
        self.setPoint1.setSizePolicy(sizePolicy)
        self.setPoint1.setMaximumSize(QtCore.QSize(int(200 * faktorWidth), int(50 * faktorHeight)))
        self.setPoint1.setCursor(QtGui.QCursor(QtCore.Qt.CursorShape.PointingHandCursor))
        self.setPoint1.setObjectName("setPoint1")
        self.horizontalLayout.addWidget(self.setPoint1)
        self.setPoint2 = QtWidgets.QPushButton(self.Buttons)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Minimum,
                                           QtWidgets.QSizePolicy.Policy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.setPoint2.sizePolicy().hasHeightForWidth())
        self.setPoint2.setSizePolicy(sizePolicy)
        self.setPoint2.setMaximumSize(QtCore.QSize(int(200 * faktorWidth), int(50 * faktorHeight)))
        self.setPoint2.setCursor(QtGui.QCursor(QtCore.Qt.CursorShape.PointingHandCursor))
        self.setPoint2.setObjectName("setPoint2")
        self.horizontalLayout.addWidget(self.setPoint2)
        self.setCalibration = QtWidgets.QPushButton(self.Buttons)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Minimum, QtWidgets.QSizePolicy.Policy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.setCalibration.sizePolicy().hasHeightForWidth())
        self.setCalibration.setSizePolicy(sizePolicy)
        self.setCalibration.setMaximumSize(QtCore.QSize(int(200 * faktorWidth), int(50 * faktorHeight)))
        self.setCalibration.setCursor(QtGui.QCursor(QtCore.Qt.CursorShape.PointingHandCursor))
        self.setCalibration.setObjectName("setCalibration")
        self.horizontalLayout.addWidget(self.setCalibration)
        self.VideoCapture = QtWidgets.QFrame(self.tab_2)
        self.VideoCapture.setGeometry(QtCore.QRect(int(200 * faktorWidth), int(110 * faktorHeight),
                                                   int(1050 * faktorWidth), int(850 * faktorHeight)))
        self.VideoCapture.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.VideoCapture.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.VideoCapture.setObjectName("VideoCapture")
        self.horizontalSlider = QtWidgets.QSlider(self.VideoCapture)
        self.horizontalSlider.setGeometry(QtCore.QRect(0, int(820 * faktorHeight),
                                                       int(960 * faktorWidth), int(20 * faktorHeight)))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Expanding,
                                           QtWidgets.QSizePolicy.Policy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.horizontalSlider.sizePolicy().hasHeightForWidth())
        self.horizontalSlider.setSizePolicy(sizePolicy)
        self.horizontalSlider.setCursor(QtGui.QCursor(QtCore.Qt.CursorShape.PointingHandCursor))
        self.horizontalSlider.setOrientation(QtCore.Qt.Orientation.Horizontal)
        self.horizontalSlider.setObjectName("horizontalSlider")
        self.groupBox = QtWidgets.QGroupBox(self.VideoCapture)
        self.groupBox.setGeometry(QtCore.QRect(0, 0, int(1000 * faktorWidth), int(800 * faktorHeight)))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.groupBox.sizePolicy().hasHeightForWidth())
        self.groupBox.setSizePolicy(sizePolicy)
        self.groupBox.setObjectName("groupBox")
        self.ImgWidget = QtWidgets.QWidget(self.groupBox)
        self.ImgWidget.setGeometry(QtCore.QRect(int(20 * faktorWidth), int(30 * faktorHeight),
                                                int(960 * faktorWidth), int(740 * faktorHeight)))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Preferred,
                                           QtWidgets.QSizePolicy.Policy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.ImgWidget.sizePolicy().hasHeightForWidth())
        self.ImgWidget.setSizePolicy(sizePolicy)
        self.ImgWidget.setObjectName("ImgWidget")

        self.verticalSlider = QtWidgets.QSlider(self.VideoCapture)
        self.verticalSlider.setGeometry(QtCore.QRect(int(1020 * faktorWidth), int(20 * faktorHeight),
                                                     int(20 * faktorWidth), int(740 * faktorHeight)))
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Policy.Fixed, QtWidgets.QSizePolicy.Policy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.verticalSlider.sizePolicy().hasHeightForWidth())
        self.verticalSlider.setSizePolicy(sizePolicy)
        self.verticalSlider.setCursor(QtGui.QCursor(QtCore.Qt.CursorShape.PointingHandCursor))
        self.verticalSlider.setOrientation(QtCore.Qt.Orientation.Vertical)
        self.verticalSlider.setInvertedAppearance(True)
        self.verticalSlider.setObjectName("verticalSlider")
        self.PointHolder = QtWidgets.QFrame(self.tab_2)
        self.PointHolder.setEnabled(True)
        self.PointHolder.setGeometry(QtCore.QRect(int(1300 * faktorWidth), int(120 * faktorHeight),
                                                  int(250 * faktorWidth), int(400 * faktorHeight)))
        self.PointHolder.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.PointHolder.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.PointHolder.setObjectName("PointHolder")
        self.P1Number_Frame = QtWidgets.QFrame(self.PointHolder)
        self.P1Number_Frame.setGeometry(QtCore.QRect(0, 0, int(250 * faktorWidth),
                                                     int(100 * faktorHeight)))
        self.P1Number_Frame.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.P1Number_Frame.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.P1Number_Frame.setObjectName("P1Number_Frame")
        self.P1_Y = QtWidgets.QLabel(self.P1Number_Frame)
        self.P1_Y.setGeometry(QtCore.QRect(int(130 * faktorWidth), int(50 * faktorHeight),
                                           int(30 * faktorWidth), int(50 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.P1_Y.setFont(font)
        self.P1_Y.setObjectName("P1_Y")
        self.P1Number_X = QtWidgets.QLabel(self.P1Number_Frame)
        self.P1Number_X.setGeometry(QtCore.QRect(int(40 * faktorWidth), int(50 * faktorHeight),
                                                 int(75 * faktorWidth), int(50 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.P1Number_X.setFont(font)
        self.P1Number_X.setObjectName("P1Number_X")
        self.P1_X = QtWidgets.QLabel(self.P1Number_Frame)
        self.P1_X.setGeometry(QtCore.QRect(int(5 * faktorWidth), int(50 * faktorHeight),
                                           int(30 * faktorWidth), int(50 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.P1_X.setFont(font)
        self.P1_X.setObjectName("P1_X")
        self.P1Number_Y = QtWidgets.QLabel(self.P1Number_Frame)
        self.P1Number_Y.setGeometry(QtCore.QRect(int(165 * faktorWidth), int(50 * faktorHeight),
                                                 int(75 * faktorWidth), int(50 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.P1Number_Y.setFont(font)
        self.P1Number_Y.setObjectName("P1Number_Y")
        self.P1_Label = QtWidgets.QLabel(self.P1Number_Frame)
        self.P1_Label.setGeometry(QtCore.QRect(0, 0, int(200 * faktorWidth), int(50 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.P1_Label.setFont(font)
        self.P1_Label.setObjectName("P1_Label")
        self.P2Number_Frame = QtWidgets.QFrame(self.PointHolder)
        self.P2Number_Frame.setGeometry(
            QtCore.QRect(0, int(100 * faktorHeight), int(250 * faktorWidth), int(100 * faktorHeight)))
        self.P2Number_Frame.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.P2Number_Frame.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.P2Number_Frame.setObjectName("P2Number_Frame")
        self.P2_Y = QtWidgets.QLabel(self.P2Number_Frame)
        self.P2_Y.setGeometry(QtCore.QRect(int(130 * faktorWidth), int(50 * faktorHeight),
                                           int(30 * faktorWidth), int(50 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.P2_Y.setFont(font)
        self.P2_Y.setObjectName("P2_Y")
        self.P2Number_X = QtWidgets.QLabel(self.P2Number_Frame)
        self.P2Number_X.setGeometry(QtCore.QRect(int(40 * faktorWidth), int(50 * faktorHeight),
                                                 int(75 * faktorWidth), int(50 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.P2Number_X.setFont(font)
        self.P2Number_X.setObjectName("P2Number_X")
        self.P2_X = QtWidgets.QLabel(self.P2Number_Frame)
        self.P2_X.setGeometry(QtCore.QRect(int(5 * faktorWidth), int(50 * faktorHeight),
                                           int(30 * faktorWidth), int(50 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.P2_X.setFont(font)
        self.P2_X.setObjectName("P2_X")
        self.P2Number_Y = QtWidgets.QLabel(self.P2Number_Frame)
        self.P2Number_Y.setGeometry(QtCore.QRect(int(165 * faktorWidth), int(50 * faktorHeight),
                                                 int(75 * faktorWidth), int(50 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.P2Number_Y.setFont(font)
        self.P2Number_Y.setObjectName("P2Number_Y")
        self.P2_Label = QtWidgets.QLabel(self.P2Number_Frame)
        self.P2_Label.setGeometry(QtCore.QRect(0, 0, int(200 * faktorWidth), int(50 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(18)
        self.P2_Label.setFont(font)
        self.P2_Label.setObjectName("P2_Label")
        self.tabWidget.addTab(self.tab_2, "")
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, int(1920 * faktorWidth), int(18 * faktorHeight)))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.CommandBoard = QtWidgets.QFrame(self.tab)
        self.CommandBoard.setGeometry(QtCore.QRect(int(1000 * faktorWidth), int(50 * faktorHeight),
                                                   int(800 * faktorWidth), int(800 * faktorHeight)))
        self.CommandBoard.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.CommandBoard.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.CommandBoard.setObjectName("CommandBoard")
        self.isRunning = QtWidgets.QLabel(self.CommandBoard)
        self.isRunning.setGeometry(QtCore.QRect(0, int(10 * faktorHeight),
                                                int(800 * faktorWidth), int(150 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(int(100*faktorScale))
        font.setBold(True)
        font.setWeight(75)
        font.setStrikeOut(False)
        font.setStyleStrategy(QtGui.QFont.StyleStrategy.NoAntialias)
        self.isRunning.setFont(font)
        self.isRunning.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.isRunning.setAutoFillBackground(False)
        self.isRunning.setLocale(QtCore.QLocale(QtCore.QLocale.Language.English, QtCore.QLocale.Country.Germany))
        self.isRunning.setTextFormat(QtCore.Qt.TextFormat.AutoText)
        self.isRunning.setScaledContents(False)
        self.isRunning.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.isRunning.setWordWrap(False)
        self.isRunning.setObjectName("isRunning")
        self.state = QtWidgets.QLabel(self.CommandBoard)
        self.state.setGeometry(QtCore.QRect(0, int(160 * faktorHeight),
                                            int(800 * faktorWidth), int(150 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(int(36*faktorScale))
        font.setBold(True)
        font.setWeight(75)
        font.setStrikeOut(False)
        font.setStyleStrategy(QtGui.QFont.StyleStrategy.NoAntialias)
        self.state.setFont(font)
        self.state.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.state.setAutoFillBackground(False)
        self.state.setLocale(QtCore.QLocale(QtCore.QLocale.Language.English, QtCore.QLocale.Country.Germany))
        self.state.setText("")
        self.state.setTextFormat(QtCore.Qt.TextFormat.AutoText)
        self.state.setScaledContents(False)
        self.state.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.state.setWordWrap(False)
        self.state.setObjectName("state")
        self.solvedArray = QtWidgets.QLabel(self.CommandBoard)
        self.solvedArray.setGeometry(QtCore.QRect(0, int(320 * faktorHeight),
                                                  int(800 * faktorWidth), int(300 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(int(11*faktorScale))
        font.setBold(True)
        font.setWeight(75)
        font.setStrikeOut(False)
        font.setStyleStrategy(QtGui.QFont.StyleStrategy.NoAntialias)
        self.solvedArray.setFont(font)
        self.solvedArray.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.solvedArray.setAutoFillBackground(False)
        self.solvedArray.setLocale(QtCore.QLocale(QtCore.QLocale.Language.English, QtCore.QLocale.Country.Germany))
        self.solvedArray.setText("")
        self.solvedArray.setTextFormat(QtCore.Qt.TextFormat.AutoText)
        self.solvedArray.setScaledContents(False)
        self.solvedArray.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.solvedArray.setWordWrap(True)
        self.solvedArray.setObjectName("solvedArray")

        # Self Programmed
        self.virtualCube = QtWidgets.QFrame(self.tab)
        self.virtualCube.setGeometry(QtCore.QRect(int(100 * faktorWidth), int(50 * faktorHeight),
                                                  int(600 * faktorWidth), int(450 * faktorHeight)))
        self.virtualCube.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.virtualCube.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.virtualCube.setObjectName("cubeArray")

        self.A1 = QtWidgets.QFrame(self.virtualCube)
        self.A1.setGeometry(QtCore.QRect(0, int(150 * faktorHeight),
                                         int(150 * faktorWidth), int(150 * faktorHeight)))
        self.A1.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.A1.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.A1.setObjectName("A1")

        self.A2 = QtWidgets.QFrame(self.virtualCube)
        self.A2.setGeometry(QtCore.QRect(int(150 * faktorWidth), 0,
                                         int(150 * faktorWidth), int(150 * faktorHeight)))
        self.A2.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.A2.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.A2.setObjectName("A2")

        self.A3 = QtWidgets.QFrame(self.virtualCube)
        self.A3.setGeometry(QtCore.QRect(int(150 * faktorWidth), int(150 * faktorHeight),
                                         int(150 * faktorWidth), int(150 * faktorHeight)))
        self.A3.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.A3.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.A3.setObjectName("A3")

        self.A4 = QtWidgets.QFrame(self.virtualCube)
        self.A4.setGeometry(QtCore.QRect(int(150 * faktorWidth), int(300 * faktorHeight),
                                         int(150 * faktorWidth), int(150 * faktorHeight)))
        self.A4.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.A4.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.A4.setObjectName("A4")

        self.A5 = QtWidgets.QFrame(self.virtualCube)
        self.A5.setGeometry(QtCore.QRect(int(300 * faktorWidth), int(150 * faktorHeight),
                                         int(150 * faktorWidth), int(150 * faktorHeight)))
        self.A5.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.A5.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.A5.setObjectName("A5")

        self.A6 = QtWidgets.QFrame(self.virtualCube)
        self.A6.setGeometry(QtCore.QRect(int(450 * faktorWidth), int(150 * faktorHeight),
                                         int(150 * faktorWidth), int(150 * faktorHeight)))
        self.A6.setFrameShape(QtWidgets.QFrame.Shape.StyledPanel)
        self.A6.setFrameShadow(QtWidgets.QFrame.Shadow.Raised)
        self.A6.setObjectName("A6")

        self.A1_Parts = self.__generateGUIVirtualCubeArray("A1", self.A1, faktorWidth, faktorHeight)
        self.A2_Parts = self.__generateGUIVirtualCubeArray("A2", self.A2, faktorWidth, faktorHeight)
        self.A3_Parts = self.__generateGUIVirtualCubeArray("A3", self.A3, faktorWidth, faktorHeight)
        self.A4_Parts = self.__generateGUIVirtualCubeArray("A4", self.A4, faktorWidth, faktorHeight)
        self.A5_Parts = self.__generateGUIVirtualCubeArray("A5", self.A5, faktorWidth, faktorHeight)
        self.A6_Parts = self.__generateGUIVirtualCubeArray("A6", self.A6, faktorWidth, faktorHeight)

        for i in range(len(self.A1_Parts)):
            self.A1_Parts[i][0].setStyleSheet(f'color: white; background:  white')
            self.A2_Parts[i][0].setStyleSheet(f'color: white; background:  white')
            self.A3_Parts[i][0].setStyleSheet(f'color: white; background:  white')
            self.A4_Parts[i][0].setStyleSheet(f'color: white; background:  white')
            self.A5_Parts[i][0].setStyleSheet(f'color: white; background:  white')
            self.A6_Parts[i][0].setStyleSheet(f'color: white; background:  white')
        self.A_Parts = [self.A1_Parts, self.A2_Parts, self.A3_Parts, self.A4_Parts, self.A5_Parts, self.A6_Parts]
        testCube = [[['R', 'Y', 'R'],
                     ['Y', 'G', 'Y'],
                     ['R', 'O', 'O']],
                    [['Y', 'R', 'W'],
                     ['B', 'W', 'G'],
                     ['Y', 'Y', 'G']],
                    [['G', 'G', 'O'],
                     ['O', 'R', 'B'],
                     ['W', 'W', 'B']],
                    [['G', 'B', 'O'],
                     ['W', 'Y', 'W'],
                     ['W', 'W', 'O']],
                    [['Y', 'R', 'G'],
                     ['O', 'B', 'G'],
                     ['W', 'R', 'Y']],
                    [['R', 'B', 'B'],
                     ['O', 'O', 'R'],
                     ['B', 'G', 'B']]]
        # testCube = Utilities.SolvedArrays.solvedArrays().getSolved()
        self.__drawCube(testCube)

        # start Camera Thread
        self.validationLabel = QtWidgets.QLabel(self.tab)
        self.validationLabel.setGeometry(QtCore.QRect(int(0 * faktorWidth), int(125 * faktorHeight),
                                            int(1920 * faktorWidth), int(500 * faktorHeight)))
        font = QtGui.QFont()
        font.setPointSize(48)
        font.setBold(True)
        font.setWeight(75)
        font.setStrikeOut(False)
        font.setStyleStrategy(QtGui.QFont.StyleStrategy.NoAntialias)
        self.validationLabel.setFont(font)
        self.validationLabel.setFocusPolicy(QtCore.Qt.FocusPolicy.NoFocus)
        self.validationLabel.setAutoFillBackground(False)
        self.validationLabel.setLocale(QtCore.QLocale(QtCore.QLocale.Language.English,
                                            QtCore.QLocale.Country.Germany))
        self.validationLabel.setTextFormat(QtCore.Qt.TextFormat.AutoText)
        self.validationLabel.setScaledContents(False)
        self.validationLabel.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)
        self.validationLabel.setWordWrap(False)
        self.validationLabel.setObjectName("validationLabel")
        self.validationLabel.setText("Not validated")
        self.validationLabel.raise_()
        self.validationLabel.setStyleSheet("background: red; color: white")
        self.validationLabel.hide()

        self.cameraThread = cameraThread()
        self.cameraThread.start(QThread.Priority.HighestPriority)
        self.communicationThread = communicationThread()
        self.communicationThread.start(QThread.Priority.IdlePriority)


        # Error Calibration
        self.label.setStyleSheet('color: white; background: red')
        if isCalibrated:
            self.label.hide()
        else:
            self.label.show()

        # Tabbar event
        self.tabWidget.tabBarClicked.connect(self.handleTabBarEvent)

        # Buttons of Calibration Tab
        self.setPoint1.clicked.connect(self.changePoint1)
        self.setPoint2.clicked.connect(self.changePoint2)
        self.takePicture.clicked.connect(self.freezePicture)
        self.setCalibration.clicked.connect(self.safeConfigs)

        self.horizontalSlider.setMinimum(0)
        self.horizontalSlider.setMaximum(camWidth)
        self.horizontalSlider.setValue(self.Point1[1])
        self.horizontalSlider.setTickInterval(5)
        self.horizontalSlider.valueChanged.connect(self.sliderHorizontalChange)
        self.horizontalSlider.hide()

        self.verticalSlider.setMinimum(0)
        self.verticalSlider.setMaximum(camHeight)
        self.verticalSlider.setValue(self.Point1[1])
        self.verticalSlider.setTickInterval(5)
        self.verticalSlider.valueChanged.connect(self.sliderVerticalChange)
        self.verticalSlider.hide()

        self.P1Number_Frame.hide()
        self.P2Number_Frame.hide()

        self.window_width = self.ImgWidget.frameSize().width()
        self.window_height = self.ImgWidget.frameSize().height()
        self.ImgWidget = OwnImageWidget(self.ImgWidget)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update_frame)
        self.timer.start(1)


        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(0)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.isRunning.setText(_translate("MainWindow", "Stopped"))
        self.label.setText(_translate("MainWindow", "NOT CALIBRATED"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("MainWindow", "Mainscreen"))
        self.takePicture.setText(_translate("MainWindow", "Take Picture"))
        self.setPoint1.setText(_translate("MainWindow", "Change Point1"))
        self.setPoint2.setText(_translate("MainWindow", "Change Point2"))
        self.setCalibration.setText(_translate("MainWindow", "Set Calibration"))
        self.groupBox.setTitle(_translate("MainWindow", "Video"))
        self.P1_Y.setText(_translate("MainWindow", "Y:"))
        self.P1Number_X.setText(_translate("MainWindow", "0000"))
        self.P1_X.setText(_translate("MainWindow", "X:"))
        self.P1Number_Y.setText(_translate("MainWindow", "0000"))
        self.P1_Label.setText(_translate("MainWindow", "Point 1:"))
        self.P2_Y.setText(_translate("MainWindow", "Y:"))
        self.P2Number_X.setText(_translate("MainWindow", "0000"))
        self.P2_X.setText(_translate("MainWindow", "X:"))
        self.P2Number_Y.setText(_translate("MainWindow", "0000"))
        self.P2_Label.setText(_translate("MainWindow", "Point 2:"))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("MainWindow", "Calibration"))

    def __drawCube(self, virtualCube):
        colors = {
            'G': 'green',
            'W': 'lightgray',
            'R': 'red',
            'Y': 'yellow',
            'B': 'blue',
            'O': 'orange'
        }
        for i in range(len(virtualCube)):
            drawPart = np.array(np.array(virtualCube[i]).reshape(9, 1))
            for j in range(len(drawPart)):
                self.A_Parts[i][j][0].setStyleSheet(f'color: white; background:  {colors[drawPart[j][0]]}')

    def __generateGUIVirtualCubeArray(self, name, frame, faktorWidth, faktorHeight):
        parts = [[0, 0, 0], [0, 0, 0], [0, 0, 0]]
        for i in range(3):
            for j in range(3):
                parts[i][j] = QtWidgets.QLabel(frame)
                parts[i][j].setGeometry(QtCore.QRect(int(50 * j * faktorWidth), int(50 * i * faktorHeight),
                                                     int(50 * faktorWidth), int(50 * faktorHeight)))
                parts[i][j].setText("")
                parts[i][j].setObjectName(name + "_" + str(i) + str(j))
        return np.array(parts).reshape((9, 1))

    def changePoint1(self):
        global calibPoint
        if not self.activeP1:
            calibPoint = 0
            self.setPoint1.setText("Set Point1")
            self.verticalSlider.setValue(self.Point1[1])
            self.horizontalSlider.setValue(self.Point1[0])
            self.verticalSlider.show()
            self.horizontalSlider.show()
            self.setPoint2.setDisabled(True)
            self.activeP1 = True
        else:
            self.setPoint1.setText("Change Point1")
            self.verticalSlider.hide()
            self.horizontalSlider.hide()
            self.P1Number_X.setText(str(self.Point1[0]))
            self.P1Number_Y.setText(str(self.Point1[1]))
            self.P1Number_Frame.show()
            self.setPoint2.setDisabled(False)
            self.activeP1 = False

    def changePoint2(self):
        global calibPoint
        if not self.activeP2:
            calibPoint = 1
            self.setPoint2.setText("Set Point2")
            self.verticalSlider.setValue(self.Point2[1])
            self.horizontalSlider.setValue(self.Point2[0])
            self.P2Number_Frame.show()
            self.verticalSlider.show()
            self.horizontalSlider.show()
            self.setPoint1.setDisabled(True)
            self.activeP2 = True
        else:
            self.setPoint2.setText("Change Point2")
            self.verticalSlider.hide()
            self.horizontalSlider.hide()
            self.P2Number_X.setText(str(self.Point2[0]))
            self.P2Number_Y.setText(str(self.Point2[1]))
            self.setPoint1.setDisabled(False)
            self.activeP2 = False

    def freezePicture(self):

        if not self.frozen:
            self.takePicture.setText("show Vid")
            self.frozen = True
        else:
            self.takePicture.setText("take Picture")
            self.frozen = False

    def safeConfigs(self):
        global isCalibrated
        settings.beginGroup("camera")
        settings.setValue("point1", self.Point1)
        settings.setValue("point2", self.Point2)
        settings.endGroup()
        isCalibrated = True
        self.label.hide()

    def update_frame(self):

        self._updateCamera()
        self._updateMainScreen()
        self._updateStates()
        gc.collect()

    def _updateMainScreen(self):
        global communicationData, communicationObject, hasOpendDialog, isCalibrated, communicationRunning
        self.__drawCube(communicationData['virtualCopy'])
        if communicationData['validate'] == 1 and not hasOpendDialog:
            isCalibrated = False
            hasOpendDialog = True
            self.validationLabel.show()
            dialog = QtWidgets.QDialog()
            dialog.ui = Ui_Dialog()
            dialog.ui.setupUi(dialog, screen.size().width(), screen.size().height(), communicationData['virtualCopy'])
            dialog.setAttribute(QtCore.Qt.WidgetAttribute.WA_DeleteOnClose)
            dialog.exec()
            communicationData['virtualCopy'] = dialog.ui.virtualCopy.copy()
            if dialog.ui.hasValidated:
                communicationObject.executeSovleHandler(communicationData['virtualCopy'].copy())
                isCalibrated = True
                self.validationLabel.hide()
            else:
                hasOpendDialog = False


    def _updateStates(self):
        stateText = {
            0: "Generating solving steps",
            1: "Close Connection",
            2: "Taking pictures",
            3: "Sending next step",
            4: "PreScrambled loaded",
            5: "Solving Cube"
        }
        self.state.setText(stateText[communicationData['state']])
        self.solvedArray.setText(str(communicationData['solvedArray']))
        if communicationData['isRunning']:
            self.Buttons.hide()
            self.isRunning.setText("Started")
            self.isRunning.setStyleSheet('color: black; background: green')
        else:
            self.Buttons.show()
            self.isRunning.setText("Stopped")
            self.isRunning.setStyleSheet('color: black; background: red')

    def _updateCamera(self):
        global FrameUsed, Frame
        if not np.array_equal(Frame,[]):
            # frame = q.get()
            # q.empty()
            img = Frame
            if not self.frozen:
                self.lastimg = img.copy()
            else:
                img = self.lastimg.copy()
            img = cv2.rectangle(img, self.Point1, self.Point2, self.RectangleColor, self.RectangleThikness)

            img_height, img_width, img_colors = img.shape
            scale_w = float(self.window_width) / float(img_width)
            scale_h = float(self.window_height) / float(img_height)
            scale = min([scale_w, scale_h])

            if scale == 0:
                scale = 1

            img = cv2.resize(img, None, fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            height, width, bpc = img.shape
            bpl = bpc * width
            image = QtGui.QImage(img.data, width, height, bpl, QtGui.QImage.Format.Format_RGB888)
            self.ImgWidget.setImage(image)
        elif self.frozen:
            img = self.lastimg.copy()
            img = cv2.rectangle(img, self.Point1, self.Point2, self.RectangleColor, self.RectangleThikness)

            img_height, img_width, img_colors = img.shape
            scale_w = float(self.window_width) / float(img_width)
            scale_h = float(self.window_height) / float(img_height)
            scale = min([scale_w, scale_h])

            if scale == 0:
                scale = 1

            img = cv2.resize(img, None, fx=scale, fy=scale, interpolation=cv2.INTER_CUBIC)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
            height, width, bpc = img.shape
            bpl = bpc * width
            image = QtGui.QImage(img.data, width, height, bpl, QtGui.QImage.Format.Format_RGB888)
            self.ImgWidget.setImage(image)
        FrameUsed = True

    def sliderHorizontalChange(self):
        global calibPoint
        size = self.horizontalSlider.value()
        if calibPoint == 0:
            self.Point1[0] = size
        else:
            self.Point2[0] = size

    def sliderVerticalChange(self):
        global calibPoint
        size = self.verticalSlider.value()
        if calibPoint == 0:
            self.Point1[1] = size
        else:
            self.Point2[1] = size

    def handleTabBarEvent(self, index):
        global isCalibrated
        if index == 0:
            if isCalibrated:
                self.startCommunicationThread()
        if index == 1:
            self.quitCommunicationThread()

    def startCommunicationThread(self):
        global communicationRunning
        communicationRunning = True
        self.communicationThread.start(QThread.Priority.IdlePriority)

    def quitCommunicationThread(self):
        global communicationRunning, communicationData
        if not communicationData['isRunning']:
            communicationRunning = False
            self.communicationThread.quit()


def startGui(debug):
    DEBUG = debug
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow, screen.size().width(), screen.size().height())
    MainWindow.showMaximized()
    sys.exit(app.exec())
