import asyncio

from Utilities.basic_alg import BasicAlgorithm


class UseFunctions:
    def __init__(self, ):
        self.BA = BasicAlgorithm()

    def turnCubeByArray(self, inputArray):
        """
        Turn cube by steps Array
        :param inputArray: string[] of steps
        :return:
        """
        for inputMove in inputArray:
            asyncio.run(self.__turnCube(inputMove))

    def getCube(self):
        """
        Give the current virtual Cube
        :return: cubeArray string[]
        """
        return self.BA.getCube()

    def setCube(self, unsolvedCube):
        """
        async override the virtual cube
        :param unsolvedCube: string[]
        :return:
        """
        asyncio.run(self.BA.setCube(unsolvedCube))

    def generateNewCube(self):
        """
        generates a new randomized Cube
        :return:
        """
        self.BA.generateNewCube()

    async def __turnCube(self, inputMove):
        """
        turns the cube by step
        :param inputMove: string of step
        :return:
        """
        if inputMove == 'U':
            await self.BA.algU()
        elif inputMove == 'Ui':
            await self.BA.algU_i()
        elif inputMove == 'F':
            await self.BA.algF()
        elif inputMove == 'Fi':
            await self.BA.algF_i()
        elif inputMove == 'L':
            await self.BA.algL()
        elif inputMove == 'Li':
            await self.BA.algL_i()
        elif inputMove == 'R':
            await self.BA.algR()
        elif inputMove == 'Ri':
            await self.BA.algR_i()
        elif inputMove == 'B':
            await self.BA.algB()
        elif inputMove == 'Bi':
            await self.BA.algB_i()
        elif inputMove == 'D':
            await self.BA.algD()
        elif inputMove == 'Di':
            await self.BA.algD_i()

    def isEqual(self,solveState):
        """
        Checks if virtual cube is equal solveState
        :param solveState: string[] to check
        :return: boolean if True or False
        """
        if self.BA.isEqual(solveState,self.getCube()):
            return True
        else:
            return False
