import numpy as np


class solvedArrays():
    def __init__(self, ):
        self.A = []
        self._A1 = [['G', 'G', 'G'], ['G', 'G', 'G'], ['G', 'G', 'G']]
        self._A2 = [['W', 'W', 'W'], ['W', 'W', 'W'], ['W', 'W', 'W']]
        self._A3 = [['R', 'R', 'R'], ['R', 'R', 'R'], ['R', 'R', 'R']]
        self._A4 = [['Y', 'Y', 'Y'], ['Y', 'Y', 'Y'], ['Y', 'Y', 'Y']]
        self._A5 = [['B', 'B', 'B'], ['B', 'B', 'B'], ['B', 'B', 'B']]
        self._A6 = [['O', 'O', 'O'], ['O', 'O', 'O'], ['O', 'O', 'O']]
        self._A_E = [['', '', ''], ['', '', ''], ['', '', '']]

    def getSolved(self, ):
        """
        Gives a copy of a solved virtual cube
        :return: cubeArray string[]
        """
        self.A = []
        return np.rot90(self._appendAll(self._A1, self._A2, self._A3, self._A4, self._A5, self._A6), 0).copy()

    def getWhiteCross(self, ):
        """
        Gives a copy of whitecross virtual cube
        :return:  cubeArray string[]
        """
        self.A = []
        A2 = [['', 'W', ''], ['W', 'W', 'W'], ['', 'W', '']]
        return np.rot90(self._appendAll(self._A_E, A2, self._A_E, self._A_E, self._A_E, self._A_E), 0)

    def getWhiteLayer(self, ):
        """
        Gives a copy of firstlayer virtual cube
        :return:  cubeArray string[]
        """
        self.A = []
        A1 = [['G', 'G', 'G'], ['', 'G', ''], ['', '', '']]
        A3 = [['R', 'R', 'R'], ['', 'R', ''], ['', '', '']]
        A5 = [['B', 'B', 'B'], ['', 'B', ''], ['', '', '']]
        A6 = [['O', 'O', 'O'], ['', 'O', ''], ['', '', '']]
        return np.rot90(self._appendAll(A1, self._A2, A3, self._A_E, A5, A6), 0)

    def getSecondLayer(self, ):
        """
        Gives a copy of secondLayer virtual cube
        :return:  cubeArray string[]
        """
        self.A = []
        A1 = [['G', 'G', 'G'], ['G', 'G', 'G'], ['', '', '']]
        A3 = [['R', 'R', 'R'], ['R', 'R', 'R'], ['', '', '']]
        A5 = [['B', 'B', 'B'], ['B', 'B', 'B'], ['', '', '']]
        A6 = [['O', 'O', 'O'], ['O', 'O', 'O'], ['', '', '']]
        return np.rot90(self._appendAll(A1, self._A2, A3, self._A_E, A5, A6), 0)


    def getTopLayer(self, ):
        """
        Gives a copy of topLayer virtual cube
        :return:  cubeArray string[]
        """
        self.A = []
        A1 = [['G', 'G', 'G'], ['G', 'G', 'G'], ['', '', '']]
        A3 = [['R', 'R', 'R'], ['R', 'R', 'R'], ['', '', '']]
        A5 = [['B', 'B', 'B'], ['B', 'B', 'B'], ['', '', '']]
        A6 = [['O', 'O', 'O'], ['O', 'O', 'O'], ['', '', '']]
        return np.rot90(self._appendAll(A1, self._A2, A3, self._A4, A5, A6), 0)

    def _appendAll(self, A1, A2, A3, A4, A5, A6):
        """
        Fuses all Layers to cubeArray
        :param A1:  LayerArray string[] Green
        :param A2:  LayerArray string[] White
        :param A3:  LayerArray string[] Red
        :param A4:  LayerArray string[] Blue
        :param A5:  LayerArray string[] Orange
        :param A6:  LayerArray string[] Yellow
        :return: cubeArray string[]
        """
        self.A.append(A1)
        self.A.append(A2)
        self.A.append(A3)
        self.A.append(A4)
        self.A.append(A5)
        self.A.append(A6)
        return self.A
