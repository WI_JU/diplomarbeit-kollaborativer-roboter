import numpy as np

from Solver.RCSolver import RubicsCubeSolver
from Utilities.GenerateRandom import GenerateRandom
from Utilities.SolvedArrays import solvedArrays
from Utilities.UseFunctions import UseFunctions
import time


def testTheSolver(runs, solvedCube=solvedArrays().getSolved().copy()):
    randomCubeGenerator = GenerateRandom()
    UseFunction = UseFunctions()
    RCSolver = RubicsCubeSolver(UseFunction)

    count = 0
    maxLenght = 0
    minLength = 200000
    sumLength = 0
    timespanArray = []
    timespan = 0
    # timeperrun =0.003 #WhiteCross
    # timeperrun = 0.15 #FirstLayer
    # timeperrun = 0.3 #SecondLayer
    # timeperrun = 0.45  # TopLayer
    timeperrun = 1.296039092540741  # With permutation
    estematedTime = (runs * timeperrun)
    if estematedTime < 60:
        print("Estemated Runtime: [s]")
        print(estematedTime)
    elif estematedTime < 60 * 60:
        print("Estemated Runtime: [min]")
        print(estematedTime / 60)
    else:
        print("Estemated Runtime: [h]")
        print(estematedTime / (60 * 60))
    print("-----------------------------------------")
    Timer = time.time()
    fails = 0
    for i in range(runs):
        start = time.time()
        unsolvedCube = randomCubeGenerator.generateNewCube().copy()
        solvingSteps = RCSolver.generateSolvingSteps(unsolvedCube)
        length = 0
        # if not partlyArrayTester(solvedCube, RCSolver.getCube()):
        if not np.array_equal(solvedCube, RCSolver.getCube()):
            fails += 1
            print(RCSolver.getMessages())
            print(solvingSteps)
            print(RCSolver.getCube())
        length = RCSolver.getLen()
        if length < minLength:
            minLength = length
        elif length > maxLenght:
            maxLenght = length
        sumLength += length
        timespanArray.append(time.time() - start)
        if i % 10 == 0:
            print(f"{i} / {runs}")
        count += 1
    print("-----------------------------------------")
    print(count)
    print("Needed Time")
    for tspan in timespanArray:
        timespan += tspan
    neededTime = time.time() - Timer
    if neededTime > 60:
        print((neededTime / 60), " min")
    else:
        print(neededTime, " s")
    print("Differenz between Times")
    if neededTime > 60:
        print(((neededTime - estematedTime) / 60), " min")
    else:
        print((neededTime - estematedTime), " s")
    print("Time per Run")
    print((time.time() - Timer) / runs)
    print("Fails:")
    print(fails)
    print("Max Length:")
    print(maxLenght)
    print("Min Length:")
    print(minLength)
    print("Average Solvelength: ")
    averagelength = sumLength / runs
    print(averagelength)

    print("Average Solve Time: [s] ")
    averageTime = timespan / runs
    print(averageTime)


def partlyArrayTester(solvedArray, toTestArray):
    for Arr, Arr2 in zip(solvedArray, toTestArray):
        for seg, seg2 in zip(Arr, Arr2):
            for part, part2 in zip(seg, seg2):
                if part != '':
                    if part != part2:
                        return False
    return True
