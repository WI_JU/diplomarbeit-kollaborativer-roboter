# from Utilities.GenerateRandom import GenerateRandom
#
# def invertCube(steps):
#     """
#            invert steps from clockwise to anticlockwise or anticlockwise to clockwise
#            :param steps: string[]
#            :return: string[] of inverted steps
#     """
#     for index in range(len(steps)):
#         if len(steps[index]) != 2:
#             steps[index] += 'i'
#         else:
#             steps[index] = steps[index].split('i')[0]
#     steps.reverse()
#     return steps
#
# GC = GenerateRandom()
# random = GC.Debug_generateNewCube()
# print(random[0])
# print("Turns:")
# print(random[1])
# print("Solve Turns:")
# print(invertCube(random[1]))
#
#
#
# # xx = [[['Y', 'W', 'Y'], ['B', 'G', 'Y'], ['G', 'O', 'Y']],
# #       [['O', 'O', 'G'], ['G', 'W', 'O'], ['R', 'W', 'B']],
# #       [['B', 'R', 'O'], ['G', 'R', 'W'], ['G', 'Y', 'O']],
# #       [['R', 'B', 'Y'], ['Y', 'Y', 'B'], ['W', 'R', 'B']],
# #       [['W', 'G', 'W'], ['O', 'B', 'R'], ['G', 'R', 'R']],
# #       [['O', 'B', 'B'], ['Y', 'O', 'W'], ['W', 'G', 'R']]]
# #
# # steps = "R U D F B L U L' U' B R'"
# # camerArray = np.rot90(xx, 0)
# # print(RubicscubeSolverLogic().generateSolvingSteps(camerArray))
#
#
#
#
#
#
#



import numpy as np

from GUI.GUI import startGui
import subprocess

# Start UserInterface
from Logic.RubicscubeLogicalSolver import RubicscubeSolverLogic

try:
    p = subprocess.Popen(['onInit.cmd'])
except:
    pass

startGui(True)
