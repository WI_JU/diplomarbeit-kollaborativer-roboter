############################################################################################
#      NSIS Installation Script created by NSIS Quick Setup Script Generator v1.09.18
#               Entirely Edited with NullSoft Scriptable Installation System                
#              by Vlasis K. Barkas aka Red Wine red_wine@freemail.gr Sep 2006               
############################################################################################

!define APP_NAME "RubicsCubeSolver"
!define COMP_NAME "HTL Wels"
!define WEB_SITE "https://www.htl-wels.at"
!define VERSION "00.00.00.02"
!define COPYRIGHT "Wieser.jul © 2022"
!define DESCRIPTION "Application"
!define INSTALLER_NAME "C:\Users\Wiju\Downloads\Nsisqssg\Output\RubicsCubeSolver\RubicsCubeSolverInstaller.exe"
!define MAIN_APP_EXE "main.exe"
!define INSTALL_TYPE "SetShellVarContext all"
!define REG_ROOT "HKLM"
!define REG_APP_PATH "Software\Microsoft\Windows\CurrentVersion\App Paths\${MAIN_APP_EXE}"
!define UNINSTALL_PATH "Software\Microsoft\Windows\CurrentVersion\Uninstall\${APP_NAME}"

######################################################################

VIProductVersion  "${VERSION}"
VIAddVersionKey "ProductName"  "${APP_NAME}"
VIAddVersionKey "CompanyName"  "${COMP_NAME}"
VIAddVersionKey "LegalCopyright"  "${COPYRIGHT}"
VIAddVersionKey "FileDescription"  "${DESCRIPTION}"
VIAddVersionKey "FileVersion"  "${VERSION}"

######################################################################

SetCompressor ZLIB
Name "${APP_NAME}"
Caption "${APP_NAME}"
OutFile "${INSTALLER_NAME}"
BrandingText "${APP_NAME}"
XPStyle on
InstallDirRegKey "${REG_ROOT}" "${REG_APP_PATH}" ""
InstallDir "$PROGRAMFILES\RubicsCubeSolver"

######################################################################

!include "MUI.nsh"

!define MUI_ABORTWARNING
!define MUI_UNABORTWARNING

!insertmacro MUI_PAGE_WELCOME

!ifdef LICENSE_TXT
!insertmacro MUI_PAGE_LICENSE "${LICENSE_TXT}"
!endif

!ifdef REG_START_MENU
!define MUI_STARTMENUPAGE_NODISABLE
!define MUI_STARTMENUPAGE_DEFAULTFOLDER "RubicsCubeSolver"
!define MUI_STARTMENUPAGE_REGISTRY_ROOT "${REG_ROOT}"
!define MUI_STARTMENUPAGE_REGISTRY_KEY "${UNINSTALL_PATH}"
!define MUI_STARTMENUPAGE_REGISTRY_VALUENAME "${REG_START_MENU}"
!insertmacro MUI_PAGE_STARTMENU Application $SM_Folder
!endif

!insertmacro MUI_PAGE_INSTFILES

!define MUI_FINISHPAGE_RUN "$INSTDIR\${MAIN_APP_EXE}"
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM

!insertmacro MUI_UNPAGE_INSTFILES

!insertmacro MUI_UNPAGE_FINISH

!insertmacro MUI_LANGUAGE "English"

######################################################################

Section -MainProgram
${INSTALL_TYPE}
SetOverwrite ifnewer
SetOutPath "$INSTDIR"
File "C:\Users\Wiju\Desktop\Solver\main.exe"
File "C:\Users\Wiju\Desktop\Solver\main.py"
File "C:\Users\Wiju\Desktop\Solver\main.spec"
File "C:\Users\Wiju\Desktop\Solver\onInit.cmd"
File "C:\Users\Wiju\Desktop\Solver\setup.cmd"
SetOutPath "$INSTDIR\__pycache__"
File "C:\Users\Wiju\Desktop\Solver\__pycache__\DEBUGmain.cpython-39.pyc"
File "C:\Users\Wiju\Desktop\Solver\__pycache__\main.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\__pycache__\Test_CameraMain.cpython-310.pyc"
SetOutPath "$INSTDIR\Utilities"
File "C:\Users\Wiju\Desktop\Solver\Utilities\basic_alg.py"
File "C:\Users\Wiju\Desktop\Solver\Utilities\GenerateRandom.py"
File "C:\Users\Wiju\Desktop\Solver\Utilities\Positions.py"
File "C:\Users\Wiju\Desktop\Solver\Utilities\SolvedArrays.py"
File "C:\Users\Wiju\Desktop\Solver\Utilities\Tester.py"
File "C:\Users\Wiju\Desktop\Solver\Utilities\UseFunctions.py"
File "C:\Users\Wiju\Desktop\Solver\Utilities\Utilits.py"
SetOutPath "$INSTDIR\Utilities\__pycache__"
File "C:\Users\Wiju\Desktop\Solver\Utilities\__pycache__\basic_alg.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Utilities\__pycache__\basic_alg.cpython-39.pyc"
File "C:\Users\Wiju\Desktop\Solver\Utilities\__pycache__\GenerateRandom.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Utilities\__pycache__\GenerateRandom.cpython-39.pyc"
File "C:\Users\Wiju\Desktop\Solver\Utilities\__pycache__\Positions.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Utilities\__pycache__\Positions.cpython-39.pyc"
File "C:\Users\Wiju\Desktop\Solver\Utilities\__pycache__\SolvedArrays.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Utilities\__pycache__\SolvedArrays.cpython-39.pyc"
File "C:\Users\Wiju\Desktop\Solver\Utilities\__pycache__\UseFunctions.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Utilities\__pycache__\UseFunctions.cpython-39.pyc"
File "C:\Users\Wiju\Desktop\Solver\Utilities\__pycache__\Utilits.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Utilities\__pycache__\Utilits.cpython-39.pyc"
SetOutPath "$INSTDIR\Solver"
File "C:\Users\Wiju\Desktop\Solver\Solver\FirstLayer.py"
File "C:\Users\Wiju\Desktop\Solver\Solver\LastLayer.py"
File "C:\Users\Wiju\Desktop\Solver\Solver\RCSolver.py"
File "C:\Users\Wiju\Desktop\Solver\Solver\SecondLayer.py"
File "C:\Users\Wiju\Desktop\Solver\Solver\TopLayer.py"
File "C:\Users\Wiju\Desktop\Solver\Solver\WhiteCross.py"
SetOutPath "$INSTDIR\Solver\__pycache__"
File "C:\Users\Wiju\Desktop\Solver\Solver\__pycache__\FirstLayer.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Solver\__pycache__\FirstLayer.cpython-39.pyc"
File "C:\Users\Wiju\Desktop\Solver\Solver\__pycache__\LastLayer.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Solver\__pycache__\LastLayer.cpython-39.pyc"
File "C:\Users\Wiju\Desktop\Solver\Solver\__pycache__\RCSolver.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Solver\__pycache__\RCSolver.cpython-39.pyc"
File "C:\Users\Wiju\Desktop\Solver\Solver\__pycache__\SecondLayer.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Solver\__pycache__\SecondLayer.cpython-39.pyc"
File "C:\Users\Wiju\Desktop\Solver\Solver\__pycache__\TopLayer.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Solver\__pycache__\TopLayer.cpython-39.pyc"
File "C:\Users\Wiju\Desktop\Solver\Solver\__pycache__\WhiteCross.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Solver\__pycache__\WhiteCross.cpython-39.pyc"
SetOutPath "$INSTDIR\Setup"
File "C:\Users\Wiju\Desktop\Solver\Setup\init.cmd"
File "C:\Users\Wiju\Desktop\Solver\Setup\new 2.txt"
SetOutPath "$INSTDIR\Safe setup"
File "C:\Users\Wiju\Desktop\Solver\Safe setup\setup.bat"
SetOutPath "$INSTDIR\Logic"
File "C:\Users\Wiju\Desktop\Solver\Logic\RCS_Socket.py"
File "C:\Users\Wiju\Desktop\Solver\Logic\RubicscubeLogicalSolver.py"
SetOutPath "$INSTDIR\Logic\__pycache__"
File "C:\Users\Wiju\Desktop\Solver\Logic\__pycache__\RCS_Socket.cpython-39.pyc"
File "C:\Users\Wiju\Desktop\Solver\Logic\__pycache__\RubicscubeLogicalSolver.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Logic\__pycache__\RubicscubeLogicalSolver.cpython-39.pyc"
SetOutPath "$INSTDIR\GUI"
File "C:\Users\Wiju\Desktop\Solver\GUI\Dialog.py"
File "C:\Users\Wiju\Desktop\Solver\GUI\Dialog.ui"
File "C:\Users\Wiju\Desktop\Solver\GUI\generateNewUI.cmd"
File "C:\Users\Wiju\Desktop\Solver\GUI\GUI.py"
File "C:\Users\Wiju\Desktop\Solver\GUI\GUI.ui"
SetOutPath "$INSTDIR\GUI\__pycache__"
File "C:\Users\Wiju\Desktop\Solver\GUI\__pycache__\Dialog.cpython-39.pyc"
File "C:\Users\Wiju\Desktop\Solver\GUI\__pycache__\GUI.cpython-39.pyc"
SetOutPath "$INSTDIR\Config"
File "C:\Users\Wiju\Desktop\Solver\Config\Config.ini"
SetOutPath "$INSTDIR\Camera"
File "C:\Users\Wiju\Desktop\Solver\Camera\CameraMain.py"
File "C:\Users\Wiju\Desktop\Solver\Camera\CamerScan.py"
SetOutPath "$INSTDIR\Camera\__pycache__"
File "C:\Users\Wiju\Desktop\Solver\Camera\__pycache__\CameraMain.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Camera\__pycache__\CameraMain.cpython-39.pyc"
File "C:\Users\Wiju\Desktop\Solver\Camera\__pycache__\CamerScan.cpython-310.pyc"
File "C:\Users\Wiju\Desktop\Solver\Camera\__pycache__\CamerScan.cpython-39.pyc"
SetOutPath "$INSTDIR\build\main"
File "C:\Users\Wiju\Desktop\Solver\build\main\Analysis-00.toc"
File "C:\Users\Wiju\Desktop\Solver\build\main\base_library.zip"
File "C:\Users\Wiju\Desktop\Solver\build\main\EXE-00.toc"
File "C:\Users\Wiju\Desktop\Solver\build\main\main.exe.manifest"
File "C:\Users\Wiju\Desktop\Solver\build\main\main.pkg"
File "C:\Users\Wiju\Desktop\Solver\build\main\PKG-00.toc"
File "C:\Users\Wiju\Desktop\Solver\build\main\PYZ-00.pyz"
File "C:\Users\Wiju\Desktop\Solver\build\main\PYZ-00.toc"
File "C:\Users\Wiju\Desktop\Solver\build\main\Tree-00.toc"
File "C:\Users\Wiju\Desktop\Solver\build\main\Tree-01.toc"
File "C:\Users\Wiju\Desktop\Solver\build\main\Tree-02.toc"
File "C:\Users\Wiju\Desktop\Solver\build\main\warn-main.txt"
File "C:\Users\Wiju\Desktop\Solver\build\main\xref-main.html"
SectionEnd

######################################################################

Section -Icons_Reg
SetOutPath "$INSTDIR"
WriteUninstaller "$INSTDIR\uninstall.exe"

!ifdef REG_START_MENU
!insertmacro MUI_STARTMENU_WRITE_BEGIN Application
CreateDirectory "$SMPROGRAMS\$SM_Folder"
CreateShortCut "$SMPROGRAMS\$SM_Folder\${APP_NAME}.lnk" "$INSTDIR\${MAIN_APP_EXE}"
CreateShortCut "$SMPROGRAMS\$SM_Folder\Uninstall ${APP_NAME}.lnk" "$INSTDIR\uninstall.exe"

!ifdef WEB_SITE
WriteIniStr "$INSTDIR\${APP_NAME} website.url" "InternetShortcut" "URL" "${WEB_SITE}"
CreateShortCut "$SMPROGRAMS\$SM_Folder\${APP_NAME} Website.lnk" "$INSTDIR\${APP_NAME} website.url"
!endif
!insertmacro MUI_STARTMENU_WRITE_END
!endif

!ifndef REG_START_MENU
CreateDirectory "$SMPROGRAMS\RubicsCubeSolver"
CreateShortCut "$SMPROGRAMS\RubicsCubeSolver\${APP_NAME}.lnk" "$INSTDIR\${MAIN_APP_EXE}"
CreateShortCut "$SMPROGRAMS\RubicsCubeSolver\Uninstall ${APP_NAME}.lnk" "$INSTDIR\uninstall.exe"

!ifdef WEB_SITE
WriteIniStr "$INSTDIR\${APP_NAME} website.url" "InternetShortcut" "URL" "${WEB_SITE}"
CreateShortCut "$SMPROGRAMS\RubicsCubeSolver\${APP_NAME} Website.lnk" "$INSTDIR\${APP_NAME} website.url"
!endif
!endif

WriteRegStr ${REG_ROOT} "${REG_APP_PATH}" "" "$INSTDIR\${MAIN_APP_EXE}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "DisplayName" "${APP_NAME}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "UninstallString" "$INSTDIR\uninstall.exe"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "DisplayIcon" "$INSTDIR\${MAIN_APP_EXE}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "DisplayVersion" "${VERSION}"
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "Publisher" "${COMP_NAME}"

!ifdef WEB_SITE
WriteRegStr ${REG_ROOT} "${UNINSTALL_PATH}"  "URLInfoAbout" "${WEB_SITE}"
!endif
SectionEnd

######################################################################

Section Uninstall
${INSTALL_TYPE}
Delete "C:\Users\RCsolver\Desktop\RubicsCubeSolver.lnk"
Delete "C:\Users\RCsolver\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\RubicsCubeSolver.lnk"
Exec   "$INSTDIR\Setup\unInstall.cmd"
Delete "$INSTDIR\main.exe"
Delete "$INSTDIR\main.py"
Delete "$INSTDIR\main.spec"
Delete "$INSTDIR\onInit.cmd"
Delete "$INSTDIR\setup.cmd"
Delete "$INSTDIR\__pycache__\DEBUGmain.cpython-39.pyc"
Delete "$INSTDIR\__pycache__\main.cpython-310.pyc"
Delete "$INSTDIR\__pycache__\Test_CameraMain.cpython-310.pyc"
Delete "$INSTDIR\Utilities\basic_alg.py"
Delete "$INSTDIR\Utilities\GenerateRandom.py"
Delete "$INSTDIR\Utilities\Positions.py"
Delete "$INSTDIR\Utilities\SolvedArrays.py"
Delete "$INSTDIR\Utilities\Tester.py"
Delete "$INSTDIR\Utilities\UseFunctions.py"
Delete "$INSTDIR\Utilities\Utilits.py"
Delete "$INSTDIR\Utilities\__pycache__\basic_alg.cpython-310.pyc"
Delete "$INSTDIR\Utilities\__pycache__\basic_alg.cpython-39.pyc"
Delete "$INSTDIR\Utilities\__pycache__\GenerateRandom.cpython-310.pyc"
Delete "$INSTDIR\Utilities\__pycache__\GenerateRandom.cpython-39.pyc"
Delete "$INSTDIR\Utilities\__pycache__\Positions.cpython-310.pyc"
Delete "$INSTDIR\Utilities\__pycache__\Positions.cpython-39.pyc"
Delete "$INSTDIR\Utilities\__pycache__\SolvedArrays.cpython-310.pyc"
Delete "$INSTDIR\Utilities\__pycache__\SolvedArrays.cpython-39.pyc"
Delete "$INSTDIR\Utilities\__pycache__\UseFunctions.cpython-310.pyc"
Delete "$INSTDIR\Utilities\__pycache__\UseFunctions.cpython-39.pyc"
Delete "$INSTDIR\Utilities\__pycache__\Utilits.cpython-310.pyc"
Delete "$INSTDIR\Utilities\__pycache__\Utilits.cpython-39.pyc"
Delete "$INSTDIR\Solver\FirstLayer.py"
Delete "$INSTDIR\Solver\LastLayer.py"
Delete "$INSTDIR\Solver\RCSolver.py"
Delete "$INSTDIR\Solver\SecondLayer.py"
Delete "$INSTDIR\Solver\TopLayer.py"
Delete "$INSTDIR\Solver\WhiteCross.py"
Delete "$INSTDIR\Solver\__pycache__\FirstLayer.cpython-310.pyc"
Delete "$INSTDIR\Solver\__pycache__\FirstLayer.cpython-39.pyc"
Delete "$INSTDIR\Solver\__pycache__\LastLayer.cpython-310.pyc"
Delete "$INSTDIR\Solver\__pycache__\LastLayer.cpython-39.pyc"
Delete "$INSTDIR\Solver\__pycache__\RCSolver.cpython-310.pyc"
Delete "$INSTDIR\Solver\__pycache__\RCSolver.cpython-39.pyc"
Delete "$INSTDIR\Solver\__pycache__\SecondLayer.cpython-310.pyc"
Delete "$INSTDIR\Solver\__pycache__\SecondLayer.cpython-39.pyc"
Delete "$INSTDIR\Solver\__pycache__\TopLayer.cpython-310.pyc"
Delete "$INSTDIR\Solver\__pycache__\TopLayer.cpython-39.pyc"
Delete "$INSTDIR\Solver\__pycache__\WhiteCross.cpython-310.pyc"
Delete "$INSTDIR\Solver\__pycache__\WhiteCross.cpython-39.pyc"
Delete "$INSTDIR\Setup\init.cmd"
Delete "$INSTDIR\Setup\new 2.txt"
Delete "$INSTDIR\Safe setup\setup.bat"
Delete "$INSTDIR\Logic\RCS_Socket.py"
Delete "$INSTDIR\Logic\RubicscubeLogicalSolver.py"
Delete "$INSTDIR\Logic\__pycache__\RCS_Socket.cpython-39.pyc"
Delete "$INSTDIR\Logic\__pycache__\RubicscubeLogicalSolver.cpython-310.pyc"
Delete "$INSTDIR\Logic\__pycache__\RubicscubeLogicalSolver.cpython-39.pyc"
Delete "$INSTDIR\GUI\Dialog.py"
Delete "$INSTDIR\GUI\Dialog.ui"
Delete "$INSTDIR\GUI\generateNewUI.cmd"
Delete "$INSTDIR\GUI\GUI.py"
Delete "$INSTDIR\GUI\GUI.ui"
Delete "$INSTDIR\GUI\__pycache__\Dialog.cpython-39.pyc"
Delete "$INSTDIR\GUI\__pycache__\GUI.cpython-39.pyc"
Delete "$INSTDIR\Config\Config.ini"
Delete "$INSTDIR\Camera\CameraMain.py"
Delete "$INSTDIR\Camera\CamerScan.py"
Delete "$INSTDIR\Camera\__pycache__\CameraMain.cpython-310.pyc"
Delete "$INSTDIR\Camera\__pycache__\CameraMain.cpython-39.pyc"
Delete "$INSTDIR\Camera\__pycache__\CamerScan.cpython-310.pyc"
Delete "$INSTDIR\Camera\__pycache__\CamerScan.cpython-39.pyc"
Delete "$INSTDIR\build\main\Analysis-00.toc"
Delete "$INSTDIR\build\main\base_library.zip"
Delete "$INSTDIR\build\main\EXE-00.toc"
Delete "$INSTDIR\build\main\main.exe.manifest"
Delete "$INSTDIR\build\main\main.pkg"
Delete "$INSTDIR\build\main\PKG-00.toc"
Delete "$INSTDIR\build\main\PYZ-00.pyz"
Delete "$INSTDIR\build\main\PYZ-00.toc"
Delete "$INSTDIR\build\main\Tree-00.toc"
Delete "$INSTDIR\build\main\Tree-01.toc"
Delete "$INSTDIR\build\main\Tree-02.toc"
Delete "$INSTDIR\build\main\warn-main.txt"
Delete "$INSTDIR\build\main\xref-main.html"
 
RmDir "$INSTDIR\build\main"
RmDir "$INSTDIR\Camera\__pycache__"
RmDir "$INSTDIR\Camera"
RmDir "$INSTDIR\Config"
RmDir "$INSTDIR\GUI\__pycache__"
RmDir "$INSTDIR\GUI"
RmDir "$INSTDIR\Logic\__pycache__"
RmDir "$INSTDIR\Logic"
RmDir "$INSTDIR\Safe setup"
RmDir "$INSTDIR\Setup"
RmDir "$INSTDIR\Solver\__pycache__"
RmDir "$INSTDIR\Solver"
RmDir "$INSTDIR\Utilities\__pycache__"
RmDir "$INSTDIR\Utilities"
RmDir "$INSTDIR\__pycache__"
 
Delete "$INSTDIR\uninstall.exe"
!ifdef WEB_SITE
Delete "$INSTDIR\${APP_NAME} website.url"
!endif

RmDir "$INSTDIR"

!ifdef REG_START_MENU
!insertmacro MUI_STARTMENU_GETFOLDER "Application" $SM_Folder
Delete "$SMPROGRAMS\$SM_Folder\${APP_NAME}.lnk"
Delete "$SMPROGRAMS\$SM_Folder\Uninstall ${APP_NAME}.lnk"
!ifdef WEB_SITE
Delete "$SMPROGRAMS\$SM_Folder\${APP_NAME} Website.lnk"
!endif
RmDir "$SMPROGRAMS\$SM_Folder"
!endif

!ifndef REG_START_MENU
Delete "$SMPROGRAMS\RubicsCubeSolver\${APP_NAME}.lnk"
Delete "$SMPROGRAMS\RubicsCubeSolver\Uninstall ${APP_NAME}.lnk"
!ifdef WEB_SITE
Delete "$SMPROGRAMS\RubicsCubeSolver\${APP_NAME} Website.lnk"
!endif
RmDir "$SMPROGRAMS\RubicsCubeSolver"
!endif

DeleteRegKey ${REG_ROOT} "${REG_APP_PATH}"
DeleteRegKey ${REG_ROOT} "${UNINSTALL_PATH}"
SectionEnd

######################################################################

