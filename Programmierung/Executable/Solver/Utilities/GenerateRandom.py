import random
import asyncio
from Utilities.SolvedArrays import solvedArrays
from Utilities.basic_alg import BasicAlgorithm

class GenerateRandom:
    def __init__(self):
        self.BA = BasicAlgorithm()
        self.cubeArray = []

    def generateNewCube(self):
        """
        generates a new Randomized cube
        :return: cubeArray string[]
        """
        self.cubeArray = solvedArrays().getSolved()
        self.__generateRandomCube()
        return self.BA.getCube()

    def __generateRandomCube(self):
        """
        async randomize Cube
        :return:
        """
        asyncio.run(self.__randomizingCube())

    async def __randomizingCube(self):
        """
        Generates random Number to randomize between 50 to 500 random Moves
        And turns starts randomsteps
        :return:
        """
        randomMixer = random.randrange(50, 500)
        for i in range(randomMixer):
            await self.__randomSteps()

    async def __randomSteps(self):
        """
        Moves the cube by a random Move between 0 to 13
        :return:
        """
        inputMove = random.randrange(0, 12)
        if inputMove == 0:
            await self.BA.algU()
        elif inputMove == 1:
            await self.BA.algF()
        elif inputMove == 2:
            await self.BA.algL()
        elif inputMove == 3:
            await self.BA.algR()
        elif inputMove == 4:
            await self.BA.algB()
        elif inputMove == 5:
            await self.BA.algD()
        elif inputMove == 6:
            await self.BA.algB_i()
        elif inputMove == 7:
            await self.BA.algR_i()
        elif inputMove == 8:
            await self.BA.algL_i()
        elif inputMove == 9:
            await self.BA.algF_i()
        elif inputMove == 10:
            await self.BA.algU_i()
        elif inputMove == 11:
            await self.BA.algD_i()
