import time
import numpy
from Utilities.Positions import Positions


def printCube(cube):
    """
    Prints the cube to the console formated
    :param cube: stirng[] of virtual Cube
    :return:
    """
    emptyLayer = numpy.rot90([[' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']])
    for i in range(3):
        print(emptyLayer[i], end="")
        print('\t', end="")
        print(cube[1][i], end="")
        print('\t', end="")
        print(emptyLayer[i], end="")
        print('\t', end="")
        print(emptyLayer[i])
    print()
    for i in range(3):
        print(cube[0][i], end="")
        print('\t', end="")
        print(cube[2][i], end="")
        print('\t', end="")
        print(cube[4][i], end="")
        print('\t', end="")
        print(cube[5][i])
    print()
    for i in range(3):
        print(emptyLayer[i], end="")
        print('\t', end="")
        print(cube[3][i], end="")
        print('\t', end="")
        print(emptyLayer[i], end="")
        print('\t', end="")
        print(emptyLayer[i])


def tester(runtime, UserFunction, stateSolved, solvedCube, falseRuns):
    """
    Tester to test the RubicscubeLogic
    :param runtime: int number of runs
    :param UserFunction: the userFucntion from the start
    :param stateSolved: the solvedState to check if finished
    :param solvedCube: the solvedCube
    :param falseRuns: boolean if should show falseRuns
    :return:
    """
    count = 0
    run = True
    failesCount = 0
    falseRun = 0
    maxLenght = 0
    minLength = 200
    prevLen = 0
    timespanArray = []
    timespan = 0
    # timeperrun =0.003 #WhiteCross
    # timeperrun = 0.15 #FirstLayer
    # timeperrun = 0.3 #SecondLayer
    timeperrun = 0.45  # TopLayer
    estematedTime = (runtime * timeperrun)
    if estematedTime < 60:
        print("Estemated Runtime: [s]")
        print(estematedTime)
    elif estematedTime < 60 * 60:
        print("Estemated Runtime: [min]")
        print(estematedTime / 60)
    else:
        print("Estemated Runtime: [h]")
        print(estematedTime / (60 * 60))
    print("-----------------------------------------")
    Timer = time.time()
    for i in range(runtime):
        start = time.time()
        UserFunction.generateNewCube()
        print(UserFunction.getCube())
        test = stateSolved.canSolveCube(UserFunction.getCube())
        if test:
            if not UserFunction.isEqual(solvedCube):
                print("FAILED")
                printCube(stateSolved.getTopLayer())
                print("Endstate")
                printCube(UserFunction.getCube())
                print(stateSolved.getMessages())
                print(stateSolved.generateSolvingSteps())
                failesCount += 1
            else:
                len = stateSolved.getLen()
                if len - prevLen >= maxLenght:
                    maxLenght = len - prevLen
                if len - prevLen <= minLength:
                    minLength = len - prevLen
                prevLen = len
        else:
            falseRun += 1
            if falseRuns:
                print("Endstate")
                printCube(UserFunction.getCube())
                # print(stateSolved.getMessages())

        count += 1
        if count % 10 == 0:
            print(f"{count}/{runtime} Runs complete!!")
        timespanArray.append(time.time() - start)
    print("-----------------------------------------")
    print("Needed Time")
    for tspan in timespanArray:
        timespan += tspan
    neededTime = time.time() - Timer
    if neededTime > 60:
        print((neededTime / 60), " min")
    else:
        print(neededTime, " s")
    print("Differenz between Times")
    if neededTime > 60:
        print(((neededTime - estematedTime) / 60), " min")
    else:
        print((neededTime - estematedTime), " s")
    print("Time per Run")
    print((time.time() - Timer) / count)
    print("Runntime: ")
    print(count)
    print("Falserun times: ")
    print(falseRun)
    print("% - Falserun times: ")
    percent = (falseRun / count) * 100
    print(percent)

    print("Failed times: ")
    print(failesCount)
    print("% - Failed times:")
    if count != falseRun:
        percent = ((failesCount) / (count - falseRun)) * 100
    else:
        percent = 0
    print(percent)
    print("Max Length:")
    print(maxLenght)
    print("Min Length:")
    print(minLength)
    print("Average Solvelength: ")
    length = stateSolved.getLen()
    div = count - falseRun - failesCount
    if div == 0:
        div = 1
    averagelength = length / div
    print(averagelength)

    print("Average Solve Time: [s] ")
    averageTime = timespan / div
    print(averageTime)


def findMissingStones(randomCube):
    """
    Lists all stone which are missing
    :param randomCube: virtuall Cube
    :return: list of missing pieces
    """
    edgeStones = ['WG', 'WR', 'WB', 'WO', 'GR', 'RB', 'BO', 'OG', 'YG', 'YR', 'YB', 'YO']
    cornerStones = ['WRG', 'WRB', 'WOG', 'WOB', 'YRG', 'YRB', 'YOG', 'YOB']
    output = []
    for edge in edgeStones:
        if Positions().testEdge(randomCube, edge) == None:
            output.append(edge)
    for corner in cornerStones:
        if Positions().testCorner(randomCube, corner) == None:
            output.append(corner)
    return output


def stringToArrayCube(stringCube):
    """
    converts stringSteps to list of Steps
    :param stringCube: string of steps
    :return: array of steps
    """
    arr = stringCube.split(',')
    return arr
