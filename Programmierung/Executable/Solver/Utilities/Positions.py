class Positions:

    def searchWhiteStonePosition(self, search, randomCube):
        """
        Searches the Position of WhiteStone
        :param search: whiteStone [PrimaryColor, SecondaryColor]
        :param randomCube: virtual Cube
        :return: position of Whitestone - [Array,WhitePosition1,WhitePosition2,ConterArrayIndex, ConterPosition1,ConterPosition2]
        """
        whitePositions = [[0, 1], [1, 2], [2, 1], [1, 0]]
        conterPosition = [
            [[1, 1, 0], [2, 1, 0], [3, 1, 0], [5, 1, 2]],
            [[5, 0, 1], [4, 0, 1], [2, 0, 1], [0, 0, 1]],
            [[1, 2, 1], [4, 1, 0], [3, 0, 1], [0, 1, 2]],
            [[2, 2, 1], [4, 2, 1], [5, 2, 1], [0, 2, 1]],
            [[1, 1, 2], [5, 1, 0], [3, 1, 2], [2, 1, 2]],
            [[1, 0, 1], [0, 1, 0], [3, 2, 1], [4, 1, 2]]
        ]
        for indexArray in range(len(randomCube)):
            for indexPosition in range(len(whitePositions)):
                position = whitePositions[indexPosition]
                positionIndex = indexPosition
                secondPosition = conterPosition[indexArray][positionIndex]
                secondColor = randomCube[secondPosition[0]][secondPosition[1]][secondPosition[2]]
                firstColor = randomCube[indexArray][position[0]][position[1]]
                color = firstColor + secondColor
                if color == search:
                    return [search, indexArray, position[0], position[1], secondPosition[0], secondPosition[1],
                            secondPosition[2]]

    def searchFLPosition(self, search, randomCube):
        """
        Search FirstLayer postion of Stone
        :param search: cornerStone [PrimaryColor, SecondaryColor]
        :param randomCube: virtual cube string[]
        :return: position of CornerStone - [search, indexCube, cornerPosition, conterPosOrientation2, conterPosOrientation1]
        """
        return self.__findeCorner(randomCube, search)

    def searchSLPosition(self,searches,randomCube):
        """
        Search SecondLayer postion of edgeStone
        :param search: edgeStone [PrimaryColor, SecondaryColor]
        :param randomCube: virtual cube string[]
        :return: position of edgeStone - [search, indexCube, edgePosition, conterEdgePosition]
        """
        return self.__findEdge(randomCube,searches)

    def testEdge(self, randomCube, search):
        """
        Search any postion of edgeStone
        :param search: edgeStone [PrimaryColor, SecondaryColor]
        :param randomCube: virtual cube string[]
        :return: position of edgeStone - [search, indexCube, edgePosition, conterEdgePosition]
        """
        return self.__findEdge(randomCube, search)

    def testCorner(self, randomCube, search):
        """
        Search any Postions of cornerStone
        :param search: cornerStone [PrimaryColor, SecondaryColor]
        :param randomCube: virtual cube string[]
        :return: position of cornerStone - [search, indexCube, cornerPosition, conterPosOrientation2, conterPosOrientation1]
        """
        return self.__findeCorner(randomCube, search)

    def __findEdge(self, randomCube, search):
        """
        Logic of finding Edges
        :param randomCube: virtual cube stirng[]
        :param search: edgeStone string[]
        :return: position of edge
        """
        edge = [[0, 1], [1, 2], [2, 1], [1, 0]]
        conterEdge = [
            [[1, 1, 0], [2, 1, 0], [3, 1, 0], [5, 1, 2]],
            [[5, 0, 1], [4, 0, 1], [2, 0, 1], [0, 0, 1]],
            [[1, 2, 1], [4, 1, 0], [3, 0, 1], [0, 1, 2]],
            [[2, 2, 1], [4, 2, 1], [5, 2, 1], [0, 2, 1]],
            [[1, 1, 2], [5, 1, 0], [3, 1, 2], [2, 1, 2]],
            [[1, 0, 1], [0, 1, 0], [3, 2, 1], [4, 1, 2]]
        ]
        for indexCube in range(len(randomCube)):
            for indexPieces in range(len(edge)):
                edgePosition = edge[indexPieces]
                conterEdgePosition = conterEdge[indexCube][indexPieces]

                firstEdgeColor = randomCube[indexCube][edgePosition[0]][edgePosition[1]]
                secondEdgeColor = randomCube[conterEdgePosition[0]][conterEdgePosition[1]][conterEdgePosition[2]]
                colorEdge = firstEdgeColor + secondEdgeColor
                if colorEdge == search:
                    return [search, indexCube, edgePosition, conterEdgePosition]

    def __findeCorner(self, randomCube, search):
        """
        Logic of finding Corner
        :param randomCube: virtual cube string[]
        :param search: cornerStone string[]
        :return: position of corner
        """
        corner = [[0, 0], [0, 2], [2, 2], [2, 0]]
        conterCornerOr1 = [
            [[1, 0, 0], [1, 2, 0], [3, 0, 0], [3, 2, 0]],
            [[5, 0, 2], [5, 0, 0], [2, 0, 2], [2, 0, 0]],
            [[1, 2, 0], [1, 2, 2], [3, 0, 2], [3, 0, 0]],
            [[2, 2, 0], [2, 2, 2], [5, 2, 0], [5, 2, 2]],
            [[1, 2, 2], [1, 0, 2], [3, 2, 2], [3, 0, 2]],
            [[1, 0, 2], [1, 0, 0], [3, 2, 0], [3, 2, 2]]
        ]
        conterCornerOr2 = [
            [[5, 0, 2], [2, 0, 0], [2, 2, 0], [5, 2, 2]],
            [[0, 0, 0], [4, 0, 2], [4, 0, 0], [0, 0, 2]],
            [[0, 0, 2], [4, 0, 0], [4, 2, 0], [0, 2, 2]],
            [[0, 2, 2], [4, 2, 0], [4, 2, 2], [0, 2, 0]],
            [[2, 0, 2], [5, 0, 0], [5, 2, 0], [2, 2, 2]],
            [[4, 0, 2], [0, 0, 0], [0, 2, 0], [4, 2, 2]]
        ]
        searchColor = search[:]
        for indexCube in range(len(randomCube)):
            for indexPieces in range(len(corner)):
                cornerPosition = corner[indexPieces]
                if searchColor[0] == randomCube[indexCube][cornerPosition[0]][cornerPosition[1]]:
                    conterPosOr1 = conterCornerOr1[indexCube][indexPieces]
                    conterPosOr2 = conterCornerOr2[indexCube][indexPieces]
                    colorOr1 = randomCube[conterPosOr1[0]][conterPosOr1[1]][conterPosOr1[2]]
                    colorOr2 = randomCube[conterPosOr2[0]][conterPosOr2[1]][conterPosOr2[2]]
                    if searchColor[1] == colorOr1 and searchColor[2] == colorOr2:
                        return [search, indexCube, cornerPosition, conterPosOr1, conterPosOr2]
                    elif searchColor[1] == colorOr2 and searchColor[2] == colorOr1:
                        return [search, indexCube, cornerPosition, conterPosOr2, conterPosOr1]

    def __arrayInclude(self, array, search):
        """
        Checks if an Array includes an Position
        :param array: string[]
        :param search: string
        :return:
        """
        for arr in array:
            if array == search:
                return True
            else:
                return False
