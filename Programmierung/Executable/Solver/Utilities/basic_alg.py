import numpy as np
import random
from Utilities.SolvedArrays import solvedArrays
import asyncio


# A1 ... Green Mid
# A2 ... White Mid
# A3 ... Red Mid
# A4 ... Yellow Mid
# A5 ... Blue Mid
# A6 ... Orange Mid

class BasicAlgorithm:
    def __init__(self):
        self.cubeArray = solvedArrays().getSolved()
        # self.__generateRandomCube()

    def getCube(self):
        """
        Returns the virtual cube
        :return: cubeArray: string[]
        """
        return self.cubeArray

    def setCube(self, cubeArray):
        """
        Overrides the current cubeArray
        :param cubeArray: string[]
        :return:
        """
        self.cubeArray = cubeArray

    def __generateRandomCube(self):
        """
        Start ansyc randomizing of the cube
        Only for testing
        :return:
        """
        asyncio.run(self.__randomizingCube())

    async def __randomizingCube(self):
        """
        Generates random steps from 50 to 500
        :return:
        """
        randomMixer = random.randrange(50, 500)
        for i in range(randomMixer):
            await self.__randomSteps()


    async def __randomSteps(self):
        """
        Turns the cube on step random
        :return:
        """
        inputMove = random.randrange(0, 13)
        if inputMove == 0:
            await self.algU()
        elif inputMove == 1:
            await self.algF()
        elif inputMove == 2:
            await self.algL()
        elif inputMove == 3:
            await self.algR()
        elif inputMove == 4:
            await self.algB()
        elif inputMove == 5:
            await self.algD()
        elif inputMove == 6:
            await self.algB_i()
        elif inputMove == 7:
            await self.algR_i()
        elif inputMove == 8:
            await self.algL_i()
        elif inputMove == 9:
            await self.algF_i()
        elif inputMove == 10:
            await self.algU_i()
        elif inputMove == 12:
            await self.algD_i()

    async def setCube(self, cubeArray):
        """
        Overrides the current cubeArray
        :param cubeArray: string[]
        :return:
        """
        self.cubeArray = cubeArray
    def generateNewCube(self):
        """
        Generates a new random cubeArray
        ONLY FOR TESTING
        :return:
        """
        self.cubeArray = solvedArrays().getSolved()
        self.__generateRandomCube()

    async def algU(self, ):
        """
        Turns the upper cube by 90deg
        :return:
        """
        await self.__rotationsCube(1, 0, False)

    async def algU_i(self, ):
        """
        Turns the upper cube by -90deg
        :return:
        """
        await self.__rotationsCube(1, 0, True)

    async def algF(self, ):
        """
        Turns the front cube by 90deg
        :return:
        """
        await self.__rotationsCube(2, 1, False)

    async def algF_i(self, ):
        """
        Turns the front cube by -90deg
        :return:
        """
        await self.__rotationsCube(2, 1, True)

    async def algL(self, ):
        """
        Turns the left cube by 90deg
        :return:
        """
        await self.__rotationsCube(0, 2, False)

    async def algL_i(self, ):
        """
        Turns the left cube by -90deg
        :return:
        """
        await self.__rotationsCube(0, 2, True)

    async def algR(self, ):
        """
        Turns the right cube by 90deg
        :return:
        """
        await self.__rotationsCube(4, 3, False)

    async def algR_i(self, ):
        """
        Turns the right cube by -90deg
        :return:
        """
        await self.__rotationsCube(4, 3, True)

    async def algB(self, ):
        """
        Turns the back cube by 90deg
        :return:
        """
        await self.__rotationsCube(5, 4, False)

    async def algB_i(self, ):
        """
        Turns the back cube by -90deg
        :return:
        """
        await self.__rotationsCube(5, 4, True)

    async def algD(self, ):
        """
        Turns the down cube by 90deg
        :return:
        """
        await self.__rotationsCube(3, 5, False)

    async def algD_i(self, ):
        """
        Turns the down cube by -90deg
        :return:
        """
        await self.__rotationsCube(3, 5, True)

    async def __rotationsCube(self, config1, config2, invert):
        """
        Rotates the cube Clockwise
        :param config1: number of primary
        :param config2: number of secondary
        :param invert: boolean clockwise = False
        :return:
        """
        cubeArray = self.getCube()
        if not invert:
            cubeArray[config1] = await self.rotateClockwiseLayer(cubeArray[config1])
            await self.setCube(cubeArray)
            await self.__rotateClockwiseSides(config2)
        else:
            self.cubeArray[config1] = await self.rotateAntiClockwiseLayer(cubeArray[config1])
            await self.setCube(cubeArray)
            await self.__rotateAntiClockwiseSides(config2)

    async def __rotateClockwiseSides(self, Layer):
        """
        Turns the side Layers Clockwise
        :param Layer: number of Layer
        :return:
        """
        cube = self.getCube()
        if Layer == 0:
            # U Layer
            Asafe = cube[0][0].copy()
            cube[0][0] = cube[2][0]
            cube[2][0] = cube[4][0]
            cube[4][0] = cube[5][0]
            cube[5][0] = Asafe
        elif Layer == 1:
            # F Layer
            for i in range(3):
                Asafe = cube[1][2][i].copy()
                cube[1][2][i] = cube[0][2 - i][2]
                cube[0][2 - i][2] = cube[3][0][2 - i]
                cube[3][0][2 - i] = cube[4][i][0]
                cube[4][i][0] = Asafe
        elif Layer == 2:
            # L Layer
            for i in range(3):
                Asafe = cube[1][i][0].copy()
                cube[1][i][0] = cube[5][2 - i][2]
                cube[5][2 - i][2] = cube[3][i][0]
                cube[3][i][0] = cube[2][i][0]
                cube[2][i][0] = Asafe
        elif Layer == 3:
            # R Layer
            for i in range(3):
                Asafe = cube[1][i][2]
                cube[1][i][2] = cube[2][i][2]
                cube[2][i][2] = cube[3][i][2]
                cube[3][i][2] = cube[5][2 - i][0]
                cube[5][2 - i][0] = Asafe
        elif Layer == 4:
            # B Layer
            for i in range(3):
                Asafe = cube[1][0][i]
                cube[1][0][i] = cube[4][i][2]
                cube[4][i][2] = cube[3][2][2 - i]
                cube[3][2][2 - i] = cube[0][2 - i][0]
                cube[0][2 - i][0] = Asafe
        elif Layer == 5:
            # D Layer
            Asafe = cube[5][2].copy()
            cube[5][2] = cube[4][2]
            cube[4][2] = cube[2][2]
            cube[2][2] = cube[0][2]
            cube[0][2] = Asafe
        await self.setCube(cube)

    async def __rotateAntiClockwiseSides(self, Layer):
        """
        Turn the layer anticlockwise
        :param Layer: number of layer
        :return:
        """
        cube = self.getCube()
        if Layer == 0:
            # U Layer
            Asafe = cube[0][0].copy()
            cube[0][0] = cube[5][0]
            cube[5][0] = cube[4][0]
            cube[4][0] = cube[2][0]
            cube[2][0] = Asafe
        elif Layer == 1:
            # F Layer
            for i in range(3):
                Asafe = cube[1][2][i]
                cube[1][2][i] = cube[4][i][0]
                cube[4][i][0] = cube[3][0][2 - i]
                cube[3][0][2 - i] = cube[0][2 - i][2]
                cube[0][2 - i][2] = Asafe
        elif Layer == 2:
            # L Layer
            for i in range(3):
                Asafe = cube[1][i][0]
                cube[1][i][0] = cube[2][i][0]
                cube[2][i][0] = cube[3][i][0]
                cube[3][i][0] = cube[5][2 - i][2]
                cube[5][2 - i][2] = Asafe
        elif Layer == 3:
            # R Layer
            for i in range(3):
                Asafe = cube[1][i][2]
                cube[1][i][2] = cube[5][2 - i][0]
                cube[5][2 - i][0] = cube[3][i][2]
                cube[3][i][2] = cube[2][i][2]
                cube[2][i][2] = Asafe
        elif Layer == 4:
            # B Layer
            for i in range(3):
                Asafe = cube[1][0][i]
                cube[1][0][i] = cube[0][2 - i][0]
                cube[0][2 - i][0] = cube[3][2][2 - i]
                cube[3][2][2 - i] = cube[4][i][2]
                cube[4][i][2] = Asafe
        elif Layer == 5:
            # D Layer
            Asafe = cube[0][2].copy()
            cube[0][2] = cube[2][2]
            cube[2][2] = cube[4][2]
            cube[4][2] = cube[5][2]
            cube[5][2] = Asafe
        await self.setCube(cube)

    @staticmethod
    def isEqual(solvedState, currentState):
        """
        Checks if solvedState is equal currenState
        :param solvedState: string[]
        :param currentState: string[]
        :return:
        """
        solvedStateArray = solvedState.copy()
        for i in range(len(solvedState)):
            for j in range(len(solvedState[i])):
                for k in range(len(solvedState[i][j])):
                    if solvedState[i][j][k] == '':
                        solvedStateArray[i][j][k] = currentState[i][j][k]
        if np.array_equal(currentState, solvedStateArray):
            return True
        else:
            return False

    @staticmethod
    async def rotateClockwiseLayer(sideArray):
        """
        Turns total layer clockwise
        :param sideArray: string[]
        :return: string[] of turned layer
        """
        return np.rot90(sideArray, 1, (1, 0))

    @staticmethod
    async def rotateAntiClockwiseLayer(sideArray):
        """
        Turns total layer anticlockwise
        :param sideArray: string[]
        :return: string[] of turned layer
        """
        return np.rot90(sideArray, 1, (0, 1))
