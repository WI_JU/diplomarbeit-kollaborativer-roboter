from copy import deepcopy

import Utilities.UseFunctions
from Utilities.Positions import Positions
import itertools


class Edge:
    def __init__(self, piece, Layer, position, Orientation1):
        self.piece = piece
        self.positionLayer = Layer
        self.position = position
        self.Orientation1Layer = Orientation1[0]
        self.Orientation1 = [Orientation1[1], Orientation1[2]]


class SecondLayer:
    def __init__(self, UF: Utilities.UseFunctions.UseFunctions, superMinimum=5):
        self._UF = UF
        self._position = Positions()
        self._PositionZero = ['RG', 'BR', 'GO', 'OB']
        self._superMinimum = superMinimum
        self._length = 0
        self._messages = ""

    def getLen(self):
        """
        adds up all moves
        :return: int of moves
        """
        return self._length

    def getMessages(self):
        """
        the messages of the solver
        :return: string of messages
        """
        return self._messages

    def canSolveCube(self):
        """
        checks if can solve virtual cube
        :return: boolean can/cannot solve
        """
        steps = self.__generateStepsToSolve()
        if not steps:
            return False
        else:
            self._length += len(steps)
            return True

    def getSteps(self):
        """
        solves Cube and return steps to solve
        :return: string[] of steps
        """
        steps = self.__generateStepsToSolve()
        return steps

    def getSolvedCube(self):
        """
        gives a copy of partly solved cube
        :return: string[] of virtual cube
        """
        steps = self.__generateStepsToSolve()
        return self._UF.getCube()

    def __generateStepsToSolve(self):
        """
        solves the cube of permutation of Stones
        :return: string[] of steps
        """
        minLenght = 100000
        solveSteps = []
        bevoreCube = deepcopy(self._UF.getCube())
        permutationObject = list(itertools.permutations(self._PositionZero))
        for perm in permutationObject:
            self._messages = ""
            self._UF.setCube(bevoreCube.copy())
            steps = self.__solveStepsByPieceOrder(perm)
            if 1 < len(steps) < minLenght:
                minLenght = len(steps)
                if minLenght < self._superMinimum:
                    return steps
                solveSteps = steps.copy()
        self._UF.setCube(bevoreCube)
        self._UF.turnCubeByArray(solveSteps)
        return solveSteps

    def __solveStepsByPieceOrder(self, orderOfPieces):
        """
        solves cube by order of stone positions
        :param orderOfPieces: string[] of Positions
        :return: string[] of steps
        """
        self._messages = ""
        allsteps = []
        for stone in orderOfPieces:
            steps = []
            self._stone = stone
            self._messages += str('\n') + str(stone) + str('\n')
            slPositions = self._position.searchSLPosition(stone, self._UF.getCube())
            self._messages += str('\n') + str(slPositions) + str('\n')
            self._edge = Edge(slPositions[0], slPositions[1], slPositions[2], slPositions[3])

            masterColor = self._edge.piece[:][0]
            slaveColor = self._edge.piece[:][1]
            if self._edge.positionLayer == self.__getFinishSideByColor(masterColor):
                if self._edge.Orientation1Layer == self.__getFinishSideByColor(slaveColor):
                    steps.extend(self.__isLCP())
                elif self._edge.Orientation1Layer == 3:

                    steps.extend(self.__tLCP())

                else:
                    steps.extend(self.__sLPFP())

            elif self._edge.Orientation1Layer == self.__getFinishSideByColor(slaveColor):
                if self._edge.positionLayer == 3:
                    steps.extend(self.__tL2CP())
                else:
                    steps.extend(self.__sLOFP())
            else:
                if self._edge.positionLayer == 3:
                    steps.extend(self.__tLFP())
                elif self._edge.Orientation1Layer == 3:
                    steps.extend(self.__tL2FP())
                else:
                    steps.extend(self.__sL2FP())
            allsteps.extend(steps)
            self._messages += str(steps)
        return allsteps

    def __isLCP(self):
        """
        solving steps for inplace side Layer Correct Position
        :return: string[] of steps
        """
        self._messages += str('\n') + "inplace side Layer Correct Position" + str('\n')
        steps = []
        return steps

    def __tLCP(self):
        """
        solving steps for top Layer Correct Position
        :return: string[] of steps
        """
        self._messages += str('\n') + "top Layer Correct Position" + str('\n')
        steps = []
        turnLayer = self.__getFinishSideByColor(self._edge.piece[:][1])
        steps.extend(self.__turnOnWorkLayer(self.__getFinishSideByColor(self._edge.piece[:][1]),
                                            self._edge.positionLayer, 1,
                                            offset=-1))
        steps.extend(self.__threeSteps(turnLayer, False, False))
        steps.extend(self.__stepsOfSideByIndex(3, True, 1))
        turnLayer = self.__getFinishSideByColor(self._edge.piece[:][0])
        steps.extend(self.__threeSteps(turnLayer, True, True))
        return steps

    def __tL2CP(self):
        """
        solving steps for top Layer 2  Correct Position
        :return: string[] of steps
        """
        self._messages += str('\n') + "top Layer 2  Correct Position" + str('\n')
        steps = []
        turnLayer = self.__getFinishSideByColor(self._edge.piece[:][0])
        steps.extend(self.__turnOnWorkLayer(self.__getFinishSideByColor(self._edge.piece[:][0]),
                                            self._edge.Orientation1Layer, 1,
                                            offset=1))
        steps.extend(self.__threeSteps(turnLayer, True, False))
        steps.extend(self.__stepsOfSideByIndex(3, False, 1))
        turnLayer = self.__getFinishSideByColor(self._edge.piece[:][1])
        steps.extend(self.__threeSteps(turnLayer, False, True))
        return steps

    def __sLPFP(self):
        """
        solving steps for side Layer PositionLayer False Position
        :return: string[] of steps
        """
        self._messages += str('\n') + "side Layer PositionLayer False Position" + str('\n')
        steps = []
        turnLayer = self._edge.positionLayer
        steps.extend(self.__threeSteps(turnLayer, False, False))
        turnLayer = self._edge.positionLayer
        steps.extend(self.__stepsOfSideByIndex(3, True, 1))
        steps.extend(self.__threeSteps(turnLayer, True, True))
        steps.extend(self.__stepsOfSideByIndex(3, False, 1))
        steps.extend(self.__tLCP())

        return steps

    def __sLOFP(self):
        """
        solving steps for side Layer OrientationLayer False Position
        :return: string[] of steps
        """
        self._messages += str('\n') + "side Layer OrientationLayer False Position" + str('\n')
        steps = []
        turnLayer = self._edge.Orientation1Layer
        steps.extend(self.__threeSteps(turnLayer, True, False))
        turnLayer = self.__getOffsetTurnWorkLayer(self._edge.Orientation1Layer, 2)
        steps.extend(self.__stepsOfSideByIndex(3, False, 1))
        steps.extend(self.__threeSteps(turnLayer, False, True))
        steps.extend(self.__stepsOfSideByIndex(3, True, 1))
        steps.extend(self.__tL2CP())

        return steps

    def __isLFP(self):
        """
        solving steps for inplace side Layer False Position
        :return: string[] of steps
        """
        self._messages += str('\n') + "inplace side Layer False Position" + str('\n')
        steps = ['']

        return steps

    def __tLFP(self):
        """
        solving steps for top Layer False Position
        :return: string[] of steps
        """
        self._messages += str('\n') + "top Layer False Position" + str('\n')
        steps = []
        intoPositon = self.__getOffsetTurnWorkLayer(self.__getFinishSideByColor(self._edge.piece[:][1]), -1)
        turnLayer = self.__getOffsetTurnWorkLayer(self.__getFinishSideByColor(self._edge.piece[:][1]), 1)
        if self._edge.Orientation1Layer != intoPositon:
            steps.extend(self.__turnOnWorkLayer(self.__getFinishSideByColor(self._edge.piece[:][1]),
                                                self._edge.Orientation1Layer, 1,
                                                offset=1))
        steps.extend(self.__threeSteps(turnLayer, True, False))
        steps.extend(self.__stepsOfSideByIndex(3, False, 1))
        turnLayer = self.__getFinishSideByColor(self._edge.piece[:][1])
        steps.extend(self.__threeSteps(turnLayer, False, True))
        return steps

    def __tL2FP(self):
        """
        solving steps for top Layer 2 False Position
        :return: string[] of steps
        """
        self._messages += str('\n') + "top Layer 2 False Position" + str('\n')
        steps = []

        intoPositon = self.__getOffsetTurnWorkLayer(self.__getFinishSideByColor(self._edge.piece[:][0]), 1)
        turnLayer = self.__getOffsetTurnWorkLayer(self.__getFinishSideByColor(self._edge.piece[:][0]), -1)
        if self._edge.positionLayer != intoPositon:
            steps.extend(self.__turnOnWorkLayer(self.__getFinishSideByColor(self._edge.piece[:][0]),
                                                self._edge.positionLayer, 1,
                                                offset=-1))
        steps.extend(self.__threeSteps(turnLayer, False, True))
        steps.extend(self.__stepsOfSideByIndex(3, True, 1))
        turnLayer = self.__getFinishSideByColor(self._edge.piece[:][0])
        steps.extend(self.__threeSteps(turnLayer, True, False))
        return steps

    def __sL2FP(self):
        """
        solving steps for side Layer 2 False Positon
        :return: string[] of steps
        """
        self._messages += str('\n') + "side Layer 2 False Positon" + str('\n')
        steps = []
        if self._edge.Orientation1 != [1, 0]:
            self._messages += str('\n') + "OrientationLayer" + str('\n')
            turnLayer = self._edge.Orientation1Layer
            steps.extend(self.__threeSteps(turnLayer, False, False))
            turnLayer = self._edge.Orientation1Layer
            steps.extend(self.__stepsOfSideByIndex(3, True, 1))
            steps.extend(self.__threeSteps(turnLayer, True, True))
            if self._edge.Orientation1Layer == self.__getFinishSideByColor(self._edge.piece[:][1]):
                steps.extend(self.__tL2CP())
            else:
                steps.extend(self.__tLFP())
        else:
            self._messages += str('\n') + "PositionLayer" + str('\n')
            turnLayer = self._edge.positionLayer
            steps.extend(self.__threeSteps(turnLayer, False, True))
            turnLayer = self.__getOffsetTurnWorkLayer(self._edge.positionLayer, 2)
            self._messages += str('\n') + str(turnLayer) + str("\t") + str(self._edge.positionLayer) + str("\n")
            steps.extend(self.__stepsOfSideByIndex(3, True, 1))
            steps.extend(self.__threeSteps(turnLayer, True, False))
            if self._edge.positionLayer == self.__getFinishSideByColor(self._edge.piece[:][0]):
                steps.extend(self.__tLCP())
            else:
                steps.extend(self.__tL2FP())

        return steps

    def __threeSteps(self, turnLayer, invert, workInvert):
        """
        repeater of typical steps
        :param turnLayer: int of layer index
        :param invert: boolean if invert
        :param workInvert: boolean worklayer invert
        :return: string[] of steps
        """
        steps = []
        change = False
        if invert:
            change = True
        steps.extend(self.__stepsOfSideByIndex(turnLayer, change, 1))
        steps.extend(self.__stepsOfSideByIndex(3, workInvert, 1))
        steps.extend(self.__stepsOfSideByIndex(turnLayer, not change, 1))
        return steps

    def __stepsOfSideByIndex(self, index, invert, addSteps):
        """
        turns the virtual cube
        :param index: int side index
        :param invert: boolean false = clockwise
        :param addSteps: int amount of turns default 1
        :return: string[] of steps
        """
        steps = []
        if index == 0:
            steps = ['L']
        elif index == 1:
            steps = ['U']
        elif index == 2:
            steps = ['F']
        elif index == 3:
            steps = ['D']
        elif index == 4:
            steps = ['R']
        elif index == 5:
            steps = ['B']
        output = []
        for index in range(addSteps):
            output.extend(steps)
        if invert:
            output = self.__invertedSteps(output)
        self.__changeOrientations(output)
        return output

    @staticmethod
    def __invertedSteps(steps):
        """
        invert steps from clockwise to anticlockwise or anticlockwise to clockwise
        :param steps: string[]
        :return: string[] of inverted steps
        """
        for index in range(len(steps)):
            if len(steps[index]) != 2:
                steps[index] += 'i'
            else:
                steps[index] = steps[index].split('i')[0]
        return steps

    def __changeOrientations(self, steps):
        """
         Turns the cube by steps and overrides the stone positions
         :param steps: string[] of steps
         :return:
         """
        self._UF.turnCubeByArray(steps)
        slPositions = self._position.searchSLPosition(self._stone, self._UF.getCube())
        self._edge = Edge(slPositions[0], slPositions[1], slPositions[2], slPositions[3])

    def __getOffsetTurnWorkLayer(self, currentLayer, positionOffset):
        """
        get the offset turnposition from currentlayer
        :param currentLayer: int currentlayer index
        :param positionOffset: int offset to finish layer
        :return:
        """
        roundLayers = [0, 2, 4, 5]
        index = roundLayers.index(currentLayer)
        if positionOffset >= 0:
            newIndex = index + positionOffset
            if newIndex >= len(roundLayers) - 1:
                return roundLayers[newIndex - len(roundLayers)]
            else:
                return roundLayers[newIndex]
        else:
            newIndex = index + positionOffset
            if newIndex < 0:
                return roundLayers[len(roundLayers) + newIndex]
            else:
                return roundLayers[newIndex]

    def __turnOnWorkLayer(self, solveIndex, currentIndex, correctCorection, **kwargs):
        """
        universal turn on work layer
        :param solveIndex: int finishside position
        :param currentIndex: int current postion
        :param correctCorection: int turn correction
        :param kwargs: config, offset
        :return: string[] of steps
        """
        defaultConfig = [['D'], ['Di'], ['D', 'D']]

        configOpposit = [[0, 4], [2, 5], [5, 2], [4, 0]]
        configLeft = [[0, 2], [2, 4], [4, 5], [5, 0]]
        configRight = [[0, 5], [5, 4], [4, 2], [2, 0]]
        config = kwargs.get('config', defaultConfig)
        offset = kwargs.get('offset', 0)

        con = [solveIndex, currentIndex]
        if self.__arrayInclude(configRight, con):
            steps = self.__offsetTurnOnWorkLayer(config, 0, offset)
        elif self.__arrayInclude(configLeft, con):
            steps = self.__offsetTurnOnWorkLayer(config, 1, offset)
        elif self.__arrayInclude(configOpposit, con):
            steps = self.__offsetTurnOnWorkLayer(config, 2, offset)
        else:
            if correctCorection == 100:
                steps = []
            else:
                steps = self.__offsetTurnOnWorkLayer(config, correctCorection, 0)
        self.__changeOrientations(steps)
        return steps

    def __offsetTurnOnWorkLayer(self, config, index, offset):
        """
        turns offset to finish layer
        :param config: string[] of steps config
        :param index: int current index
        :param offset: int turn offset
        :return: string[] of step/s
        """
        if offset >= 0:
            newIndex = index + offset
            if newIndex < len(config):
                return config[newIndex]
            else:
                return config[newIndex - len(config)]
        else:
            newIndex = index + offset
            if newIndex < 0:
                return config[len(config) + newIndex]
            else:
                return config[newIndex]

    @staticmethod
    def __getFinishSideByColor(color):
        """
        get finish index by main color
        :param color: string of color
        :return: int index of color
        """
        side = [0, 1, 2, 3, 4, 5]
        colors = ['G', 'W', 'R', 'Y', 'B', 'O']
        return side[colors.index(color)]

    @staticmethod
    def __getColorBySide(side):
        """
        get color of side by index
        :param side: int of side
        :return:
        """
        sides = [0, 1, 2, 3, 4, 5]
        colors = ['G', 'W', 'R', 'Y', 'B', 'O']
        return colors[sides.index(side)]

    @staticmethod
    def __getStepBySide(side):
        """
        get move of side index
        :param side: int of side
        :return: string of step
        """
        steps = ['L', 'U', 'F', 'D', 'R', 'B']
        sides = [0, 1, 2, 3, 4, 5]
        return steps[sides.index(side)]

    def __arrayInclude(self, array, search):
        """
        checks if mutliarray include search
        :param array: any[]
        :param search: search type
        :return: boolean if found
        """
        for ar in array:
            if ar == search:
                return True
        return False
