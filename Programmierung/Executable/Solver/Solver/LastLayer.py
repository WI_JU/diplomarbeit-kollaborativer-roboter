import Utilities.UseFunctions
from Utilities.Positions import Positions
import numpy as np


class LastLayer:
    def __init__(self, UF: Utilities.UseFunctions.UseFunctions):
        self._UF = UF
        self._position = Positions()
        self._length = 0
        self._messages = ""
        self._LastLayer = []
        self._SolvedLastLayer = [
            ['G', 'G', 'G'],
            ['R', 'R', 'R'],
            ['B', 'B', 'B'],
            ['O', 'O', 'O'],
        ]

    def getLen(self):
        """
        adds up all moves
        :return: int of moves
        """
        return self._length

    def getMessages(self):
        """
        the messages of the solver
        :return: string of messages
        """
        return self._messages

    def canSolveCube(self):
        """
        checks if can solve virtual cube
        :return: boolean can/cannot solve
        """
        steps = self.__generateSteps()
        if not steps:
            return False
        else:
            self._length += len(steps)
            return True

    def getSteps(self):
        """
        solves Cube and return steps to solve
        :return: string[] of steps
        """
        steps = self.__generateSteps()
        return steps

    def getSolvedCube(self):
        """
        gives a copy of partly solved cube
        :return: string[] of virtual cube
        """
        steps = self.__generateSteps()
        return self._UF.getCube()


    @staticmethod
    def __getFinishSideByColor(color):
        """
        get finish index by main color
        :param color: string of color
        :return: int index of color
        """
        side = [0, 1, 2, 3, 4, 5]
        colors = ['G', 'W', 'R', 'Y', 'B', 'O']
        return side[colors.index(color)]

    def __generateSteps(self):
        """
        solve all cases of the last layer
        :return:
        """
        self._messages = ""
        allsteps = []
        self._LastLayer = self.__generateSideArray()
        for i in range(10):
            if self.isEqual(self._LastLayer, self._SolvedLastLayer):
                return allsteps
            steps = []
            [doubleCount, self._sides] = self.__countDoubleSignalsandSideOfDouble()
            if doubleCount == 0:
                steps.extend(self._noSignals())
            elif doubleCount == 4:
                if self.__hasOnlyFullSignals():
                    steps.extend(self._onlyFullSignal())
                elif self.__hasOneFullSignals():
                    steps.extend(self._oneFullSignal())
                else:
                    steps.extend(self._onlyHalfSignals())
            else:
                steps.extend(self._oneHalfSignal())

            self.__changeOrientations(steps)
            allsteps.extend(steps)
            self._messages += str(steps)
            self._LastLayer = self.__generateSideArray()
            if self.isEqual(self._LastLayer, self._SolvedLastLayer):
                return allsteps
        return allsteps

    def _noSignals(self):
        """
        solve if no signals are shown
        :return:
        """
        self._messages += str('\n') + str("No Signals") + str('\n')
        steps = []
        side = self._LastLayer[0]
        indexSide = self._LastLayer.index(side)
        turnLayer = self._getTurnLayerBySideIndex(indexSide)
        turnLayerOffset1 = self.__getOffsetTurnWorkLayer(turnLayer,1)
        turnLayerOffset2 = self.__getOffsetTurnWorkLayer(turnLayer, 2)
        steps.extend(self._reoriantatedCorner(turnLayer,turnLayerOffset1,turnLayerOffset2))
        return steps

    def _oneFullSignal(self):
        """
        solve if one full signals are shown
        :return:
        """
        self._messages += str('\n') + str("One Full Signal") + str('\n')
        steps = []
        side = self._getTheOneFullSignal()
        indexSide = self._LastLayer.index(side)
        turnLayer = self._getTurnLayerBySideIndex(indexSide)
        turnLayerOffset2 = self.__getOffsetTurnWorkLayer(turnLayer, -1)
        steps.extend(self._lastAlgorythm(turnLayerOffset2))
        return steps

    def _onlyFullSignal(self):
        """
        solve if only full signals are shown
        :return:
        """
        self._messages += str('\n') + str("Only Full Signal") + str('\n')
        steps = []
        midStone = self._LastLayer[0][1]
        currentLayer = 0
        solveStone = self.__getFinishSideByColor(midStone)
        solveIndex = 0
        if solveStone == 0:
            solveIndex = 0
        elif solveStone == 2:
            solveIndex = 1
        elif solveStone == 4:
            solveIndex = 2
        elif solveStone == 5:
            solveIndex = 3
        stepsToGo = solveIndex - currentLayer
        if stepsToGo == 1 or stepsToGo == -3:
            steps.extend(self.__stepsOfSideByIndex(3,False,1))
        elif stepsToGo == 2 or stepsToGo == -2:
            steps.extend(self.__stepsOfSideByIndex(3,False,2))
        elif stepsToGo == 1 or stepsToGo == 3:
            steps.extend(self.__stepsOfSideByIndex(3,True,1))
        else:
            steps.extend(' ')
        return steps

    def _onlyHalfSignals(self):
        """
        solve if only half signals are shown
        :return:
        """
        self._messages += str('\n') + str("Only Half Signals") + str('\n')
        steps = []
        side = self._sides[0]
        indexSide = self._LastLayer.index(side)
        turnLayer = self._getTurnLayerBySideIndex(indexSide)
        turnLayerOffset2 = self.__getOffsetTurnWorkLayer(turnLayer, -1)
        steps.extend(self._lastAlgorythm(turnLayerOffset2))
        return steps

    def _oneHalfSignal(self):
        """
        solve if one half signals are shown
        :return:
        """
        self._messages += str('\n') + str("At least One Signal") + str('\n')
        steps = []
        side = self._sides[0]
        indexSide = self._LastLayer.index(side)
        turnLayer = self._getTurnLayerBySideIndex(indexSide)
        turnLayerOffset1 = self.__getOffsetTurnWorkLayer(turnLayer, 1)
        turnLayerOffset2 = self.__getOffsetTurnWorkLayer(turnLayer, 2)
        steps.extend(self._reoriantatedCorner(turnLayer, turnLayerOffset1, turnLayerOffset2))
        return steps

    def _getTurnLayerBySideIndex(self,indexSide):
        """
        get turns layer index by the finish side index
        :param indexSide: int index of side
        :return: int turnlayer index
        """
        if indexSide == 0:
            return 0
        elif indexSide == 1:
            return 2
        elif indexSide == 2:
            return 4
        else:
            return 5
    
    def _lastAlgorythm(self, tL):
        """
        the finish alogrythm to solve the cube
        :param tL: int turnLayer index
        :return: string[] of steps
        """
        steps = []
        steps.extend(self.__stepsOfSideByIndex(tL, True, 1))
        steps.extend(self.__stepsOfSideByIndex(3,False,1))
        steps.extend(self.__stepsOfSideByIndex(tL, True, 1))
        steps.extend(self.__stepsOfSideByIndex(3,True,1))
        steps.extend(self.__stepsOfSideByIndex(tL, True, 1))
        steps.extend(self.__stepsOfSideByIndex(3,True,1))
        steps.extend(self.__stepsOfSideByIndex(tL, True, 1))
        steps.extend(self.__stepsOfSideByIndex(3,False,1))
        steps.extend(self.__stepsOfSideByIndex(tL, False, 1))
        steps.extend(self.__stepsOfSideByIndex(3,False,1))
        steps.extend(self.__stepsOfSideByIndex(tL, False, 2))
        
        return steps
    
    
    def _reoriantatedCorner(self,tL,tL1,tL2):
        """
        reorintate the Corners of last layer
        :param tL: int turnLayer index
        :param tL1: int turnLayer offset 1 index
        :param tL2: int turnLayer offset 2 index
        :return: string[] of steps
        """
        steps = []
        steps.extend(self.__stepsOfSideByIndex(tL1,True,1))
        steps.extend(self.__stepsOfSideByIndex(tL2, False, 1))
        steps.extend(self.__stepsOfSideByIndex(tL1, True, 1))
        steps.extend(self.__stepsOfSideByIndex(tL, False, 2))
        steps.extend(self.__stepsOfSideByIndex(tL1, False, 1))
        steps.extend(self.__stepsOfSideByIndex(tL2, True, 1))
        steps.extend(self.__stepsOfSideByIndex(tL1, True, 1))
        steps.extend(self.__stepsOfSideByIndex(tL, False, 2))
        steps.extend(self.__stepsOfSideByIndex(tL1, False, 2))
        return steps

    def __countDoubleSignalsandSideOfDouble(self):
        """
        count Double Signal and returns their postion
        :return: [int, int[]] number of signals, positions of signals
        """
        count = 0
        outputSide = []
        for side in self._LastLayer:
            if side[0] == side[2]:
                count += 1
                outputSide.append(side)
        return [count, outputSide]
    
    def _getTheOneFullSignal(self):
        """
        positions of full signals
        :return: int[] positions of signals
        """
        for side in self._LastLayer:
            if side[0] == side[2] and side[0] == side[1]:
                return side
    
    def __hasOnlyFullSignals(self):
        """
        checks if cube has only full signals
        :return: boolean
        """
        checkArray = []
        for side in self._LastLayer:
            if side[0] == side[2] and side[0] == side[1]:
                checkArray.append(True)
            else:
                checkArray.append(False)
        if False in checkArray:
            return False
        else:
            return True

    def __hasOneFullSignals(self):
        """
        checks if cube has one full signals
        :return: boolean
        """
        for side in self._LastLayer:
            if side[0] == side[2] and side[0] == side[1]:
                return True
        return False

    def __generateSideArray(self):
        """
        generates a Yellow based matrix of layer and replace all others with X
        :return: string[] of cleaned side
        """
        cube = self._UF.getCube().copy()
        sideArray = [cube[0][2], cube[2][2], cube[4][2], cube[5][2]]
        outputArray = []
        for i in range(len(sideArray)):
            outputArray.append(list(sideArray[i]))
        return outputArray

    @staticmethod
    def isEqual(state1, musterState):
        """
        checks if two arrays are equal (X == all true)
        :param state1: search option
        :param musterState: base option
        :return: boolean
        """
        for i in range(len(musterState)):
            for j in range(len(musterState[i])):
                if musterState[i][j] == 'X':
                    musterState[i][j] = state1[i][j]
        solvedStateArray = state1.copy()
        if np.array_equal(musterState, solvedStateArray):
            return True
        else:
            return False

    def __stepsOfSideByIndex(self, index, invert, addSteps):
        """
        turns the virtual cube
        :param index: int side index
        :param invert: boolean false = clockwise
        :param addSteps: int amount of turns default 1
        :return: string[] of steps
        """
        steps = []
        if index == 0:
            steps = ['L']
        elif index == 1:
            steps = ['U']
        elif index == 2:
            steps = ['F']
        elif index == 3:
            steps = ['D']
        elif index == 4:
            steps = ['R']
        elif index == 5:
            steps = ['B']
        output = []
        for index in range(addSteps):
            output.extend(steps)
        if invert:
            output = self.__invertedSteps(output)
        return output

    @staticmethod
    def __invertedSteps(steps):
        """
        invert steps from clockwise to anticlockwise or anticlockwise to clockwise
        :param steps: string[]
        :return: string[] of inverted steps
        """
        for index in range(len(steps)):
            if len(steps[index]) != 2:
                steps[index] += 'i'
            else:
                steps[index] = steps[index].split('i')[0]
        return steps

    def __changeOrientations(self, steps):
        """
         Turns the cube by steps and overrides the stone positions
         :param steps: string[] of steps
         :return:
         """
        self._UF.turnCubeByArray(steps)

    def __getOffsetTurnWorkLayer(self, currentLayer, positionOffset):
        """
        get the offset turnposition from currentlayer
        :param currentLayer: int currentlayer index
        :param positionOffset: int offset to finish layer
        :return:
        """
        roundLayers = [0, 2, 4, 5]
        index = roundLayers.index(currentLayer)
        if positionOffset >= 0:
            newIndex = index + positionOffset
            if newIndex >= len(roundLayers) - 1:
                return roundLayers[newIndex - len(roundLayers)]
            else:
                return roundLayers[newIndex]
        else:
            newIndex = index + positionOffset
            if newIndex < 0:
                return roundLayers[len(roundLayers) + newIndex]
            else:
                return roundLayers[newIndex]

    def __turnOnWorkLayer(self, solveIndex, currentIndex, correctCorection, **kwargs):
        """
        universal turn on work layer
        :param solveIndex: int finishside position
        :param currentIndex: int current postion
        :param correctCorection: int turn correction
        :param kwargs: config, offset
        :return: string[] of steps
        """
        defaultConfig = [['D'], ['Di'], ['D', 'D']]

        configOpposit = [[0, 4], [2, 5], [5, 2], [4, 0]]
        configLeft = [[0, 2], [2, 4], [4, 5], [5, 0]]
        configRight = [[0, 5], [5, 4], [4, 2], [2, 0]]
        config = kwargs.get('config', defaultConfig)
        offset = kwargs.get('offset', 0)

        con = [solveIndex, currentIndex]
        if self.__arrayInclude(configRight, con):
            steps = self.__offsetTurnOnWorkLayer(config, 0, offset)
        elif self.__arrayInclude(configLeft, con):
            steps = self.__offsetTurnOnWorkLayer(config, 1, offset)
        elif self.__arrayInclude(configOpposit, con):
            steps = self.__offsetTurnOnWorkLayer(config, 2, offset)
        else:
            if correctCorection == 100:
                steps = []
            else:
                steps = self.__offsetTurnOnWorkLayer(config, correctCorection, 0)
        self.__changeOrientations(steps)
        return steps

    def __offsetTurnOnWorkLayer(self, config, index, offset):
        """
        turns offset to finish layer
        :param config: string[] of steps config
        :param index: int current index
        :param offset: int turn offset
        :return: string[] of step/s
        """
        if offset >= 0:
            newIndex = index + offset
            if newIndex < len(config):
                return config[newIndex]
            else:
                return config[newIndex - len(config)]
        else:
            newIndex = index + offset
            if newIndex < 0:
                return config[len(config) + newIndex]
            else:
                return config[newIndex]

    def __arrayInclude(self, array, search):
        """
        checks if mutliarray include search
        :param array: any[]
        :param search: search type
        :return: boolean if found
        """
        for ar in array:
            if ar == search:
                return True
        return False
