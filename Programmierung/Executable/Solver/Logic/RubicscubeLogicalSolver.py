from Solver.RCSolver import RubicsCubeSolver
from Utilities.SolvedArrays import solvedArrays
from Utilities.UseFunctions import UseFunctions
from Utilities.Utilits import findMissingStones
import numpy as np

UseFunction = UseFunctions()
RCSolver = RubicsCubeSolver(UseFunction)


def solveBySteps(steps=[]):
    """
    Testing function to simulate virtual cube by first turn it
    :param steps: list[] of steps
    :return:
    """
    solvedArray = solvedArrays().getSolved()
    UF = UseFunctions()
    UF.setCube(solvedArray)
    UF.turnCubeByArray(steps)
    turnedArray = UF.getCube()
    print("\n\n\n\n")
    print(turnedArray)
    stepsArray = RCSolver.generateSolvingSteps(turnedArray)
    return RCSolver.generateArrayOfSteps(stepsArray)


class RubicscubeSolverLogic:

    def generateSolvingSteps(self, inputArray):
        """
        Checks if cube is valid and then solve it
        :param inputArray: string[] virtual cube
        :return: list[] of steps / false if invalid
        """
        if findMissingStones(inputArray) == []:
            solvingSteps = self._solveInputCube(inputArray)
            return solvingSteps
        else:
            return False

    def _solveInputCube(self, inputCube):
        """
        Solves virtual cube
        :param inputCube: stirng[] virtual cube
        :return: list[] of steps
        """
        inputCube = np.array(inputCube)
        stepsArray = RCSolver.generateSolvingSteps(inputCube)
        return RCSolver.generateArrayOfSteps(stepsArray)
