import socket
from time import sleep

import numpy as np

import Utilities.Utilits
from Camera.CameraMain import CameraPrediction
from Logic.RubicscubeLogicalSolver import RubicscubeSolverLogic
from Utilities.GenerateRandom import GenerateRandom
from Utilities.SolvedArrays import solvedArrays


class Communication:

    _clientSocket = socket.socket()
    _ipAddress = "192.168.125.1"
    _port = 5516
    _virtualIPAddress = '127.0.0.1'
    _virtualPort = 55000
    # Classes
    _cameraPrediction = CameraPrediction()
    _rubicscubeSolverLogic = RubicscubeSolverLogic()

    # keep track of connection status
    _connected = False
    _handleCases = ["GetSolvedCube", "CloseConnection", "CameraPosition", "NextStep"]
    _VALID_CASE = "VALID"
    _INVALID_CASE = "INVALID"
    _FINISHED_SEQUENCE = "FINISH"
    communicationState = {
        'state': 1,
        'stepIndex': 0,
        'solvedArray': [],
        'virtualCopy': solvedArrays().getSolved(),
        'isRunning': False,
        'validate': 0,
    }
    # Camera-handling
    _FRAME = []

    def communicate(self, frame=[], DEBUG=False):
        """
        Starts the communication with the Roboter if there is a connection
        :param frame: the current Frame of the camera
        :param DEBUG: switches the IP and Port true == simulation
        :return:
        """
        self._FRAME = frame
        if DEBUG:
            self._ipAddress = self._virtualIPAddress
            self._port = self._virtualPort
        try:
            msg = self._clientSocket.recv(1024).decode("UTF-8")
            if msg != "":
                self._inputHandler(msg)
        except socket.error:
            # set connection status and recreate socket
            self._connected = False
            self._clientSocket = socket.socket()
            print("connection lost... reconnecting")
            # while not self._connected:
            # attempt to reconnect, otherwise sleep for 2 seconds
            try:
                self._clientSocket.connect((self._ipAddress, self._port))
                self._connected = True
                print("re-connection successful")
                self.communicationState['isRunning'] = True
            except socket.error:
                self.communicationState['isRunning'] = False
        return self.communicationState

    def _inputHandler(self, inputMessage):
        """
        Handles all InputMessages from the socket
        :param inputMessage: string message from socket
        :return:
        """
        print(f"Received: {inputMessage}")
        if inputMessage == self._handleCases[0]:
            self.__solveHandler()
            self.communicationState['state'] = 0
        elif inputMessage == self._handleCases[1]:
            self._clientSend(self._VALID_CASE)
            self._clientSocket.close()
            self.communicationState['state'] = 1
        elif inputMessage == self._handleCases[3]:
            self.communicationState['state'] = 3
            self.communicationState['stepIndex'] = self._currentStepIndex
            self.communicationState['solvedArray'] = self._solvedArray
            if self._activateSolvedStates and self._currentStepIndex < len(self._solvedArray):
                self._clientSend(str(self._solvedArray[self._currentStepIndex]))
                self._currentStepIndex += 1
            elif self._activateSolvedStates and self._currentStepIndex >= len(self._solvedArray):
                self._clientSend(self._FINISHED_SEQUENCE)
                self.communicationState['validate'] = 0
        else:
            inputMessage = inputMessage.split(",")
            useCase = inputMessage[0]
            if useCase == self._handleCases[2]:
                self.communicationState['state'] = 2
                self._cameraHandler(inputMessage)
            else:
                print("Unknown Case")

    def _cameraHandler(self, inputMessage):
        """
        Start taking pictures from current frame, response with Valid or Invalid
        :param inputMessage: string HandelCase[2]
        :return:
        """
        number = int(inputMessage[1])
        try:
            invalid = self._cameraPrediction.takePicture(number, self._FRAME)
        except:
            invalid = True
        if not invalid:
            self._clientSend(self._VALID_CASE)
        else:
            self._clientSend(self._INVALID_CASE)

    def __solveHandler(self):
        """
        From all Pictures generates imageArray and then solve the virtual cube
        :return:
        """
        cameraArray = self._cameraPrediction.generatePictureArray()
        # cameraArray = GenerateRandom().generateNewCube().copy()
        self.communicationState['virtualCopy'] = cameraArray.copy()
        if Utilities.Utilits.findMissingStones(cameraArray.copy()) == []:
            self.communicationState['validate'] = 2
            self.executeSovleHandler(cameraArray.copy())
        else:
            self.communicationState['validate'] = 1

    def executeSovleHandler(self, cameraArray):
        print("Start Solve Handler")
        self._clientSend(self._VALID_CASE)
        steps = self._rubicscubeSolverLogic.generateSolvingSteps(cameraArray)
        if steps != False:
            self._solvedArray = steps
            self._activateSolvedStates = True
            self._currentStepIndex = 0
            self._clientSend(self._VALID_CASE)
        else:
            self._activateSolvedStates = False
            self._clientSend(self._INVALID_CASE)


    def _clientSend(self, sendMessage):
        """
        Sends message to the socket
        :param sendMessage: strinsg message
        :return:
        """
        print(f"Sent: {sendMessage}")
        sleep(0.05)
        self._clientSocket.send(bytes(sendMessage, "UTF-8"))
