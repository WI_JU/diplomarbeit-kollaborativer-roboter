echo off
goto run
:run
    :: Create User RCSolver
    net user RCSolver
    if %ERRORLEVEL% EQU 0 (
        echo User RCSolver exist already
    )else (
        net user RCSolver rcsolver /add
        echo User RCSolver is created
    )
    goto createLinks
    :: Exit
    goto exit

:createLinks
	XCopy "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\RubicsCubeSolver\RubicsCubeSolver.lnk" "C:\Users\RCsolver\Desktop\"
	if %ERRORLEVEL% EQU 0 echo Links already exist
	XCopy "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\RubicsCubeSolver\RubicsCubeSolver.lnk" "C:\Users\RCsolver\AppData\Roaming\Microsoft\Windows\Start Menu\Programs\Startup\"
	if %ERRORLEVEL% EQU 0 echo Links already exist
	
:createAutoStartRegistry
	REG ADD "HKLM\Software\Microsoft\Windows NT\CurrentVersion\Winlogon" /v AutoAdminLogon /t REG_SZ /d 1 /f
	if %ERRORLEVEL% EQU 0 echo Autologin already exist
	REG ADD "HKLM\Software\Microsoft\Windows NT\CurrentVersion\Winlogon" /v DefaultDomainName /t REG_SZ /d %USERDOMAIN% /f
	if %ERRORLEVEL% EQU 0 echo Autologin already exist
	REG ADD "HKLM\Software\Microsoft\Windows NT\CurrentVersion\Winlogon" /v DefaultUserName /t REG_SZ /d RCSolver /f
	if %ERRORLEVEL% EQU 0 echo Autologin already exist
	REG ADD "HKLM\Software\Microsoft\Windows NT\CurrentVersion\Winlogon" /v DefaultPassword /t REG_SZ /d rcsolver /f
	if %ERRORLEVEL% EQU 0 echo Autologin already exist

:exit
    PING 127.0.0.1 > NUL 2>&1
    EXIT /B 1
