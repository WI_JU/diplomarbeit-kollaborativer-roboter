MODULE SolveMethods
    CONST robtarget rT_Ui:=[[0,-30.5,19],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.964433017,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Ui_Offset:=[[0,-60.5,19],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.964433017,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Di_Offset:=[[0,-60.5,-19],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.964433017,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Di:=[[0,-30.5,-19],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.964433017,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Haltepunkt_R:=[[0,-30.5,0],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.964433017,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Haltepunkt_R_Offset:=[[0,-60.5,0],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.960017596,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Ue:=[[30.5,0,19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Ue_Offset:=[[60.5,0,19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_De:=[[30.5,0,-19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_De_Offset:=[[60.5,0,-19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_R_Offset:=[[0,-60.5,0],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.960017596,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_R:=[[0,-30.5,0],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.964433017,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Uie:=[[30.5,0,19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Uie_Offset:=[[60.5,0,19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Die:=[[30.5,0,-19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Die_Offset:=[[60.5,0,-19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    CONST robtarget rT_Fi:=[[-19,-30.5,0],[0.5,-0.5,-0.5,-0.5],[1,-1,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Fi_Offset:=[[-19,-60.5,0],[0.5,-0.5,-0.5,-0.5],[1,-1,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Fie:=[[-19,0,30.5],[0,0.707106781,0.707106781,0],[1,-3,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Fie_Offset:=[[-19,0,60.5],[0,0.707106781,0.707106781,0],[1,-3,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    CONST robtarget rT_B:=[[19,-30.5,0],[0.5,-0.5,-0.5,-0.5],[1,-1,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_B_Offset:=[[19,-60.5,0],[0.5,-0.5,-0.5,-0.5],[1,-1,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Be:=[[19,0,30.5],[0,0.707106781,0.707106781,0],[1,-3,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Be_Offset:=[[19,0,60.5],[0,0.707106781,0.707106781,0],[1,-3,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    ! Save if was in HoldPosition
    VAR bool wasWork := FALSE;
    VAR bool isHold1 := TRUE;
    VAR bool isHold2 := FALSE;
    VAR bool nextStepChangeHold := FALSE;
    ! Save next Step
    VAR string nextStep := "";
    ! Radius from Center to RC1 Point 
    var num Radius := 30.5;
    var num RadiusOffset := 60.5;
    
    PROC solveCube()
        VAR string currentStep := "";
        VAR string stepBevore := "";
        
        MoveJ rT_Haltepunkt_R, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;
         FOR i FROM 1 TO maxSolveLength DO
            ! Get the next Step
            IF i < maxSolveLength THEN
                nextStep := solvedArray{i+1};
            ELSE
                nextStep := "";
            ENDIF
            IF NOT i = 1 THEN
                stepBevore := solvedArray{i-1};
            ELSE
                stepBevore := "";
            ENDIF
            currentStep := solvedArray{i};
            IF ((stepBevore = "Fi" OR stepBevore = "B") AND (NOT (currentStep = "Fi" OR currentStep = "B"))) OR ((NOT (stepBevore = "Fi" OR stepBevore = "B")) AND (currentStep = "Fi" OR currentStep = "B")) THEN
                nextStepChangeHold := TRUE;
            ELSE
                nextStepChangeHold := FALSE;
            ENDIF
            WaitSyncTask syncSolveCubeStart,task_list;
            IF currentStep = "" THEN
                RETURN;
            ELSEIF currentStep = "L" OR currentStep = "Li" THEN
                hold1;
            ELSEIF currentStep = "R" THEN
                work;
                path_GoR;
            ELSEIF currentStep = "Ri" THEN
                work;
                path_GoRi;                
            ELSEIF currentStep = "F" THEN
                hold2;
            ELSEIF currentStep = "Fi" THEN
                work;
                path_GoFi;
            ELSEIF currentStep = "B" THEN
                work;
                path_GoB;
            ELSEIF currentStep = "Bi" THEN
                hold2;
            ELSEIF currentStep = "U" THEN
                hold1;
            ELSEIF currentStep = "Ui" THEN
                work;
                path_GoUi;
            ELSEIF currentStep = "D" THEN
                work;
                path_GoD;
            ELSEIF currentStep = "Di" THEN
                hold1;
            ENDIF
            WaitSyncTask syncSolveCubeStop,task_list;
         ENDFOR
    ENDPROC
    
    PROC hold1()
        IF wasWork THEN 
            WaitSyncTask syncStartHold, task_list;
            MoveJ rT_Haltepunkt_R_Offset, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL rT_Haltepunkt_R, v100, fine, Servo_RC1\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;   
            smartGripperOpen;
            MoveL rT_Haltepunkt_R, v100, fine, Servo_RC2\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncStopHold,task_list;
        ELSEIF isHold1 THEN
            WaitSyncTask syncStartHold, task_list;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;   
            WaitSyncTask syncStopHold,task_list;
        ELSEIF isHold2 THEN
            WaitSyncTask syncStartHold, task_list;
            smartGripperOpen;
            MoveL RelTool(rT_Haltepunkt_R,0,0,0\Rx:=0\Ry:=0\Rz:=90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;   
            smartGripperOpen;
            MoveL RelTool(rT_Haltepunkt_R_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=90),v100,z10,Servo_RC1\WObj:=wobj_Workingpoint;
            MoveJ rT_Haltepunkt_R_Offset, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL rT_Haltepunkt_R, v100, fine, Servo_RC2\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncStopHold,task_list;
        ENDIF
        wasWork := FALSE;
        isHold2 := FALSE; 
        isHold1 := TRUE;            
    ENDPROC 
    
    PROC hold2()
        IF wasWork THEN 
            WaitSyncTask syncStartHold, task_list;
            MoveJ RelTool(rT_Haltepunkt_R_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=90),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL RelTool(rT_Haltepunkt_R,0,0,0\Rx:=0\Ry:=0\Rz:=90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;   
            smartGripperOpen;
            MoveL RelTool(rT_Haltepunkt_R,0,0,0\Rx:=0\Ry:=0\Rz:=90),v100,fine,Servo_RC2\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncStopHold,task_list;
        ELSEIF isHold1 THEN
            WaitSyncTask syncStartHold, task_list;
            smartGripperOpen;
            MoveL rT_Haltepunkt_R, v100, fine, Servo_RC1\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;   
            smartGripperOpen;
            MoveL rT_Haltepunkt_R_Offset, v100, z10, Servo_RC1\WObj:=wobj_Workingpoint;
            MoveJ RelTool(rT_Haltepunkt_R_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=90),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL RelTool(rT_Haltepunkt_R,0,0,0\Rx:=0\Ry:=0\Rz:=90),v100,fine,Servo_RC2\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncStopHold,task_list;
        ELSEIF isHold2 THEN
            WaitSyncTask syncStartHold, task_list;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;   
            WaitSyncTask syncStopHold,task_list;
        ENDIF
        wasWork := FALSE;
        isHold2 := TRUE; 
        isHold1 := FALSE;            
    ENDPROC
    
    PROC work()
        IF wasWork AND (NOT nextStepChangeHold) THEN
            WaitSyncTask syncStartHold, task_list;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;   
            WaitSyncTask syncStopHold,task_list;
        ELSEIF wasWork AND nextStepChangeHold THEN
            MoveL rT_Haltepunkt_R, v100, fine, Servo_RC1\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncStartHold, task_list;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;   
            WaitSyncTask syncStopHold,task_list;
            smartGripperOpen;
            MoveL rT_Haltepunkt_R_Offset, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;
        ELSEIF isHold1 THEN 
            WaitSyncTask syncStartHold, task_list;
            WaitSyncTask syncSwitchStart, task_list;
            smartGripperOpen;
            MoveL rT_Haltepunkt_R, v100, fine, Servo_RC1\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncSwitchStop, task_list;            
            WaitSyncTask syncStopHold,task_list;
            smartGripperOpen;
            MoveL rT_Haltepunkt_R_Offset, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;    
        ELSEIF isHold2 THEN
            WaitSyncTask syncStartHold, task_list;
            WaitSyncTask syncSwitchStart, task_list;
            smartGripperOpen;
            MoveL RelTool(rT_Haltepunkt_R,0,0,0\Rx:=0\Ry:=0\Rz:=90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncSwitchStop, task_list;            
            WaitSyncTask syncStopHold,task_list;
            smartGripperOpen;
            MoveL RelTool(rT_Haltepunkt_R_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=90),v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
            MoveJ rT_Haltepunkt_R_Offset, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;
        ENDIF
        wasWork := TRUE;
        isHold2 := FALSE; 
        isHold1 := FALSE;  
    ENDPROC

    
    PROC path_GoR()
        MoveL rT_R,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveL RelTool(rT_R,0,0,0\Rx:=0\Ry:=0\Rz:=90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL RelTool(rT_R_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Haltepunkt_R_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPoint;
    ENDPROC
    
     PROC path_GoRi()
        MoveL rT_R,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveL RelTool(rT_R,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL RelTool(rT_R_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Haltepunkt_R_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPoint;
    ENDPROC
    
    PROC path_GoUi()
        MoveJ rT_Ui_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Ui,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_Ui,sin(45)*Radius,Radius - cos(45)*Radius,0),rT_Uie,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_Uie_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPXY(rT_Ui_Offset);
    ENDPROC
    
    PROC path_GoD()
        MoveJ rT_Di_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Di,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_Di,sin(45)*Radius,Radius - cos(45)*Radius,0),rT_Die,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_Die_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPXY(rT_Di_Offset);
    ENDPROC
    
    PROC path_GoFi()
        MoveJ rT_Fi_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Fi,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_Fi,0,Radius - cos(45)*Radius,sin(45)*Radius),rT_Fie,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_Fie_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPYZ(rT_Fi_Offset);
    ENDPROC
    
    PROC path_GoB()
        MoveJ rT_B_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_B,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_B,0,Radius - cos(45)*Radius,sin(45)*Radius),rT_Be,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_Be_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPYZ(rT_B_Offset);
    ENDPROC
    
    PROC path_GoWorkPoint()
        MoveJ rT_Haltepunkt_R_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
    ENDPROC
    
    PROC path_GoWorkPointCPXY(robtarget startPoint)
        MoveC Offs(startPoint,sin(45)*RadiusOffset,RadiusOffset - cos(45)*RadiusOffset,0),startPoint,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoAfterCP;
    ENDPROC
    
    PROC path_GoWorkPointCPYZ(robtarget startPoint)
        MoveC Offs(startPoint,0,RadiusOffset - cos(45)*RadiusOffset,sin(45)*RadiusOffset),startPoint,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoAfterCP;
    ENDPROC
    
    PROC path_GoAfterCP()
        IF nextStep = "B" THEN
            MoveJ RelTool(rT_Haltepunkt_R_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=90),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        ELSEIF nextStep = "Fi" THEN
            MoveJ RelTool(rT_Haltepunkt_R_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=90),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        ELSE
            path_GoWorkPoint;
        ENDIF
    ENDPROC
    
    PROC path_GoHaltepunkt_R()
        MoveL rT_Haltepunkt_R,v1000,fine,Servo_RC2\WObj:=wobj_Workingpoint;
    ENDPROC
    PROC path_GoHaltepunkt_R_start()
        MoveJ rT_Haltepunkt_R_Offset,v1000,z100,Servo_RC2\WObj:=wobj_Workingpoint;
        MoveL rT_Haltepunkt_R,v1000,fine,Servo_RC2\WObj:=wobj_Workingpoint;
    ENDPROC
    
ENDMODULE 