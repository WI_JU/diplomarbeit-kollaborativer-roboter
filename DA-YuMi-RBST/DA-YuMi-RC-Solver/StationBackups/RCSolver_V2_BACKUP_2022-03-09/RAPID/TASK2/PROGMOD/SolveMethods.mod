MODULE SolveMethods
    CONST robtarget rT_D:=[[0,30.5,-19],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_D_Offset:=[[0,60.5,-19],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_L:=[[0,30.5,0],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_L_Offset:=[[0,60.5,0],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Haltepunkt_L:=[[0,30.5,0],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Haltepunkt_L_Offset:=[[0,60.5,0],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_U:=[[0,30.5,19],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_U_Offset:=[[0,60.5,19],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_De:=[[30.5,0,-19],[0.5,-0.5,-0.5,0.5],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_De_Offset:=[[60.5,0,-19],[0.5,-0.5,-0.5,0.5],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Ue:=[[30.5,0,19],[0.5,-0.5,-0.5,0.5],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Ue_Offset:=[[60.5,0,19],[0.5,-0.5,-0.5,0.5],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    CONST robtarget rT_F:=[[-19,30.5,0],[0.5,0.5,-0.5,0.5],[-1,1,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_F_Offset:=[[-19,60.5,0],[0.5,0.5,-0.5,0.5],[-1,1,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Fe:=[[-19,0,30.5],[0,-0.707106781,0.707106781,0],[-1,2,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Fe_Offset:=[[-19,0,60.5],[0,-0.707106781,0.707106781,0],[-1,2,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Bi:=[[19,30.5,0],[0.5,0.5,-0.5,0.5],[-1,1,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Bi_Offset:=[[19,60.5,0],[0.5,0.5,-0.5,0.5],[-1,1,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Bie:=[[19,0,30.5],[0,-0.707106781,0.707106781,0],[-1,2,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Bie_Offset:=[[19,0,60.5],[0,-0.707106781,0.707106781,0],[-1,2,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Haltepunkt_L_Offset_P:=[[0,60.5,0],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Haltepunkt_L_P:=[[0,30.5,0],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    
    ! Save if was in HoldPosition
    VAR bool wasWork := TRUE;
    VAR bool isHold1 := FALSE;
    VAR bool isHold2 := FALSE;
    VAR bool nextStepChangeHold := FALSE;
    ! Save next Step
    VAR string nextStep := "";
    ! Radius from Center to RC1 Point 
    var num Radius := 30.5;
    var num RadiusOffset := 60.5;

    PROC solveCube()
        VAR string currentStep := "";
        VAR string stepBevore := "";
        MoveJ rT_Haltepunkt_L_Offset, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;
         FOR i FROM 1 TO maxSolveLength DO
            ! Get the next Step
            IF i < maxSolveLength THEN
                nextStep := solvedArray{i+1};
            ELSE
                nextStep := "";
            ENDIF
            IF NOT i = 1 THEN
                stepBevore := solvedArray{i-1};
            ELSE
                stepBevore := "";
            ENDIF
            currentStep := solvedArray{i};
            IF ((stepBevore = "F" OR stepBevore = "Bi") AND (NOT (currentStep = "F" OR currentStep = "Bi"))) OR ((NOT (stepBevore = "F" OR stepBevore = "Bi")) AND (currentStep = "F" OR currentStep = "Bi")) THEN
                nextStepChangeHold := TRUE;
            ELSE
                nextStepChangeHold := FALSE;
            ENDIF
            WaitSyncTask syncSolveCubeStart,task_list;
            IF currentStep = "" THEN
                RETURN;
            ELSEIF currentStep = "L" THEN
                work;
                path_GoL;
            ELSEIF currentStep = "Li" THEN
                work;
                path_GoLi;
            ELSEIF currentStep = "R" OR currentStep = "Ri" THEN
                hold1;
            ELSEIF currentStep = "F" THEN
                work;
                path_GoF;
            ELSEIF currentStep = "Fi" THEN
                hold2;
            ELSEIF currentStep = "B" THEN
                hold2;
            ELSEIF currentStep = "Bi" THEN
                work;
                path_GoBi;
            ELSEIF currentStep = "U" THEN
                work;
                path_GoU;
            ELSEIF currentStep = "Ui" THEN
                hold1;
            ELSEIF currentStep = "D" THEN
                hold1;
            ELSEIF currentStep = "Di" THEN
                work;
                path_GoDi;
            ENDIF
            WaitSyncTask syncSolveCubeStop,task_list;
         ENDFOR
    ENDPROC
    
    PROC hold1()
        IF wasWork THEN 
            WaitSyncTask syncStartHold, task_list;
            MoveJ rT_Haltepunkt_L_Offset, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL rT_Haltepunkt_L, v100, fine, Servo_RC1\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;
            smartGripperOpen;
            MoveL rT_Haltepunkt_L, v100, fine, Servo_RC2\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncStopHold,task_list;
        ELSEIF isHold1 THEN
            WaitSyncTask syncStartHold, task_list;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;
            WaitSyncTask syncStopHold,task_list;
        ELSEIF isHold2 THEN
            WaitSyncTask syncStartHold, task_list;
            smartGripperOpen;
             MoveL RelTool(rT_Haltepunkt_L,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;
            smartGripperOpen;
            MoveJ rT_Haltepunkt_L_Offset, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL rT_Haltepunkt_L, v100, fine, Servo_RC2\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncStopHold,task_list;
        ENDIF
        wasWork := FALSE;
        isHold2 := FALSE; 
        isHold1 := TRUE;            
    ENDPROC 
    
    PROC hold2()
        IF wasWork THEN 
            WaitSyncTask syncStartHold, task_list;
            MoveJ RelTool(rT_Haltepunkt_L_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL RelTool(rT_Haltepunkt_L,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;
            smartGripperOpen;
            MoveL RelTool(rT_Haltepunkt_L,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v100,fine,Servo_RC2\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncStopHold,task_list;
        ELSEIF isHold1 THEN
            WaitSyncTask syncStartHold, task_list;
            smartGripperOpen;
            MoveL rT_Haltepunkt_L, v100, fine, Servo_RC1\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;   
            smartGripperOpen;
            MoveL rT_Haltepunkt_L_Offset, v100, z10, Servo_RC1\WObj:=wobj_Workingpoint;
            MoveJ RelTool(rT_Haltepunkt_L_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL RelTool(rT_Haltepunkt_L,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v100,fine,Servo_RC2\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncStopHold,task_list;
        ELSEIF isHold2 THEN
            WaitSyncTask syncStartHold, task_list;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;   
            WaitSyncTask syncStopHold,task_list;
        ENDIF
        wasWork := FALSE;
        isHold2 := TRUE; 
        isHold1 := FALSE;            
    ENDPROC
    
    PROC work()
        IF wasWork AND (NOT nextStepChangeHold) THEN
            WaitSyncTask syncStartHold, task_list;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;   
            WaitSyncTask syncStopHold,task_list;
        ELSEIF wasWork AND nextStepChangeHold THEN
            MoveL rT_Haltepunkt_L, v100, fine, Servo_RC1\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncStartHold, task_list;
            WaitSyncTask syncSwitchStart, task_list;
            WaitSyncTask syncSwitchStop, task_list;   
            WaitSyncTask syncStopHold,task_list;
            smartGripperOpen;
            MoveL rT_Haltepunkt_L_Offset, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;     
        ELSEIF isHold1 THEN 
            WaitSyncTask syncStartHold, task_list;
            WaitSyncTask syncSwitchStart, task_list;
            smartGripperOpen;
            MoveL rT_Haltepunkt_L, v100, fine, Servo_RC1\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncSwitchStop, task_list;            
            WaitSyncTask syncStopHold,task_list;
            smartGripperOpen;
            MoveL rT_Haltepunkt_L_Offset, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;    
        ELSEIF isHold2 THEN
            WaitSyncTask syncStartHold, task_list;
            WaitSyncTask syncSwitchStart, task_list;
            smartGripperOpen;
            MoveL RelTool(rT_Haltepunkt_L,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
            smartGripperGrab;
            WaitSyncTask syncSwitchStop, task_list;            
            WaitSyncTask syncStopHold,task_list;
            smartGripperOpen;
            MoveL RelTool(rT_Haltepunkt_L_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
            MoveJ rT_Haltepunkt_L_Offset, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;
        ENDIF
        wasWork := TRUE;
        isHold2 := FALSE; 
        isHold1 := FALSE;  
    ENDPROC
    
    
    PROC path_GoL()
        MoveL rT_L,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveL RelTool(rT_L,0,0,0\Rx:=0\Ry:=0\Rz:=90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL RelTool(rT_L_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_L_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPoint;
    ENDPROC
    
     PROC path_GoLi()
        MoveL rT_L,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveL RelTool(rT_L,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL RelTool(rT_L_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_L_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPoint;
    ENDPROC
    
    PROC path_GoU()
        MoveJ rT_U_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_U,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_U,sin(45)*Radius,cos(45)*Radius - Radius,0),rT_Ue,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_Ue_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPXY(rT_U_Offset);
    ENDPROC
    
    PROC path_GoDi()
        MoveJ rT_D_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_D,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_D,sin(45)*Radius,cos(45)*Radius - Radius,0),rT_De,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_De_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPXY(rT_D_Offset);
    ENDPROC

    PROC path_GoF()
        MoveJ rT_F_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_F,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_F,0,cos(45)*Radius - Radius,sin(45)*Radius),rT_Fe,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_Fe_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPYZ(rT_F_Offset);
    ENDPROC
    
    PROC path_GoBi()
        MoveJ rT_Bi_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Bi,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_Bi,0,cos(45)*Radius - Radius,sin(45)*Radius),rT_Bie,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_Bie_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPYZ(rT_Bi_Offset);
    ENDPROC
    
    PROC path_GoWorkPoint()
        MoveJ rT_Haltepunkt_L_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
    ENDPROC
    
    PROC path_GoWorkPointCPXY(robtarget startPoint)
        MoveC Offs(startPoint,0,cos(45)*RadiusOffset - RadiusOffset,sin(45)*RadiusOffset),startPoint,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoAfterCP;
    ENDPROC
    
    PROC path_GoWorkPointCPYZ(robtarget startPoint)
        MoveC Offs(startPoint,0,cos(45)*RadiusOffset - RadiusOffset,sin(45)*RadiusOffset),startPoint,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoAfterCP;
    ENDPROC
    
    PROC path_GoAfterCP()
        IF nextStep = "Bi" THEN
            MoveJ RelTool(rT_Haltepunkt_L_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        ELSEIF nextStep = "F" THEN
            MoveJ RelTool(rT_Haltepunkt_L_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        ELSE
            path_GoWorkPoint;
        ENDIF
    ENDPROC
    
    PROC path_GoHaltepunkt_L()
        MoveL rT_Haltepunkt_L,v1000,fine,Servo_RC2\WObj:=wobj_Workingpoint;
    ENDPROC
    PROC path_GoHaltepunkt_L_start()
        MoveJ rT_Haltepunkt_L_Offset,v1000,z100,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Haltepunkt_L,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
    ENDPROC


    
    
ENDMODULE