MODULE Module1
    CONST jointtarget jT_Home_Offset:=[[-69.453905556,-140.880957142,51.323974367,-4.612532188,-7.319531243,-199.7280502],[51.931635807,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget jT_Home:=[[0,-130,30,0,40,0],[135,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    Const num maxSolveLength := 500;
    Pers string solvedArray{maxSolveLength};
    ! Sync tasks
    PERS tasks task_list{2} := [ ["T_ROB_L"], ["T_ROB_R"] ];
    VAR syncident syncCommunication;
    VAR syncident syncSolveCubeStart;
    VAR syncident syncSolveCubeStop;
    VAR syncident syncStartHold;
    VAR syncident syncSwitchStart;
    VAR syncident syncSwitchStop;
    VAR syncident syncStopHold;
    
    VAR syncident syncHoldPointStart;
    VAR syncident syncHoldPoint1;
    VAR syncident syncHoldPoint2;
    VAR syncident syncHoldPointStop;
    VAR syncident sync2HoldPointStart;
    VAR syncident sync2HoldPoint1;
    VAR syncident sync2HoldPoint2;
    VAR syncident sync2HoldPointStop;
    VAR syncident syncCameraHandlingStarted;
    VAR syncident syncCameraHandling{2};
    VAR syncident syncCameraHandlingFinished;
    PERS wobjdata wobj_Workingpoint:=[FALSE,TRUE,"",[[325,0,130],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
    CONST robtarget rT_Haltepunkt_Top:=[[0,0,30.5],[0,0,1,0],[-1,2,0,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Haltepunkt_Top_Offset:=[[0,0,60.5],[0,0,1,0],[-1,2,0,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
!***********************************************************
    !
    ! Modul:  Module1
    !
    ! Beschreibung:
    !   <Fuegen Sie hier eine Beschreibung ein>
    !
    ! Autor: Wiju
    !
    ! Version: 1.047
    !
    !***********************************************************
    
    
    !***********************************************************
    !
    ! Prozedur main
    !
    !   Dies ist die Startroutine 'main', die den Ablauf der Task steuert.
    !
    !***********************************************************
    PROC main()
        ! Init the Smart gripper
!        g_Calibrate;
!        g_Init\maxSpd:=20\holdForce := 10;
        !Eigenen Programmcode einf�gen
        path_goOffsetPoint;
        Test_startCommunication;
!        RCS_startCommunication;
        WaitSyncTask syncCommunication,task_list;
!        Start_CameraHanding;
!        Communication_GetSolvedArray;
        solveCube;
        path_goOffsetPoint;
!        Communication_CloseConnection;
        
    ENDPROC
    PROC path_goHome()
        MoveAbsJ jT_Home_Offset,v1000,z100,tool0\WObj:=wobj0;
        MoveAbsJ jT_Home,v1000,z100,tool0\WObj:=wobj0;
    ENDPROC

    PROC path_goOffsetPoint()
        MoveAbsJ jT_Home_Offset,v1000,z100,tool0\WObj:=wobj0;
    ENDPROC 
    
    PROC smartGripperGrab()
!        g_GripIn \holdForce := 10;
        WaitTime 0.05;
    ENDPROC
    
    PROC smartGripperOpen()
!        g_JogOut;
        WaitTime 0.05;
    ENDPROC 

    
ENDMODULE