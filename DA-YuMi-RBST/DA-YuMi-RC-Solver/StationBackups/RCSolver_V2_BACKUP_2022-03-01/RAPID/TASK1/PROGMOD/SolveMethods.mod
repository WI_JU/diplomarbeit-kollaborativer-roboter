MODULE SolveMethods
    CONST robtarget rT_Ui:=[[0,-30.5,19],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.964433017,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Ui_Offset:=[[0,-60.5,19],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.964433017,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Di_Offset:=[[0,-60.5,-19],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.964433017,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Di:=[[0,-30.5,-19],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.964433017,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Haltepunkt_R:=[[0,-30.5,0],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.964433017,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Haltepunkt_R_Offset:=[[0,-60.5,0],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.960017596,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Ue:=[[30.5,0,19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Ue_Offset:=[[60.5,0,19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_De:=[[30.5,0,-19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_De_Offset:=[[60.5,0,-19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_R_Offset:=[[0,-60.5,0],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.960017596,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_R:=[[0,-30.5,0],[0,0,0.707106781,0.707106781],[1,-1,0,4],[-101.964433017,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Uie:=[[30.5,0,19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Uie_Offset:=[[60.5,0,19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Die:=[[30.5,0,-19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Die_Offset:=[[60.5,0,-19],[0.5,0.5,-0.5,-0.5],[1,-1,0,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    CONST robtarget rT_Fi:=[[-19,-30.5,0],[0.5,-0.5,-0.5,-0.5],[1,-1,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Fi_Offset:=[[-19,-60.5,0],[0.5,-0.5,-0.5,-0.5],[1,-1,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Fie:=[[-19,0,30.5],[0,0.707106781,0.707106781,0],[1,-3,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Fie_Offset:=[[-19,0,60.5],[0,0.707106781,0.707106781,0],[1,-3,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    CONST robtarget rT_B:=[[19,-30.5,0],[0.5,-0.5,-0.5,-0.5],[1,-1,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_B_Offset:=[[19,-60.5,0],[0.5,-0.5,-0.5,-0.5],[1,-1,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Be:=[[19,0,30.5],[0,0.707106781,0.707106781,0],[1,-3,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Be_Offset:=[[19,0,60.5],[0,0.707106781,0.707106781,0],[1,-3,1,4],[-101.964430611,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    ! Save if was in HoldPosition
    VAR bool isHoldPosition := FALSE;
    VAR bool wasHoldPosition := FALSE;
    ! Save next Step
    VAR string nextStep := "";
    ! Radius from Center to RC1 Point 
    var num Radius := 30.5;
    var num RadiusOffset := 60.5;
    
    PROC smartGripperGrab()
        g_GripIn \holdForce := 10;
    ENDPROC
    
    PROC smartGripperOpen()
        g_JogOut;
    ENDPROC 
    
    PROC solveCube()
        IF solvedArray{1} = "R" or solvedArray{1} = "Ri" or solvedArray{1} = "Ui" or solvedArray{1} = "Di" or solvedArray{1} = "Fi" or solvedArray{1} = "B" THEN
            path_GoHaltepunkt_R_start_Work;
        ELSE
            path_GoHaltepunkt_R_start_Hold;
            wasHoldPosition := TRUE;
        ENDIF
        WaitSyncTask syncSolveCube,task_list;
        FOR i FROM 1 TO 500 DO
            IF i < maxSolveLength THEN
                nextStep := solvedArray{i+1};
            ELSE
                nextStep := "";
            ENDIF
            ! Go to HoldingPoints
            IF (solvedArray{i} = "R" or solvedArray{i} = "Ri" or solvedArray{i} = "Ui" or solvedArray{i} = "Di" or solvedArray{i} = "Fi" or solvedArray{i} = "B") and wasHoldPosition THEN
                path_GoHoldingPoint_R_After;
                wasHoldPosition := FALSE;
            ENDIF
            IF solvedArray{i} = "F" OR solvedArray{i} = "Bi" THEN
                path_GoHoldingPoint_R_2;
            ENDIF
            IF solvedArray{i} = "" THEN
                RETURN;
            ELSEIF solvedArray{i} = "R" THEN
                path_GoR;
            ELSEIF solvedArray{i} = "Ri" THEN
                path_GoRi;
            ELSEIF solvedArray{i} = "Ui" THEN
                path_GoUi;
            ELSEIF solvedArray{i} = "Di" THEN
                path_GoDi;
            ELSEIF solvedArray{i} = "Fi" THEN
                path_GoHoldingPoint_R_After;
                path_GoFi;
            ELSEIF solvedArray{i} = "B" THEN
                path_GoHoldingPoint_R_After;
                path_GoB;
            ELSE
                IF NOT wasHoldPosition THEN
                    path_GoHoldingPoint_R;
                    wasHoldPosition := TRUE;
                ENDIF
            ENDIF            
            WaitSyncTask syncSolveCube,task_list;
        ENDFOR
    ENDPROC
   
    PROC path_GoHoldingPoint_R()
        WaitSyncTask syncHoldPointStart,task_list;
        MoveL rT_Haltepunkt_R,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        WaitSyncTask syncHoldPoint1,task_list;
        WaitSyncTask syncHoldPoint2,task_list;
        smartGripperOpen;
        MoveL rT_Haltepunkt_R,v1000,fine,Servo_RC2\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        WaitSyncTask syncHoldPointStop,task_list;
    ENDPROC
    
    PROC path_GoHoldingPoint_R_2()
        WaitSyncTask syncHoldPointStart,task_list;
        MoveL rT_Haltepunkt_R,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        WaitSyncTask syncHoldPoint1,task_list;
        WaitSyncTask syncHoldPoint2,task_list;
        smartGripperOpen;
        MoveL rT_Haltepunkt_R_Offset, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL RelTool(rT_Haltepunkt_R,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v1000,fine,Servo_RC2\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        WaitSyncTask syncHoldPointStop,task_list;
    ENDPROC
    
    PROC path_GoHoldingPoint_R_After()
        WaitSyncTask syncHoldPointStart,task_list;
        WaitSyncTask syncHoldPoint1,task_list;
        smartGripperOpen;
        MoveL rT_Haltepunkt_R,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        WaitSyncTask syncHoldPoint2,task_list;
        WaitSyncTask syncHoldPointStop,task_list;
    ENDPROC
        
    PROC path_GoHaltepunkt_R_start_Work()
        smartGripperOpen;
        MoveJ rT_Haltepunkt_R_Offset,v1000,z100,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Haltepunkt_R,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        isHoldPosition := TRUE;
    ENDPROC
    
    PROC path_GoHaltepunkt_R_start_Hold()
        MoveJ rT_Haltepunkt_R_Offset,v1000,z100,Servo_RC2\WObj:=wobj_Workingpoint;
        MoveL rT_Haltepunkt_R,v1000,fine,Servo_RC2\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        isHoldPosition := TRUE;
    ENDPROC

    
    PROC path_GoR()
        IF isHoldPosition THEN
        ELSE
            MoveJ rT_R_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        ENDIF
        MoveL rT_R,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveL RelTool(rT_R,0,0,0\Rx:=0\Ry:=0\Rz:=90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL RelTool(rT_R_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Haltepunkt_R_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPoint;
    ENDPROC
    
     PROC path_GoRi()
        IF isHoldPosition THEN
        ELSE
            MoveJ rT_R_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        ENDIF
        MoveL rT_R,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveL RelTool(rT_R,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL RelTool(rT_R_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Haltepunkt_R_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPoint;
    ENDPROC
    
    PROC path_GoUi()
        goToWorkPositionIfWasInHold;
        MoveJ rT_Ui_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Ui,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_Ui,sin(45)*Radius,Radius - cos(45)*Radius,0),rT_Uie,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_Uie_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPXY(rT_Ui_Offset);
    ENDPROC
    
    PROC path_GoDi()
        goToWorkPositionIfWasInHold;
        MoveJ rT_Di_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Di,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_Di,sin(45)*Radius,Radius - cos(45)*Radius,0),rT_Die,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_Die_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPXY(rT_Di_Offset);
    ENDPROC
    
    PROC path_GoFi()
        goToWorkPositionIfWasInHold;
        MoveJ rT_Fi_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Fi,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_Fi,0,Radius - cos(45)*Radius,sin(45)*Radius),rT_Fie,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_Fie_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPYZ(rT_Fi_Offset);
    ENDPROC
    
    PROC path_GoB()
        goToWorkPositionIfWasInHold;
        MoveJ rT_B_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_B,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_B,0,Radius - cos(45)*Radius,sin(45)*Radius),rT_Be,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_Be_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPYZ(rT_B_Offset);
    ENDPROC
    
    PROC path_GoWorkPoint()
        MoveJ rT_Haltepunkt_R_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
    ENDPROC
    
    PROC path_GoWorkPointCPXY(robtarget startPoint)
        MoveC Offs(startPoint,sin(45)*RadiusOffset,RadiusOffset - cos(45)*RadiusOffset,0),startPoint,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoAfterCP;
    ENDPROC
    
    PROC path_GoWorkPointCPYZ(robtarget startPoint)
        MoveC Offs(startPoint,0,RadiusOffset - cos(45)*RadiusOffset,sin(45)*RadiusOffset),startPoint,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoAfterCP;
    ENDPROC
    
    PROC path_GoAfterCP()
        IF nextStep = "B" THEN
            MoveJ RelTool(rT_Haltepunkt_R_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=90),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        ELSEIF nextStep = "Fi" THEN
            MoveJ RelTool(rT_Haltepunkt_R_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=90),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        ELSE
            path_GoWorkPoint;
        ENDIF
    ENDPROC
    
    PROC goToWorkPositionIfWasInHold()
        IF isHoldPosition THEN
            path_GoWorkPoint;
            isHoldPosition:= FALSE;
        ENDIF
    ENDPROC
    PROC path_GoHaltepunkt_R()
        MoveL rT_Haltepunkt_R,v1000,fine,Servo_RC2\WObj:=wobj_Workingpoint;
    ENDPROC
    PROC path_GoHaltepunkt_R_start()
        MoveJ rT_Haltepunkt_R_Offset,v1000,z100,Servo_RC2\WObj:=wobj_Workingpoint;
        MoveL rT_Haltepunkt_R,v1000,fine,Servo_RC2\WObj:=wobj_Workingpoint;
    ENDPROC
    
ENDMODULE 