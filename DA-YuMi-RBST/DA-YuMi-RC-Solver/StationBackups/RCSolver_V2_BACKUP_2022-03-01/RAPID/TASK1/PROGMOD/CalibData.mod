MODULE CalibData
    PERS tooldata Servo:=[TRUE,[[0,0,131.5],[1,0,0,0]],[0.28,[7.1,11.9,47.3],[1,0,0,0],0,0,0]];
    PERS tooldata VacuumOne:=[TRUE,[[63,18.5,37.5],[0.707106781,0,0.707106781,0]],[0.28,[7.1,11.9,47.3],[1,0,0,0],0,0,0]];
    PERS tooldata VacuumTwo:=[TRUE,[[-63.5,18.5,37.5],[0,-0.707106781,0,0.707106781]],[0.28,[7.1,11.9,47.3],[1,0,0,0],0,0,0]];
    PERS tooldata Servo_RC2:=[TRUE,[[0,0,103.6],[1,0,0,0]],[0.28,[7.1,11.9,47.3],[1,0,0,0],0,0,0]];
    PERS tooldata Servo_RC1:=[TRUE,[[0,0,112.9],[1,0,0,0]],[0.28,[7.1,11.9,47.3],[1,0,0,0],0,0,0]];
    PERS tooldata Servo_RC1_4:=[TRUE,[[0,0,112.9],[1,0,0,0]],[0.262,[7.8,11.9,50.7],[1,0,0,0],0.00022,0.00024,0.00009]];
ENDMODULE