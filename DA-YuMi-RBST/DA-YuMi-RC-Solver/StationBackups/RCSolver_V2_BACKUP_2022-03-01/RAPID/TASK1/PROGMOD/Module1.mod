MODULE Module1
    ! Workobjekts
    PERS wobjdata wobj_RC_Ablage:=[FALSE,TRUE,"",[[250,-350,30],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
    PERS wobjdata wobj_Workingpoint:=[FALSE,TRUE,"",[[325,0,130],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
    ! JointTargets and RobotTargets
    CONST jointtarget jT_Home_Offset:=[[49.526920861,-142.177777838,56.646351466,73.804961454,-4.532956215,-215.111575985],[-54.16943459,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget jT_Home:=[[-0.605182561,-126.401871346,2.161069932,2.838240713,70.816869805,-2.176210114],[-137.405114854,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_pickUp_Offset:=[[28.48,28.5,95],[0,1,0,0],[1,0,0,5],[-101.964433017,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_pickUp:=[[28.48,28.5,57],[0,1,0,0],[1,0,0,5],[-101.964433017,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget Target_10:=[[5.000077778,-60.500206974,-0.010081976],[-0.000000184,0.000000127,0.707106886,0.707106676],[1,-3,2,5],[-102.020008415,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_position1:=[[50.875801087,-137.420271127,92.750243391],[0.039241605,0.040041462,0.705694947,0.706294145],[1,-3,2,5],[-102.020007487,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget JointTarget_1:=[[83.40074886,-114.711128318,15.313871515,-239.178061714,-59.122226739,156.634201066],[-77.119925466,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget JointTarget_3:=[[80.830077393,-113.37709675,13.704649065,-235.675540708,-62.804613947,154.473429741],[-75.533880408,9E+09,9E+09,9E+09,9E+09,9E+09]];

    Const num maxSolveLength := 500;
    Pers string solvedArray{maxSolveLength};
    ! Sync tasks
    PERS tasks task_list{2} := [ ["T_ROB_L"], ["T_ROB_R"] ];
    VAR syncident syncCommunication;
    VAR syncident syncSolveCube;
    VAR syncident syncHoldPointStart;
    VAR syncident syncHoldPoint1;
    VAR syncident syncHoldPoint2;
    VAR syncident syncHoldPointStop;
    VAR syncident syncCameraHandlingFinished;
    VAR syncident syncCameraHandling{2};
    VAR syncident syncCameraHandlingStarted;
    
    PERS wobjdata wobj_Workingpoint_4:=[FALSE,TRUE,"",[[325,0,110],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
   
!***********************************************************
    !
    ! Modul:  Module1
    !
    ! Beschreibung:
    !   <Fuegen Sie hier eine Beschreibung ein>
    !
    ! Autor: Wiju
    !
    ! Version: 1.0
    !
    !***********************************************************
    
    
    !***********************************************************
    !
    ! Prozedur main
    !
    !   Dies ist die Startroutine 'main', die den Ablauf der Task steuert.
    !
    !***********************************************************
    PROC main()
        !Init the smart Gripper
!        g_Init\maxSpd:=20\holdForce := 10;
        !Eigenen Programmcode einf�gen
        !path_pickUpRC;
        !path_goHome;
        path_goOffsetPoint;
        WaitSyncTask syncCommunication,task_list;
!        Start_CameraHandling;
        solveCube;
        path_goOffsetPoint;
    ENDPROC
    
    
    PROC path_goOffsetPoint()
        MoveAbsJ jT_Home_Offset,v1000,z100,tool0\WObj:=wobj0;
    ENDPROC
    
    PROC path_goHome()
        MoveAbsJ jT_Home_Offset,v1000,z100,tool0\WObj:=wobj0;
        MoveAbsJ jT_Home,v1000,z100,tool0\WObj:=wobj0;
        
    ENDPROC
    
    PROC path_pickUpRC()
        MoveAbsJ jT_Home_Offset,v1000,z10,Servo_RC2\WObj:=wobj0;
        MoveJ rT_pickUp_Offset,v1000,fine,Servo_RC1\WObj:=wobj_RC_Ablage;
        MoveL rT_pickUp,v1000,fine,Servo_RC1\WObj:=wobj_RC_Ablage;
        MoveJ rT_pickUp_Offset,v1000,fine,Servo_RC1\WObj:=wobj_RC_Ablage;
    ENDPROC

    

ENDMODULE