MODULE CameraHandling
    
    PROC start_CameraHandling()
        MoveJ rT_Haltepunkt_R_Offset, v1000, fine,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Haltepunkt_R, v1000, fine,Servo_RC1\WObj:=wobj_Workingpoint;
        FOR i FROM 1 TO 6 Step 2 DO
            WaitSyncTask syncCameraHandlingStarted,task_list;
            openGripper;
            closeGripper;
            WaitSyncTask syncCameraHandlingFinished,task_list;
        ENDFOR
        MoveJ rT_Haltepunkt_R_Offset, v1000, fine,Servo_RC1\WObj:=wobj_Workingpoint;
        
    ENDPROC
    
    PROC openGripper()
        WaitSyncTask syncCameraHandling{1},task_list;
        WaitTime 0.1;
        ! Open Smart Gripper
    ENDPROC
    
    PROC closeGripper()
        ! Grab Smart Gripper
        WaitTime 0.1;
        WaitSyncTask syncCameraHandling{2},task_list;
    ENDPROC
    
    
    
    
    
    
ENDMODULE