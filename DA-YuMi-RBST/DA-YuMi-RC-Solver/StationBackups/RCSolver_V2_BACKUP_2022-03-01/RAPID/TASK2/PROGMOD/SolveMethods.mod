MODULE SolveMethods
    CONST robtarget rT_D:=[[0,30.5,-19],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_D_Offset:=[[0,60.5,-19],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_L:=[[0,30.5,0],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_L_Offset:=[[0,60.5,0],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Haltepunkt_L:=[[0,30.5,0],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Haltepunkt_L_Offset:=[[0,60.5,0],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_U:=[[0,30.5,19],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_U_Offset:=[[0,60.5,19],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_De:=[[30.5,0,-19],[0.5,-0.5,-0.5,0.5],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_De_Offset:=[[60.5,0,-19],[0.5,-0.5,-0.5,0.5],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Ue:=[[30.5,0,19],[0.5,-0.5,-0.5,0.5],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Ue_Offset:=[[60.5,0,19],[0.5,-0.5,-0.5,0.5],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    CONST robtarget rT_F:=[[-19,30.5,0],[0.5,0.5,-0.5,0.5],[-1,1,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_F_Offset:=[[-19,60.5,0],[0.5,0.5,-0.5,0.5],[-1,1,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Fe:=[[-19,0,30.5],[0,-0.707106781,0.707106781,0],[-1,2,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Fe_Offset:=[[-19,0,60.5],[0,-0.707106781,0.707106781,0],[-1,2,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Bi:=[[19,30.5,0],[0.5,0.5,-0.5,0.5],[-1,1,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Bi_Offset:=[[19,60.5,0],[0.5,0.5,-0.5,0.5],[-1,1,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Bie:=[[19,0,30.5],[0,-0.707106781,0.707106781,0],[-1,2,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Bie_Offset:=[[19,0,60.5],[0,-0.707106781,0.707106781,0],[-1,2,-1,4],[101.964440923,9E+09,9E+09,9E+09,9E+09,9E+09]];
    
    
    
    ! Save if was in HoldPosition
    VAR bool isHoldPosition := FALSE;
    VAR bool wasHoldPosition := FALSE;
    ! Save next Step
    VAR string nextStep := "";
    ! Radius from Center to RC1 Point 
    var num Radius := 30.5;
    var num RadiusOffset := 60.5;
    CONST robtarget rT_Haltepunkt_L_Offset_P:=[[0,60.5,0],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget rT_Haltepunkt_L_P:=[[0,30.5,0],[0,0,-0.707106781,0.707106781],[-1,1,0,4],[101.964426653,9E+09,9E+09,9E+09,9E+09,9E+09]];
        
    PROC smartGripperGrab()
        g_GripIn \holdForce := 10;
    ENDPROC
    
    PROC smartGripperOpen()
        g_JogOut;
    ENDPROC 
    
    PROC solveCube()
        IF solvedArray{1} = "L" or solvedArray{1} = "Li" or solvedArray{1} = "U" or solvedArray{1} = "D" or solvedArray{1} = "F" or solvedArray{1} = "Bi" THEN
            path_GoHaltepunkt_L_start_Work;
        ELSE
            path_GoHaltepunkt_L_start_Hold;
            wasHoldPosition := TRUE;
        ENDIF   
        WaitSyncTask syncSolveCube,task_list;
        FOR i FROM 1 TO maxSolveLength DO
            ! Get the next Step
            IF i < maxSolveLength THEN
                nextStep := solvedArray{i+1};
            ELSE
                nextStep := "";
            ENDIF
            ! Go to HoldingPoints
            IF (solvedArray{i} = "L" or solvedArray{i} = "Li" or solvedArray{i} = "U" or solvedArray{i} = "D" or solvedArray{i} = "F" or solvedArray{i} = "Bi") and wasHoldPosition THEN
                path_GoHoldingPoint_L_After;
                wasHoldPosition := FALSE;
            ELSEIF solvedArray{i} = "Fi" OR solvedArray{i} = "B" THEN
                path_GoHoldingPoint_L_2;
            ENDIF
            
            IF solvedArray{i} = "" THEN
                RETURN;
            ELSEIF solvedArray{i} = "L" THEN
                path_GoL;
            ELSEIF solvedArray{i} = "Li" THEN
                path_GoLi;
            ELSEIF solvedArray{i} = "U" THEN
                path_GoU;
            ELSEIF solvedArray{i} = "D" THEN
                path_GoD;
            ELSEIF solvedArray{i} = "F" THEN
                path_GoHoldingPoint_L_After;
                path_GoF;
            ELSEIF solvedArray{i} = "Bi" THEN
                path_GoHoldingPoint_L_After;
                path_GoBi;
            ELSE
                IF NOT wasHoldPosition THEN
                    path_GoHoldingPoint_L;
                    wasHoldPosition := TRUE;
                ENDIF
            ENDIF
            WaitSyncTask syncSolveCube,task_list;
        ENDFOR
    ENDPROC
    
    PROC path_GoHoldingPoint_L()
        WaitSyncTask syncHoldPointStart,task_list;
        MoveL rT_Haltepunkt_L,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        WaitSyncTask syncHoldPoint1,task_list;
        WaitSyncTask syncHoldPoint2,task_list;
        smartGripperOpen;
        MoveL rT_Haltepunkt_L,v1000,fine,Servo_RC2\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        WaitSyncTask syncHoldPointStop,task_list;
    ENDPROC
    
    PROC path_GoHoldingPoint_L_2()
        WaitSyncTask syncHoldPointStart,task_list;
        MoveL rT_Haltepunkt_L,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        WaitSyncTask syncHoldPoint1,task_list;
        WaitSyncTask syncHoldPoint2,task_list;
        smartGripperOpen;
        MoveL rT_Haltepunkt_L_Offset, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL RelTool(rT_Haltepunkt_L,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v1000,fine,Servo_RC2\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        WaitSyncTask syncHoldPointStop,task_list;
    ENDPROC
    
    PROC path_GoHoldingPoint_L_After()
        WaitSyncTask syncHoldPointStart,task_list;
        WaitSyncTask syncHoldPoint1,task_list;
        smartGripperOpen;
        MoveL rT_Haltepunkt_L,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        WaitSyncTask syncHoldPoint2,task_list;
        WaitSyncTask syncHoldPointStop,task_list;
    ENDPROC
    
    PROC path_GoHaltepunkt_L_start_Work()
        smartGripperOpen;
        MoveJ rT_Haltepunkt_L_Offset,v1000,z100,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Haltepunkt_L,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        isHoldPosition := TRUE;
    ENDPROC
    
    PROC path_GoHaltepunkt_L_start_Hold()
        MoveJ rT_Haltepunkt_L_Offset,v1000,z100,Servo_RC2\WObj:=wobj_Workingpoint;
        MoveL rT_Haltepunkt_L,v1000,fine,Servo_RC2\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        isHoldPosition := TRUE;
    ENDPROC
    
    PROC path_GoL()
        IF NOT isHoldPosition THEN
            MoveJ rT_L_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        ENDIF
        MoveL rT_L,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveL RelTool(rT_L,0,0,0\Rx:=0\Ry:=0\Rz:=90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL RelTool(rT_L_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_L_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPoint;
    ENDPROC
    
     PROC path_GoLi()
        IF NOT isHoldPosition THEN
            MoveJ rT_L_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        ENDIF
        MoveL rT_L,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveL RelTool(rT_L,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL RelTool(rT_L_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v100,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_L_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPoint;
    ENDPROC
    
    PROC path_GoU()
        goToWorkPositionIfWasInHold;
        MoveJ rT_U_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_U,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_U,sin(45)*Radius,cos(45)*Radius - Radius,0),rT_Ue,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_Ue_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPXY(rT_U_Offset);
    ENDPROC
    
    PROC path_GoD()
        goToWorkPositionIfWasInHold;
        MoveJ rT_D_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_D,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_D,sin(45)*Radius,cos(45)*Radius - Radius,0),rT_De,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_De_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPXY(rT_D_Offset);
    ENDPROC

    PROC path_GoF()
        
        goToWorkPositionIfWasInHold;
        MoveJ rT_F_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_F,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_F,0,cos(45)*Radius - Radius,sin(45)*Radius),rT_Fe,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_Fe_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPYZ(rT_F_Offset);
    ENDPROC
    
    PROC path_GoBi()
        goToWorkPositionIfWasInHold;
        MoveJ rT_Bi_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Bi,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperGrab;
        MoveC Offs(rT_Bi,0,cos(45)*Radius - Radius,sin(45)*Radius),rT_Bie,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        smartGripperOpen;
        MoveL rT_Bie_Offset,v1000,z10,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoWorkPointCPYZ(rT_Bi_Offset);
    ENDPROC
    
    PROC path_GoWorkPoint()
        MoveJ rT_Haltepunkt_L_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
    ENDPROC
    
    PROC path_GoWorkPointCPXY(robtarget startPoint)
        MoveC Offs(startPoint,0,cos(45)*RadiusOffset - RadiusOffset,sin(45)*RadiusOffset),startPoint,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoAfterCP;
    ENDPROC
    
    PROC path_GoWorkPointCPYZ(robtarget startPoint)
        MoveC Offs(startPoint,0,cos(45)*RadiusOffset - RadiusOffset,sin(45)*RadiusOffset),startPoint,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        path_GoAfterCP;
    ENDPROC
    
    PROC path_GoAfterCP()
        IF nextStep = "Bi" THEN
            MoveJ RelTool(rT_Haltepunkt_L_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        ELSEIF nextStep = "F" THEN
            MoveJ RelTool(rT_Haltepunkt_L_Offset,0,0,0\Rx:=0\Ry:=0\Rz:=-90),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        ELSE
            path_GoWorkPoint;
        ENDIF
    ENDPROC
    
    PROC goToWorkPositionIfWasInHold()
        IF isHoldPosition THEN
            path_GoWorkPoint;
            isHoldPosition:= FALSE;
        ENDIF
    ENDPROC
    PROC path_GoHaltepunkt_L()
        MoveL rT_Haltepunkt_L,v1000,fine,Servo_RC2\WObj:=wobj_Workingpoint;
    ENDPROC
    PROC path_GoHaltepunkt_L_start()
        MoveJ rT_Haltepunkt_L_Offset,v1000,z100,Servo_RC1\WObj:=wobj_Workingpoint;
        MoveL rT_Haltepunkt_L,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
    ENDPROC


    
    
ENDMODULE