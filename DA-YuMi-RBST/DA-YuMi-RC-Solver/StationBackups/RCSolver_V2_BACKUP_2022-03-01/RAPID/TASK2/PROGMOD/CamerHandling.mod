MODULE CamerHandling
       
    VAR num currentPoint;
    PERS wobjdata wobj_Workingpoint_Pictures:=[FALSE,TRUE,"",[[175,0,130],[1,0,0,0]],[[0,0,0],[1,0,0,0]]];
    
    
    PROC Start_CameraHanding()
        FOR i FROM 1 TO 6 Step 2 DO
            WaitSyncTask syncCameraHandlingStarted,task_list;            
            currentPoint := i;
            moveCameraCase;
            WaitSyncTask syncCameraHandlingFinished,task_list;
        ENDFOR
        Communication_CameraHandling(7);
    ENDPROC
    
    
    PROC moveCameraCase()
        pickRubicscube;
        cameraPosition;
        returnRubicscube;
    ENDPROC
    
    PROC cameraPosition()
        TPWrite NumToStr(currentPoint,0);
        MoveJ rT_Haltepunkt_L_Offset_P,v1000,z100,Servo_RC1\WObj:=wobj_Workingpoint_Pictures;
        MoveL rT_Haltepunkt_L_P,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint_Pictures;
        MoveL RelTool(rT_Haltepunkt_L_P, 0, 0, 0\Rx:=0\Ry:=0\Rz:=45),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint_Pictures;
        Communication_CameraHandling(currentPoint);
        MoveL RelTool(rT_Haltepunkt_L_P, 0, 0, 0\Rx:=0\Ry:=0\Rz:=225),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint_Pictures;
        Communication_CameraHandling(currentPoint + 1);
        MoveL RelTool(rT_Haltepunkt_L_P, 0, 0, 0\Rx:=0\Ry:=0\Rz:=0),v1000,z100,Servo_RC1\WObj:=wobj_Workingpoint_Pictures;
        MoveJ rT_Haltepunkt_L_Offset_P,v1000,z100,Servo_RC2\WObj:=wobj_Workingpoint_Pictures;
    ENDPROC
    
    PROC pickRubicscube()
        IF currentPoint = 1 THEN
            MoveJ rT_Haltepunkt_L_Offset, v1000, z100, Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL rT_Haltepunkt_L, v1000, fine, Servo_RC2\WObj:=wobj_Workingpoint;
            ! Grab Smart Gripper
            WaitSyncTask syncCameraHandling{1},task_list;
            WaitTime 0.1;
            
        ELSEIF currentPoint = 3 THEN
            MoveJ rT_Haltepunkt_L_Offset, v1000, z100, Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL RelTool(rT_Haltepunkt_L_Offset, 0, 0, 0\Rx:=0\Ry:=0\Rz:=90),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL RelTool(rT_Haltepunkt_L, 0, 0, 0\Rx:=0\Ry:=0\Rz:=90), v1000, fine, Servo_RC2\WObj:=wobj_Workingpoint;
            ! Grab Smart Gripper
            WaitTime 0.1;
            WaitSyncTask syncCameraHandling{1},task_list;
        ELSE
            MoveJ rT_Haltepunkt_L_Offset, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;
            TPWrite NumToStr(cos(45)*RadiusOffset - RadiusOffset,2);
            TPWrite NumToStr(sin(45)*RadiusOffset,2);
            MoveC Offs(rT_Haltepunkt_L_Offset,0,cos(45)*RadiusOffset - RadiusOffset,sin(45)*RadiusOffset),rT_Haltepunkt_Top_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL rT_Haltepunkt_Top, v1000, fine, Servo_RC2\WObj:=wobj_Workingpoint;
            ! Grab Smart Gripper
            WaitTime 0.1;
            WaitSyncTask syncCameraHandling{1},task_list;
            MoveL rT_Haltepunkt_Top_Offset, v1000, fine, Servo_RC2\WObj:=wobj_Workingpoint;
            MoveC Offs(rT_Haltepunkt_Top_Offset,0,cos(45)*RadiusOffset,sin(45)*RadiusOffset - RadiusOffset),rT_Haltepunkt_L_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        ENDIF
    ENDPROC
    
    
    PROC returnRubicscube()
        IF currentPoint = 1 THEN
            MoveJ rT_Haltepunkt_L_Offset, v1000, z100, Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL rT_Haltepunkt_L, v1000, fine, Servo_RC1\WObj:=wobj_Workingpoint;
            WaitSyncTask syncCameraHandling{2},task_list;
            WaitTime 0.1;
            ! Open Smart Gripper
        ELSEIF currentPoint = 3 THEN
            MoveJ rT_Haltepunkt_L_Offset, v1000, z100, Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL RelTool(rT_Haltepunkt_L_Offset, 0, 0, 0\Rx:=0\Ry:=0\Rz:=90),v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL RelTool(rT_Haltepunkt_L, 0, 0, 0\Rx:=0\Ry:=0\Rz:=90), v1000, fine, Servo_RC2\WObj:=wobj_Workingpoint;
            WaitSyncTask syncCameraHandling{2},task_list;
            WaitTime 0.1;
            ! Open Smart Gripper
        ELSE
            MoveJ rT_Haltepunkt_L_Offset, v1000, z100, Servo_RC1\WObj:=wobj_Workingpoint;
           MoveC Offs(rT_Haltepunkt_L_Offset,0,cos(45)*RadiusOffset - RadiusOffset,sin(45)*RadiusOffset),rT_Haltepunkt_Top_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
            MoveL rT_Haltepunkt_Top, v1000, fine, Servo_RC2\WObj:=wobj_Workingpoint;
            WaitSyncTask syncCameraHandling{2},task_list;
            WaitTime 0.1;
            ! Open Smart Gripper
            MoveL rT_Haltepunkt_Top_Offset, v1000, fine, Servo_RC2\WObj:=wobj_Workingpoint;
            MoveC Offs(rT_Haltepunkt_Top_Offset,0,cos(45)*RadiusOffset,sin(45)*RadiusOffset - RadiusOffset),rT_Haltepunkt_L_Offset,v1000,fine,Servo_RC1\WObj:=wobj_Workingpoint;
        ENDIF
        
    Endproc
    
ENDMODULE