MODULE RCS_Communication
    !Socket Declatration
    VAR socketdev RCSserverSocket;
    VAR socketdev RCSclientSocket;  
    VAR string virtuellIP := "127.0.0.1";
    CONST string roboterIP := "192.168.125.1";
    CONST num robotPORT := 5516;
    VAR num virtuellPORT := 55000;
    CONST string VALID_RESPOND := "VALID";
    CONST string INVALID_RESPOND := "INVALID";
    CONST string FINISHED_SEQUENCE := "FINISH";
    CONST string handleCases{4} := ["GetSolvedCube", "CloseConnection", "CameraPosition","NextStep"];
    !solved Array (dynamic)
    CONST num maxarraysize := 500;
    VAR num currentarraysize:=0;
        
    PROC RCS_startCommunication()
        VAR string respond;
        TPWrite "Communication started";
    ! Create Communication
        RCSserverCreate;
        
        ERROR
        
        IF ERRNO = ERR_SOCK_TIMEOUT THEN
            RETRY;
        ELSEIF ERRNO = ERR_SOCK_Closed THEN
            RCSserverCreate;
            RETRY;
        ELSE
            stop;
        ENDIF
    ENDPROC
    
    PROC RCSserverCreate()
        SocketCreate RCSserverSocket;
        SocketBind RCSserverSocket, virtuellIP, virtuellPORT;
!        SocketBind RCSserverSocket, roboterIP, robotPORT;
        SocketListen RCSserverSocket;
        SocketAccept RCSserverSocket,RCSclientSocket,\Time:=WAIT_MAX;    
    ENDPROC
    
    PROC Test_startCommunication()
        VAR string steps{12} := ["R","Ri","Ui","Di","L","Li","U","D","F","Bi","Fi","B"];
        !VAR string steps{12} := ["R","","","","","","","","","","",""];
        FOR i FROM 1 TO 12 DO
            solvedArray{i} := steps{i};
        ENDFOR
    ENDPROC 
    
    PROC Communication_CameraHandling(num camPoint)
            VAR string respond;
            RCSsend(handleCases{3}+","+NumToStr(camPoint,0)); 
            waittime 3;
    ENDPROC
    
    PROC Communication_GetSolvedArray()
        !Ready for solve Algrotythm
        TPWrite "SolvedArrayhandling activated";
        RCSgetSolvedArrays;
    Endproc
    
    PROC Communication_CloseConnection()
    !Send Close Communication 
        TPWrite "Close Connection activated";
        RCSsend(handleCases{2});
        
    !Close Communication
        SocketClose RCSclientSocket;
        SocketClose RCSserverSocket;
        
        TPWrite "Communication connection close";
    ENDPROC 

    
    
    !Sends to CLIENT and checkt if respond is truthy 
    Proc RCSsend(string message)
        VAR string respond;
        SocketSend RCSclientSocket,\Str:=message;
        waitTime 0.05;
        SocketReceive RCSclientSocket,\Str:=respond;
        TPWrite respond;
    ENDPROC
    
    PROC RCSgetSolvedArrays()
        
        RCSsend(handleCases{1});
        ! get solved Array
        revieveSteps;        
    ENDPROC
    
    PROC revieveSteps()
        VAR string respond;
        VAR num index;
        VAR bool stopHandler := FALSE;
        clearSolvedArray;
        
        waitTime 0.05;
        WHILE not stopHandler DO
            SocketSend RCSclientSocket,\Str:=handleCases{4}; 
            waitTime 0.05;
            SocketReceive RCSclientSocket,\Str:=respond;
            IF respond = FINISHED_SEQUENCE THEN
                stopHandler := TRUE;
            ELSE
                currentarraysize:=currentarraysize+1;
                solvedArray{currentarraysize}:=respond;
            ENDIF
        ENDWHILE                
    ENDPROC

    PROC clearSolvedArray()
        currentarraysize := 0;
        FOR i FROM 1 TO maxarraysize DO
            solvedArray{i} := "";
        ENDFOR
    ENDPROC
ENDMODULE