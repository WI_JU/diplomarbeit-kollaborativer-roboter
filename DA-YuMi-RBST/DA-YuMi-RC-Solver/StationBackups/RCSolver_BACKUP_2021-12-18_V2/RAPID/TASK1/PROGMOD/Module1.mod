MODULE Module1
        CONST jointtarget jT_Home:=[[0,-130,30,0,40,0],[-135,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget jT_Home_Offset:=[[48.982927029,-137.790815636,40.140601696,-6.270368936,13.15620195,-132.867205471],[-54.363821774,9E+09,9E+09,9E+09,9E+09,9E+09]];
!***********************************************************
    !
    ! Modul:  Module1
    !
    ! Beschreibung:
    !   <Fuegen Sie hier eine Beschreibung ein>
    !
    ! Autor: jusic.far
    !
    ! Version: 1.0
    !
    !***********************************************************
    
    
    !***********************************************************
    !
    ! Prozedur main
    !
    !   Dies ist die Startroutine 'main', die den Ablauf der Task steuert.
    !
    !***********************************************************
    PROC main()
        !Eigenen Programmcode einf�gen
        path_GoToHome;
    ENDPROC
    PROC path_GoToHome()
        MoveAbsJ jT_Home_Offset,v1000,z50,tool0\WObj:=wobj0;
        MoveAbsJ jT_Home,v1000,z100,tool0\WObj:=wobj0;
    ENDPROC
ENDMODULE