MODULE Module1
    ! Koordinaten von Roboter Punkten
    CONST robtarget   pPalR := [[20,20,32],[0,0,1,0],[0,3,0,5],[-135,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget   pKoopR:= [[0,-17,0],[0.183012702,-0.183012702,0.683012702,0.683012702],[1,3,0,5],[-135,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget jpHomeR:=[[0,-130,30,0,40,0],[-135,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget jp1:=    [[35,-105,30,235,-65,20],[-115,9E+09,9E+09,9E+09,9E+09,9E+09]];
    ! Gr��en vom W�rfel und Anzahl
    CONST num nX := 3; ! Anzahl in X-Richtung von W�rfel
    CONST num nY := 2; ! Anzahl in Y-Richtung von W�rfel
    CONST num xW := 55; ! Breite in X-Richtung von W�rfel
    CONST num yW := 60; ! L�nge in Y-Richtung von W�rfel
    CONST num zW := 30.2; ! H�he in Z-Richtung von W�rfel
    CONST num zPal := 60; ! Abstand zur Palette
    CONST num zOffKop := 2; !Offset Abstand Kooppoint (z- Achse)
    !_____________________________________________________
    ! Synchroniesierungs ID Deklarierung
    VAR syncident ID_1;
    VAR syncident ID_2;
    VAR syncident ID_3;
    VAR syncident ID_4;
    VAR syncident ID_5;
    PERS tasks married_Tasks{2}:=[["T_ROB_L"],["T_ROB_R"]];
    CONST robtarget pAbstand_R:=[[50,40,150],[0.707106781,0,0.707106781,0],[0,3,0,5],[-134.999968902,9E+09,9E+09,9E+09,9E+09,9E+09]];
    !_____________________________________________________
   
  ! Teilprogramme:
    ! Aufnehmen der W�rfel mit dem Rechtenarm des YuMis
    PROC AufnehmenR()
        ! F�r alle W�rfel (Anzahl n ... St�ck X/Y ... Koordinaten)
        ! 1) Zum Offsetpunkt W�rfelpunkt fahren
        ! 2) Linear zum W�rfel fahren
        ! 3) 0.3 sec warten Vaccumsauger DO auf True (high) setzen 
        ! 4) Linear zum Offsetpunkt fahren
        ! 5) Zum Offset Austauschpunkt fahren
        ! 6) Linear zum Austauschpunkt bewegen
        ! 7) 0.3 sec und auf Linkenarm warten, danach Vaccumsauger DO auf False (low) setzen
        ! 8) Linear zum Offset Austauschpunkt fahren
        FOR j from nX-1 TO 0 step -1 DO
            FOR i FROM nY-1 TO 0 Step -1 DO
                ! Offsetpunkt W�rfelheben
                MoveJ offs(pPalR,j*xW,i*yW,zPal),v1000,z100,VaccumOne\WObj:=wobjPalR;
                ! Linear zum W�rfelpunkt
                MoveL    offs(pPalR,j*xW,i*yW,0),v1000,fine,VaccumOne\WObj:=wobjPalR;
                ! Vaccumsauger einschalten
                SetDO hand_CmdVacuum1_R,high;
                WaitTime 0.3;
                ! Offset W�rfelheben
                MoveL offs(pPalR,j*xW,i*yW,zPal),v1000,z100,VaccumOne\WObj:=wobjPalR;
                ! Abstandpunkt jp1
                MoveAbsJ jp1,v1000,z200,VaccumOne\WObj:=wobjKoopR;
                ! Offset Austauschpunkt
                MoveJ    offs(pKoopR,0,-20,0),v1000,z10,VaccumOne\WObj:=wobjKoopR;
                ! Austauschpunkt
                MoveL    offs(pKoopR,0,0,zOffKop),v1000,fine,VaccumOne\WObj:=wobjKoopR;
                ! 0.3 sec Verz�gerung und Linkerarm warten danach Vaccumsauger ausschalten
                WaitSyncTask ID_1, married_Tasks;
                WaitTime 0.3;
                SetDO hand_CmdVacuum1_R,low;
                SetDO hand_CmdBlowoff1_R, high;
                WaitTime 0.3;
                SetDo hand_CmdBlowoff1_R, low;
                ! Offset Austauschpunkt
                MoveL    offs(pKoopR,0,-20,0),v1000,z10,VaccumOne\WObj:=wobjKoopR;
                ! Abstandpunkt jp1
                MoveAbsJ jp1,v1000,z200,VaccumOne\WObj:=wobjKoopR;
            ENDFOR
        ENDFOR
    ENDPROC
    ! Weglegen der W�rfel mit dem Rechtenarm des YuMis
    PROC WeglegenR()
        ! F�r alle W�rfel (Anzahl n ... St�ck X/Y ... Koordinaten)
        ! 1) Zum Offset Austauschpunkt fahren
        ! 2) Linear zum Austauschpunkt bewegen
        ! 3) 0.3 sec und auf Linkenarm warten, danach Vaccumsauger DO auf True (high) setzen
        ! 4) Zum Offset Austauschpunkt fahren
        ! 5) Zum Offsetpunkt W�rfelpunkt fahren
        ! 6) Linear zum W�rfel fahren
        ! 7) 0.3 sec warten Vaccumsauger DO auf False (low) setzen 
        ! 8) Zum Offsetpunkt W�rfelpunkt fahren        
        FOR j from 0 TO nX-1 DO
            FOR i FROM 0 TO nY-1 DO
                ! Offset Austauschpunkt
                MoveJ    offs(pKoopR,0,-20,0),v1000,z10,VaccumOne\WObj:=wobjKoopR;
                ! Austauschpunkt
                MoveL    pKoopR,v1000,fine,VaccumOne\WObj:=wobjKoopR;
                ! 0.3 sec Verz�gerung und Linkerarm warten danach Vaccumsauger einschalten
                WaitSyncTask ID_1, married_Tasks;
                SetDO hand_CmdVacuum1_R,high;
                WaitTime 0.3;
                ! Offset Austauschpunkt
                MoveL    offs(pKoopR,0,-20,0),v1000,z10,VaccumOne\WObj:=wobjKoopR;
                ! Abstandpunkt jp1
                MoveAbsJ jp1,v1000,z200,VaccumOne\WObj:=wobjKoopR;
                ! Offset W�rfelpunkt
                MoveJ    offs(pPalR,j*xW,i*yW,zPal),v1000,z100,VaccumOne\WObj:=wobjPalR;
                ! W�rfelpunkt
                MoveL    offs(pPalR,j*xW,i*yW,0),v1000,fine,VaccumOne\WObj:=wobjPalR;
                ! 0.3 sec Verz�gerung danach Vaccumsauger auschalten
                WaitTime 0.3;
                SetDO hand_CmdVacuum1_R,low;
                WaitTime 0.3;
                ! Offset W�rfelpunkt
                MoveL    offs(pPalR,j*xW,i*yW,zPal),v1000,z100,VaccumOne\WObj:=wobjPalR;
                ! Abstandpunkt jp1
                MoveAbsJ jp1,v1000,z200,VaccumOne\WObj:=wobjKoopR;
            ENDFOR
        ENDFOR
    ENDPROC
    ! Bewegen des Rechtenarmes des YuMis in die Homeposition
    PROC gotoHomeR()
        ! Bewegen mit Winkelpositionen in die Homeposition
        MoveAbsJ jpHomeR,v1000,fine,VaccumOne\WObj:=wobjKoopR;
    ENDPROC
    PROC Abstand_R()
        MoveJ pAbstand_R,v1000,z20,tool0\WObj:=wobjPalR;
    ENDPROC
ENDMODULE