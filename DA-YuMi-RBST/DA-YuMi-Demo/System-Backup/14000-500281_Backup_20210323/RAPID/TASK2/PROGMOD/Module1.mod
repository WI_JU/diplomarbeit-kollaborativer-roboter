MODULE Module1

    CONST robtarget   pKoopL:=  [[0,20,0],[0.177045394,0.177045394,0.684583763,-0.684583763],[1,-1,2,0],[-170,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget   pTurmL:=  [[0,0,40],[0,0.70710678,0.707106782,0],[1,2,1,0],[-170,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget jpHomeL:= [[0,-130,30,0,40,0],[135,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget JointTarget_5:=[[126.513790826,38.901571229,27.398052223,255.289357815,80.146798929,166.333479499],[-91.520313821,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget JointTarget_7:=[[-62.069057215,-138.840123938,42.237531242,136.019091887,29.372975809,-135.366467311],[46.595106556,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST num nXY := 6;
    CONST num zW := 36;
    !__________________________________________________
    VAR syncident ID_1;
    VAR syncident ID_2;
    VAR syncident ID_3;
    VAR syncident ID_4;
    VAR syncident ID_5;
    PERS tasks married_Tasks{2}:=[["T_ROB_L"],["T_ROB_R"]];
    CONST jointtarget jPFreistellen:=[[132.324178112,31.257804076,-9.982875571,-33.152491722,79.631228929,118.435985642],[-122.436550382,9E+09,9E+09,9E+09,9E+09,9E+09]];
    !_____________________________________________________

    PROC AufnehmenL()
        FOR i FROM nXY-1 TO 0 step -1 DO
    
            MoveJ offs(pTurmL,0,0,i*zW+10),v1000,z100,VaccumOne\WObj:=wobjTurmL;
            MoveL offs(pTurmL,0,0,i*zW),v1000,fine,VaccumOne\WObj:=wobjTurmL;
            WaitTime 0.3;
            SetDO hand_CmdVacuum1_L,high;
            WaitTime 0.3;
            MoveL offs(pTurmL,0,0,i*zW+10),v1000,z100,VaccumOne\WObj:=wobjTurmL;
            MoveJ offs(pKoopL,0,20,0),v1000,z100,VaccumOne\WObj:=wobjKoopL;
            MoveL offs(pKoopL,0,0,0),v1000,fine,VaccumOne\WObj:=wobjKoopL;
            WaitSyncTask ID_1, married_Tasks;
                            !WaitTime 0.3;
            SetDO hand_CmdVacuum1_L,low;
            WaitTime 0.3;
            MoveL offs(pKoopL,0,20,0),v1000,z100,VaccumOne\WObj:=wobjKoopL;
        ENDFOR
    ENDPROC
    PROC WeglegenL()
        FOR i FROM 0 TO nXY -1 do
            MoveJ offs(pKoopL,0,20,0),v1000,z100,VaccumOne\WObj:=wobjKoopL;
            MoveL offs(pKoopL,0,0,0),v1000,fine,VaccumOne\WObj:=wobjKoopL;
            WaitSyncTask ID_1, married_Tasks;
                            !WaitTime 0.3;
            SetDO hand_CmdVacuum1_L,high;
            WaitTime 0.3;
            MoveL offs(pKoopL,0,20,0),v1000,z100,VaccumOne\WObj:=wobjKoopL;
            !MoveAbsJ JointTarget_7,v1000,fine,VaccumOne\WObj:=wobjTurmL;
            MoveJ offs(pTurmL,0,0,i*zW+10),v1000,z100,VaccumOne\WObj:=wobjTurmL;
            MoveL offs(pTurmL,0,0,i*zW),v1000,fine,VaccumOne\WObj:=wobjTurmL;
            WaitTime 0.3;
            SetDO hand_CmdVacuum1_L,low;
            WaitTime 0.3;
            MoveL offs(pTurmL,0,0,i*zW+10),v1000,z100,VaccumOne\WObj:=wobjTurmL;
            !MoveAbsJ JointTarget_7,v1000,fine,VaccumOne\WObj:=wobjTurmL;
        ENDFOR
    ENDPROC
    PROC gotoHomeL()
        MoveAbsJ jPFreistellen,v1000,fine,VaccumOne\WObj:=wobjTurmL;
        MoveAbsJ jpHomeL,v1000,fine,VaccumOne\WObj:=wobjTurmL;
    ENDPROC
ENDMODULE