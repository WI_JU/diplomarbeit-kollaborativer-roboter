MODULE Module1
    CONST robtarget   pPalR := [[20,20,40],[0,0,1,0],[0,3,0,5],[-135,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST robtarget   pKoopR:= [[0,-20,0],[0.183012702,-0.183012702,0.683012702,0.683012702],[1,3,0,5],[-135,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget jpHomeR:=[[0,-130,30,0,40,0],[-135,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST jointtarget jp1:=    [[35,-105,30,235,-65,20],[-115,9E+09,9E+09,9E+09,9E+09,9E+09]];
    CONST num nX := 3;
    CONST num nY := 2;
    CONST num xW := 55;
    CONST num yW := 60;
    CONST num zW := 36;
    !_____________________________________________________
    VAR syncident ID_1;
    VAR syncident ID_2;
    VAR syncident ID_3;
    VAR syncident ID_4;
    VAR syncident ID_5;
    PERS tasks married_Tasks{2}:=[["T_ROB_L"],["T_ROB_R"]];
    !_____________________________________________________
    
    PROC AufnehmenR()
        FOR i FROM 0 TO nY-1 DO
            FOR j from nX-1 TO 0 step -1 DO
                MoveJ    offs(pPalR,j*xW,i*yW,50),v1000,z100,VaccumOne\WObj:=wobjPalR;
                MoveL    offs(pPalR,j*xW,i*yW,0),v1000,fine,VaccumOne\WObj:=wobjPalR;
                WaitTime 0.3;
                SetDO hand_CmdVacuum1_R,high;
                WaitTime 0.3;
                MoveL    offs(pPalR,j*xW,i*yW,50),v1000,z100,VaccumOne\WObj:=wobjPalR;
                MoveAbsJ jp1,v1000,z200,VaccumOne\WObj:=wobjKoopR;
                MoveJ    offs(pKoopR,0,-20,0),v1000,z10,VaccumOne\WObj:=wobjKoopR;
                MoveL    pKoopR,v1000,fine,VaccumOne\WObj:=wobjKoopR;
                WaitSyncTask ID_1, married_Tasks;
                WaitTime 0.3;
                SetDO hand_CmdVacuum1_R,low;
                WaitTime 0.3;
                MoveL    offs(pKoopR,0,-20,0),v1000,z10,VaccumOne\WObj:=wobjKoopR;
                MoveAbsJ jp1,v1000,z200,VaccumOne\WObj:=wobjKoopR;
            ENDFOR
        ENDFOR
    ENDPROC
    
    PROC WeglegenR()
        FOR j from nX-1 TO 0 Step -1 DO
            FOR i FROM 0 TO nY-1 DO
                MoveJ    offs(pKoopR,0,-20,0),v1000,z10,VaccumOne\WObj:=wobjKoopR;
                MoveL    pKoopR,v1000,fine,VaccumOne\WObj:=wobjKoopR;
                WaitSyncTask ID_1, married_Tasks;
                WaitTime 0.3;
                SetDO hand_CmdVacuum1_R,high;
                WaitTime 0.3;
                MoveL    offs(pKoopR,0,-20,0),v1000,z10,VaccumOne\WObj:=wobjKoopR;
                MoveAbsJ jp1,v1000,z200,VaccumOne\WObj:=wobjKoopR;
                MoveJ    offs(pPalR,j*xW,i*yW,50),v1000,z100,VaccumOne\WObj:=wobjPalR;
                MoveL    offs(pPalR,j*xW,i*yW,0),v1000,fine,VaccumOne\WObj:=wobjPalR;
                WaitTime 0.3;
                SetDO hand_CmdVacuum1_R,low;
                WaitTime 0.3;
                MoveL    offs(pPalR,j*xW,i*yW,50),v1000,z100,VaccumOne\WObj:=wobjPalR;
                MoveAbsJ jp1,v1000,z200,VaccumOne\WObj:=wobjKoopR;
            ENDFOR
        ENDFOR
    ENDPROC
    PROC gotoHomeR()
        MoveAbsJ jpHomeR,v1000,fine,VaccumOne\WObj:=wobjKoopR;
    ENDPROC
ENDMODULE