#
# Generated backup file, do not edit.
#
# Originally at: /hd0a/TEMP/backup.682412
# 2021-12-09  12:58:59
#
>>SYSTEM_ID:
14000-500281

>>PRODUCTS_ID:
RobotWare Version: 6.11.02.00
SmartGripper Version: 3.56.0000.00
FlexFeeder Version: 1.03

  Contact L
  Leadthrough
  YuMi AbsAcc Recovery
  637-1 Production Screen
  English
  603-1 Absolute Accuracy
  988-1 RW Add-In Prepared
  613-1 Collision Detection
  709-1 DeviceNet Master/Slave
  841-1 EtherNet/IP Scanner/Adapter
  617-1 FlexPendant Interface
  1341-1/1520-1 Integrated Vision Interface
  976-1 T10 Jogging Device
  604-1 MultiMove Coordinated
  623-1 Multitasking
  612-1 Path Offset
  611-1 Path Recovery
  616-1 PC Interface
  688-1 RobotStudio App Connect
  608-1 World Zones
  RobotWare Base
  Hall Sensor Calibration
  Drive System IRB 14000
  IRB 14000-0.5/0.5
  SmartGripper
  F1-DeviceNet

>>HOME:
All public files and directories

>>SYSPAR:
SYS.cfg
PROC.cfg
MMC.cfg
EIO.cfg
SIO.cfg
MOC.cfg


>>TASK1: (T_ROB_R,T_ROB_R,)
SYSMOD/user.sys @ 
PROGMOD/Module1.mod @ 
PROGMOD/CalibData.mod @ 

>>TASK2: (T_ROB_L,T_ROB_L,)
SYSMOD/user.sys @ 
PROGMOD/Module1.mod @ 
PROGMOD/CalibData.mod @ 

>>TASK3: (Feeder1,,)
SYSMOD/user.sys @ 
PROGMOD/FEEDER_MAIN.mod @ 


>>EOF:
