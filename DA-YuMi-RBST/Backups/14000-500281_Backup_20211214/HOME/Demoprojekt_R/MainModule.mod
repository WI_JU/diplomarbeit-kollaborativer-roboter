MODULE MainModule
	TASK PERS tooldata tool1:=[TRUE,[[-80.72,19.5079,39.7263],[0.710973,-0.00787092,-0.703132,-0.00778408]],[1,[0,0,0],[1,0,0,0],0,0,0]];
!Main ablauf Programm des Rechtenarmes
	PROC main()
        ! Vaccum ausschalten
        SetDO hand_CmdVacuum1_R,low;
        ! Roboter in Homeposition fahren
        gotoHomeR;
        ! Synchronisieren mit Linkenarm
        WaitSyncTask ID_1, married_Tasks;
        ! Aufnehmen von W�rfel und �bergabe an Linkenarm
        Abstand_R;
        AufnehmenR;
        ! Roboter in Homeposition fahren
        gotoHomeR;
        ! Synchronisieren mit Linkenarm
        WaitSyncTask ID_1, married_Tasks;
        ! Weglegen von W�rfel von �bergabepunkt auf Palette
        WeglegenR;
        ! Roboter in Homeposition fahren
        gotoHomeR;
	ENDPROC
ENDMODULE