MODULE FEEDER_MAIN
  !
  !********************************************************************
  !
  ! Copyright (c) ABB Automation GmbH 2017. All rights reserved
  !
  !********************************************************************
  !
  ! Module:       FEEDER_MAIN.mod
  !
  ! Description:  Software to implement on FlexFeeder
  !
  ! Version:      See below data definitions
  !
  ! History:      Version  Autor          Date        Reason
  !               -------------------------------------------
  !               0.1      Ivan Lundberg  2015-03-??  Created
  !               0.2      Christian Goy  2015-11-25  Adapted
  !               0.3      Willi Tyroke   2015-11-26  Hand to ABB standard structure, Remove GoTo, 
  !                                                   Change Fill to Load, Add Unload, Rename and add IO,
  !                                                   Add Error handling and Error message,
  !               0.4      Willi Tyroke   2015-12-02  Add Screen shake for quick move of parts on screen
  !               0.41     Willi Tyroke   2015-12-11  Adapt num declarations 
  !               0.42     Willi Tyroke   2015-12-16  Add WaitTime Part stop swing on screen
  !               0.43     Willi Tyroke   2016-01-07  Add Arm working area supervision
  !               0.44     Willi Tyroke   2016-01-25  Add Error Num+Message for working area, addapt Error handling,                                                      
  !                                                   add option to select bunker blow (bBlowAtCycleEnd) at end of feeder cycle.
  !                                                   Adaptet Error handling in FeederJamResolution. Massege to operator by unsolvable jam
  !               0.45     Willi Tyroke   2016-01-29  Push sectional in feeder cycle now selecteble
  !               0.6      Willi Tyroke   2016-02-02  Adapt FeederLoad and FeederUnload Signal flow and add WaitDI monitoring                                                     
  !               0.8      Willi Tyroke   2016-05-19  Adapt stTaskName (Feedback from Ivan)
  !               1.0      Willi Tyroke   2016-07-27  Adapt for new FlexFeeder GUI. 
  !                                                   New PROC: MsgGUI, SetCycleBool, FeederManControl, FeederWorkArea
  !                                                   The PROC call is now effected via Boolean variable. 
  !                                                   All message output is on the GUI with MsgGUI on 4 massages lines.
  !                                                   Everything has been optimized. Now thay are only one GUI loader in the HOME, for up to 4 FlexFeeder!
  !                                                   Add ad Init() ErrWrite if TASK Feeder1 is missing. GUI can not be loaded! 
  !               1.01     Willi Tyroke   2016-08-16  Correction of spelling.
  !               1.02     Willi Tyroke   2016-12-02  replace RemoveAllCyclicBool to depending RemoveCyclicBool 
  !               1.03     Taylor Hornung 2017-04-24  Add error handling for communication loss to feeder
  !                        Willi Tyroke   2017-04-27  Add ResetCycleBool and adapting error handling  
  !
  !*********************************************************************
  !
  !Version information
  CONST string stVerFeederMain{2}:=["V 1.03","2017-05-03"];
  !ToDo: DATA - DECLARATIONS
  !###################################################################
  !
  !                  D A T A  -  D E C L A R A T I O N S
  !
  !###################################################################
  !
  !**********************************************************
  !*                  signaldi declarations                 *
  !**********************************************************
  !Physical digital input Signals of the feeder
  VAR signaldi diLiftUp;
  VAR signaldi diLiftDown;
  VAR signaldi diPushForward;
  VAR signaldi diPushSectional;
  VAR signaldi diPushBackward;
  VAR signaldi diScreenOn;
  VAR signaldi diScreenOff;
  VAR signaldi diEmpty;
  !
  !**********************************************************
  !*                  signaldo declarations                 *
  !**********************************************************
  !Physical digital output Signals of the feeder
  VAR signaldo doLiftUp;
  VAR signaldo doLiftDown;
  VAR signaldo doPusher;
  VAR signaldo doScreenOn;
  VAR signaldo doScreenOff;
  VAR signaldo doSlowPushSpeed;
  VAR signaldo doBlow;
  !Virtual digital output Signals of the feeder
  VAR signaldo doReady;
  VAR signaldo doWorkAreaFree;
  VAR signaldo doTaskRun;
  !
  !**********************************************************
  !*                   Boolean variables                    *
  !**********************************************************
  !Flag to enable the sectional push befor | Feeder1-4
  PERS bool bActPushSectional{4}:=[FALSE,FALSE,FALSE,FALSE];
  !Flag to enable first screen shake at feeder cycle | Feeder1-4
  PERS bool bFirstScreenShake{4}:=[FALSE,FALSE,FALSE,FALSE];
  !Flag to enable second screen shake at feeder cycle and screen shake| Feeder1-4
  PERS bool bSecondScreenShake{4}:=[FALSE,FALSE,FALSE,FALSE];
  !Flag to enable blow, at end of feeder cycle | Feeder1-4
  PERS bool bBlowAtCycleEnd{4}:=[FALSE,FALSE,FALSE,FALSE];
  !Flag to enable bebug run| Feeder1-4
  PERS bool bDebugRun{4}:=[FALSE,FALSE,FALSE,FALSE];
  !Flag to enable GUI for feeder 1-4
  PERS bool bFeederGUI{4};
  !Flag to enable manual feeder control by GUI
  PERS bool bFeederManControl;
  !Flag to lock operator input on GUI to enabel feeder work area
  PERS bool bFeederGUIWorkArea{4};
  !Flag to enable FeederCycle
  PERS bool bFeederCycle{4};
  !Flag to enable FeederCycleContinuous
  PERS bool bFeederCycleContinuous{4};
  !Flag to enable FeederLoad
  PERS bool bFeederLoad{4};
  !Flag to enable FeederUnload
  PERS bool bFeederUnload{4};
  !Flag to enable FeederScreenShake
  PERS bool bFeederScreenShake{4};
  !Flag to enable FeederJamResolution
  TASK PERS bool bFeederJamResolution:=FALSE;
  !Cycle Bool to visualize the alias signaldi on GUI
  PERS bool bSetupCycleBoolDone{4};
  !Flag to lock that feeder is running  
  PERS bool bFeederRunning{4};
  !Bool cross connected to digital input
  PERS bool bDILiftUp;
  PERS bool bDILiftDown;
  PERS bool bDIPushForward;
  PERS bool bDIPushSectional;
  PERS bool bDIPushBackward;
  PERS bool bDIScreenOn;
  PERS bool bDIScreenOff;
  PERS bool bDIEmpty;
  !Bool cross connected to digital output
  PERS bool bDOLiftUp{4};
  PERS bool bDOLiftDown{4};
  PERS bool bDOPush{4};
  PERS bool bDOSlowPushSpeed{4};
  PERS bool bDOScreenOn{4};
  PERS bool bDOScreenOff{4};
  PERS bool bDOBlow{4};
  PERS bool bDOReady;
  PERS bool bDOWorkAreaFree;
  PERS bool bDOTaskRun;
  !If the massege is an Error then mark it and make the text color red | Feeder1-4
  PERS bool bMsg1Error{4};
  PERS bool bMsg2Error{4};
  PERS bool bMsg3Error{4};
  PERS bool bMsg4Error{4};
  !
  !**********************************************************
  !*                  num declarations                      *
  !**********************************************************
  !Error supervision MaxTime constants
  LOCAL PERS num nMaxTimeToHome:=2;
  LOCAL PERS num nMaxTimeWorkArea:=2;
  !Wait times 
  LOCAL PERS num nWaitTimeLiftUp:=2;
  LOCAL PERS num nWaitTimeLiftDown:=2;
  LOCAL PERS num nWaitTimeForSafty:=0.5;
  LOCAL PERS num nWaitTimePusherForw:=1.5;
  LOCAL PERS num nWaitTimePusherBack:=1.5;
  LOCAL PERS num nWaitTimeScreenOn:=1;
  LOCAL PERS num nWaitTimeScreenOff:=1;
  !Wait time after feeder cycle, befor starting new cycle
  PERS num nWaitTimeRunContinuous{4}:=[1,1,1,1];
  !Wait time after blowing in bunker, befor starting new cycle
  PERS num nWaitTimeAfterBlow{4}:=[0,0,0,0];
  !Wait time on parts stop with swing on screen 
  PERS num nWaitTimeStopSwing{4}:=[1,1,1,1];
  !Time to move pusher time based
  PERS num nTimePushSectional{4}:=[0.5,0.5,0.5,0.5];
  !Time for first screen shake, to separate or turn parts | Feeder1-4 
  PERS num nTimeFirstScreenShake{4}:=[0.2,0.2,0.2,0.2];
  !Time for second screen shake, to separate or turn parts | Feeder1-4 
  PERS num nTimeSecondScreenShake{4}:=[0.2,0.2,0.2,0.2];
  !Time to blow in bunker, befor starting new feeder cycle
  PERS num nTimeBlow{4}:=[1,1,1,1];
  !Default values for Feeder Settings
  PERS num nDefWaitTimeRunContinuous:=1;
  PERS num nDefWaitTimeAfterBlow:=0;
  PERS num nDefWaitTimeStopSwing:=1;
  PERS num nDefTimePushSectional:=0.5;
  PERS num nDefTimeFirstScreenShake:=0.2;
  PERS num nDefTimeSecondScreenShake:=0.2;
  PERS num nDefTimeBlow:=1;
  !variable to lock Clock Read
  VAR num nTime;
  !Error number for GUI
  LOCAL CONST num nErrorLiftUp:=1;
  LOCAL CONST num nErrorLiftDown:=2;
  LOCAL CONST num nErrorPushForward:=3;
  LOCAL CONST num nErrorPushBackward:=4;
  LOCAL CONST num nErrorScreenOn:=5;
  LOCAL CONST num nErrorScreenOff:=6;
  LOCAL CONST num nErrorWorkArea:=7;
  !Counter for ScreenShake Error. Used in ERROR handling!
  LOCAL PERS num nCountScreenShakeError:=0;
  !Current Feeder Task number
  LOCAL PERS num nFeederNum:=1;
  PERS num nLastActFeeder;
  !GUI
  !Active GUI Feeder Tap number
  PERS num nFeederActTap:=1;
  !
  !**********************************************************
  !*                  errnum declarations                   *
  !**********************************************************
  VAR errnum ERR_RESTART_FEED:=1;
  VAR errnum ERR_WORK_AREA:=2;
  VAR errnum ERR_FEEDER_LOST:=3;
  !
  !**********************************************************
  !*                  clock declarations                    *
  !**********************************************************
  !Feeder cycle time
  LOCAL VAR clock ckFeeder;
  !
  !**********************************************************
  !*                  string declarations                   *
  !**********************************************************
  !Current Task name
  TASK PERS string stTaskName:="Feeder1";
  !Use to show curent Massage at GUI
  PERS string stMsg1GUI{4};
  PERS string stMsg2GUI{4};
  PERS string stMsg3GUI{4};
  PERS string stMsg4GUI{4};
  !Maximum length of text              1234567890123456789012345678901234567890
  LOCAL CONST string stErrorText{10}:=["Signal Lifter Up is missing, Feeder ",
                                       "Signal Lifter Down is missing, Feeder ",
                                       "Signal Pusher Forward is missing, Feeder ",
                                       "Signal Pusher Backward is missing, Feeder ",
                                       "Signal Screen On is missing, Feeder ",
                                       "Signal Screen Off is missing, Feeder ",
                                       "Feeder working area blocked! Feeder ",
                                       "No Connection to Feeder ",
                                       "",
                                       ""];
  !Massage on TASK Windows. Operator interaction
  LOCAL CONST string stFeederJam{5}:=["Unable to resolve Jam in Feeder!"," ","All requirements will be reset!"," ","Please try to solve the problem manually!"];
  LOCAL CONST string stFeeder1Mis{5}:=["TASK Feeder 1 is missing!","To show the feeder GUI at FlexPendant,","it is necesary to select Feeder 1 for this,","System at the Installationsmanager!",""];
  !
  !**********************************************************
  !*                Button result variables                 *
  !**********************************************************
  !Button result for UIMessageBox
  VAR btnres resOPAnswer;
  !
  !ToDo: BASE ROUTINES
  !###################################################################
  !
  !                        ROUTINES
  !
  !###################################################################
  !
  !**********************************************************
  !*  Procedure SetAliasIO                                  *
  !*                                                        *
  !*  Description:                                          *
  !*                                                        *
  !*  This routine sets the name for the used signals       *
  !*  If other names are use in EIO it should be            *
  !*  changed here                                          *
  !*                                                        *
  !*  Date:          Version:    Programmer:       Reason:  *
  !*  25.11.2015       0.1       C. Goy            created  *
  !*  26.11.2015       0.2       W. Tyroke         adapted  *
  !*  19.05.2016       0.3       W. Tyroke         adapted  *
  !*  25.07.2016       1.0       W. Tyroke    add GUI enable*
  !**********************************************************
  PROC SetAliasIO()
    VAR string stIOName;

    !Connect alias with real digital input
    stIOName:="di"+stTaskName+"_LiftUp";
    AliasIO stIOName,diLiftUp;
    stIOName:="di"+stTaskName+"_LiftDown";
    AliasIO stIOName,diLiftDown;
    stIOName:="di"+stTaskName+"_PushForward";
    AliasIO stIOName,diPushForward;
    stIOName:="di"+stTaskName+"_PushSectional";
    AliasIO stIOName,diPushSectional;
    stIOName:="di"+stTaskName+"_PushBackward";
    AliasIO stIOName,diPushBackward;
    stIOName:="di"+stTaskName+"_ScreenOn";
    AliasIO stIOName,diScreenOn;
    stIOName:="di"+stTaskName+"_ScreenOff";
    AliasIO stIOName,diScreenOff;
    stIOName:="di"+stTaskName+"_Empty";
    AliasIO stIOName,diEmpty;
    !
    !connect alias with real digital output
    stIOName:="do"+stTaskName+"_LiftUp";
    AliasIO stIOName,doLiftUp;
    stIOName:="do"+stTaskName+"_LiftDown";
    AliasIO stIOName,doLiftDown;
    stIOName:="do"+stTaskName+"_Pusher";
    AliasIO stIOName,doPusher;
    stIOName:="do"+stTaskName+"_ScreenOn";
    AliasIO stIOName,doScreenOn;
    stIOName:="do"+stTaskName+"_ScreenOff";
    AliasIO stIOName,doScreenOff;
    stIOName:="do"+stTaskName+"_SlowPushSpeed";
    AliasIO stIOName,doSlowPushSpeed;
    stIOName:="do"+stTaskName+"_Blow";
    AliasIO stIOName,doBlow;
    !
    !Connect alias with virtual digital output
    stIOName:="do"+stTaskName+"_Ready";
    AliasIO stIOName,doReady;
    stIOName:="do"+stTaskName+"_WorkAreaFree";
    AliasIO stIOName,doWorkAreaFree;
    stIOName:="do"+stTaskName+"_TaskRun";
    AliasIO stIOName,doTaskRun;
    !
  ENDPROC

  !**********************************************************
  !*  Procedure SetCycleBool                                *
  !*                                                        *
  !*  Description:                                          *
  !*                                                        *
  !*  This routine conect the alias signals with bool       *
  !*  variabels to show the visualize the inputs on         *
  !*  FlexFeeder GUI.                                       *
  !*                                                        *
  !*  Date:          Version:    Programmer:       Reason:  *
  !*  26.07.2016       1.0       W. Tyroke         created  *
  !*  02.12.2016       1.02      W. Tyroke         adapted  *
  !*  24.04.2017       1.03      T. Hornung        err hand *
  !**********************************************************
  PROC SetCycleBool()
    VAR bool bTimeOut;
    !
    IF nFeederActTap=nFeederNum AND bSetupCycleBoolDone{nFeederNum}=FALSE THEN
      !Connect AliasDI with Bool
      RemoveCyclicBool bDILiftUp;
      SetupCyclicBool bDILiftUp,diLiftUp=1;
      RemoveCyclicBool bDILiftDown;
      SetupCyclicBool bDILiftDown,diLiftDown=1;
      RemoveCyclicBool bDIPushForward;
      SetupCyclicBool bDIPushForward,diPushForward=1;
      RemoveCyclicBool bDIPushSectional;
      SetupCyclicBool bDIPushSectional,diPushSectional=1;
      RemoveCyclicBool bDIPushBackward;
      SetupCyclicBool bDIPushBackward,diPushBackward=1;
      RemoveCyclicBool bDIScreenOn;
      SetupCyclicBool bDIScreenOn,diScreenOn=1;
      RemoveCyclicBool bDIScreenOff;
      SetupCyclicBool bDIScreenOff,diScreenOff=1;
      RemoveCyclicBool bDIEmpty;
      SetupCyclicBool bDIEmpty,diEmpty=1;
      !Connect AliasDO with Bool
      RemoveCyclicBool bDOReady;
      SetupCyclicBool bDOReady,doReady=1;
      RemoveCyclicBool bDOWorkAreaFree;
      SetupCyclicBool bDOWorkAreaFree,doWorkAreaFree=1;
      RemoveCyclicBool bDOTaskRun;
      SetupCyclicBool bDOTaskRun,doTaskRun=1;
      !Flag to now that the connection was done
      bSetupCycleBoolDone{nFeederNum}:=TRUE;
      nLastActFeeder:=nFeederNum;
    ELSEIF nFeederActTap<>nFeederNum AND bSetupCycleBoolDone{nFeederNum}=TRUE THEN
      bSetupCycleBoolDone{nFeederNum}:=FALSE;
      WaitUntil bSetupCycleBoolDone{nFeederActTap}=TRUE\MaxTime:=0.5\TimeFlag:=bTimeOut;
      IF bTimeOut=TRUE OR nLastActFeeder=nFeederNum THEN
        !Disconnect AliasDI/DO from Bool
        ResetCycleBool;
        bSetupCycleBoolDone{nFeederActTap}:=FALSE;
        bDOTaskRun:=FALSE;
      ENDIF
    ENDIF
    !Reset maunal control buttons if the GUI is not longer in ManControl
    IF bFeederManControl=FALSE THEN
      bDOLiftUp{nFeederNum}:=FALSE;
      bDOLiftDown{nFeederNum}:=FALSE;
      bDOSlowPushSpeed{nFeederNum}:=FALSE;
      bDOPush{nFeederNum}:=FALSE;
      bDOScreenOn{nFeederNum}:=FALSE;
      bDOScreenOff{nFeederNum}:=FALSE;
      bDOBlow{nFeederNum}:=FALSE;
    ENDIF
    !
  ERROR
    RAISE ;
  ENDPROC

  !**********************************************************
  !*  Procedure ResetCycleBool                              *
  !*                                                        *
  !*  Description:                                          *
  !*                                                        *
  !*  This routine delet the conect with alias signals and  *
  !*  bool variabels.                                       *
  !*                                                        *
  !*  Date:          Version:    Programmer:       Reason:  *
  !*  28.04.2017       1.0       W. Tyroke         created  *
  !**********************************************************
  PROC ResetCycleBool()
    !Disconnect AliasDI/DO from Bool
    RemoveCyclicBool bDILiftUp;
    RemoveCyclicBool bDILiftDown;
    RemoveCyclicBool bDIPushForward;
    RemoveCyclicBool bDIPushSectional;
    RemoveCyclicBool bDIPushBackward;
    RemoveCyclicBool bDIScreenOn;
    RemoveCyclicBool bDIScreenOff;
    RemoveCyclicBool bDIEmpty;
    RemoveCyclicBool bDOReady;
    RemoveCyclicBool bDOWorkAreaFree;
    RemoveCyclicBool bDOTaskRun;
    !
  ERROR
    RAISE ;
  ENDPROC

  !**********************************************************
  !*  Procedure Init                                        *
  !*                                                        *
  !*  Description:                                          *
  !*                                                        *
  !*  This procedure sets the initial values for the        *
  !*  production sequence.                                  *
  !*                                                        *
  !*  Date:          Version:    Programmer:       Reason:  *
  !*  ??.03.2015       0.1       I. Lundberg       created  *
  !*  26.11.2015       0.2       W. Tyroke         adapted  *
  !*  27.07.2016       1.0       W. Tyroke         adapted  *
  !*  24.04.2017       1.03      T. Hornung        err hand *
  !**********************************************************
  PROC Init()
    VAR string stTemp;
    VAR bool bTemp;
    
    !Write current Task name
    stTaskName:=GetTaskName();
    stTemp:=StrPart(stTaskName,7,1);
    bTemp:=StrToVal(stTemp,nFeederNum);
    !Enable GUI for feeder 
    bFeederGUI{nFeederNum}:=TRUE;
    !
    !check feeder connection
    IF NOT (IOUnitState(stTaskName+"_Device"\Phys)=IOUNIT_PHYS_STATE_RUNNING) THEN
      MsgGUI\ErrorNum:=8;
      WaitUntil(IOUnitState(stTaskName+"_Device"\Phys)=IOUNIT_PHYS_STATE_RUNNING);
    ENDIF
    MsgGUI\Msg:="Connection Established on Feeder ";
    !Init Alias IO
    SetAliasIO;
    MsgGUI\Msg:="Initializing Feeder ";
    !Set startup value
    bSetupCycleBoolDone{nFeederNum}:=FALSE;
    bFeederCycle{nFeederNum}:=FALSE;
    bFeederCycleContinuous{nFeederNum}:=FALSE;
    bFeederLoad{nFeederNum}:=FALSE;
    bFeederUnload{nFeederNum}:=FALSE;
    bFeederScreenShake{nFeederNum}:=FALSE;
    bFeederJamResolution:=FALSE;
    bFeederManControl:=FALSE;
    bFeederRunning{nFeederNum}:=FALSE;
    nCountScreenShakeError:=0;
    !Init CycleBool
    SetCycleBool;
    IF bFeederGUI{1}=FALSE ErrWrite stFeeder1Mis{1},stFeeder1Mis{2}\RL2:=stFeeder1Mis{3}\RL3:=stFeeder1Mis{4}\RL4:=stFeeder1Mis{5};
    !Reset Error Text and number
    MsgGUI\Msg:="No Error on Feeder ";
    !Reset and start cycle time
    ClkReset ckFeeder;
    ClkStart ckFeeder;
    !
  ERROR
    IF ERRNO=ERR_NORUNUNIT OR ERRNO=ERR_FEEDER_LOST THEN
      SkipWarn;
      !Disconnect AliasDI/DO from Bool
      ResetCycleBool;
      ExitCycle;
    ELSE
      RAISE ;
    ENDIF
  ENDPROC

  !**********************************************************
  !*  Procedure main                                        *
  !*                                                        *
  !*  Description:                                          *
  !*                                                        *
  !*  This procedure is the Main routine and controls       *
  !*  whole process of the user program.                    *
  !*                                                        *
  !*  Date:          Version:    Programmer:       Reason:  *
  !*  ??.03.2015       0.1       I. Lundberg       created  *
  !*  27.11.2015       0.2       W. Tyroke         adapted  *
  !*  25.01.2016       0.3       W. Tyroke         adapted  *
  !*  27.07.2016       1.0       W. Tyroke      adapted Msg *
  !*  24.04.2017       1.03      T. Hornung        err hand *
  !**********************************************************
  PROC main()
    !Set initial values
    Init;
    !production loop
    WHILE TRUE DO
      !Continuously check feeder for lossed connection
      IF NOT (IOUnitState(stTaskName+"_Device"\Phys)=IOUNIT_PHYS_STATE_RUNNING) THEN
        RAISE ERR_FEEDER_LOST;
      ENDIF
      !Setup or Remove CycleBool
      SetCycleBool;
      !Call Feeder action depending on Bool/GUI
      !If operator push switsh on GUI, reset feeder work area
      FeederWorkArea;
      !Condition for feeder cycle or continuous run
      IF bFeederCycle{nFeederNum}=TRUE OR bFeederCycleContinuous{nFeederNum}=TRUE FeederCycle;
      !Condition for feeder load
      IF bFeederLoad{nFeederNum}=TRUE FeederLoad;
      !Condition for feeder unload
      IF bFeederUnload{nFeederNum}=TRUE FeederUnload;
      !Condition for feeder screen shake
      IF bFeederScreenShake{nFeederNum}=TRUE FeederScreenShake;
      !In case of feeder jam try to resolve
      IF bFeederJamResolution=TRUE FeederJamResolution;
      !Condition for manual feeder control
      IF bFeederManControl=TRUE AND nFeederActTap=nFeederNum FeederManControl;
      WaitTime 0.01;
      !
    ENDWHILE
    !
  ERROR(ERR_RESTART_FEED,ERR_NORUNUNIT,ERR_FEEDER_LOST)
    IF ERRNO=ERR_RESTART_FEED THEN
      !Write Error Text in string to show on GUI 
      IF doLiftUP=1 AND diLiftUp=0 MsgGUI\ErrorNum:=nErrorLiftUp;
      IF doLiftDown=1 AND diLiftDown=0 MsgGUI\ErrorNum:=nErrorLiftDown;
      IF doPusher=1 AND diPushForward=0 MsgGUI\ErrorNum:=nErrorPushForward;
      IF doPusher=0 AND diPushBackward=0 MsgGUI\ErrorNum:=nErrorPushBackward;
      IF doScreenOn=1 AND diScreenOn=0 MsgGUI\ErrorNum:=nErrorScreenOn;
      IF doScreenOff=1 AND diScreenOff=0 MsgGUI\ErrorNum:=nErrorScreenOff;
      IF doWorkAreaFree=0 MsgGUI\ErrorNum:=nErrorWorkArea;
      !Test nFeederAction and make a new loop
      TRYNEXT;
    ELSEIF ERRNO=ERR_NORUNUNIT OR ERRNO=ERR_FEEDER_LOST THEN
      SkipWarn;
      !Disconnect AliasDI/DO from Bool
      ResetCycleBool;
      ExitCycle;
    ELSE
      RETURN ;
    ENDIF
  ENDPROC

  !**********************************************************
  !*  Procedure FeederCycle                                 *
  !*                                                        *
  !*  Description:                                          *
  !*                                                        *
  !*  The routine is responsible for the Feeder cycle.      *
  !*                                                        *
  !*  Date:          Version:    Programmer:       Reason:  *
  !*  ??.03.2015       0.1       I. Lundberg       created  *
  !*  25.11.2015       0.2       C. Goy            adapted  *
  !*  01.12.2015       0.3       W. Tyroke      screen shake*
  !*  16.12.2015       0.4       W. Tyroke      add WaitTime*
  !*  07.01.2016       0.5       W. Tyroke      Working area*
  !*  25.01.2016       0.6       W. Tyroke      add ERR area*
  !*  24.04.2017       1.03      T. Hornung        err hand *
  !**********************************************************
  PROC FeederCycle()
    VAR bool bWorkAreaBlock;

    Reset doReady;
    !Wait until Robot out of Feeder working area
    WaitDO doWorkAreaFree,1\MaxTime:=nMaxTimeWorkArea\TimeFlag:=bWorkAreaBlock;
    IF bWorkAreaBlock=TRUE RAISE ERR_WORK_AREA;
    MsgGUI\Msg:="Run cycle Feeder ";
    !Retract all actuators
    bFeederRunning{nFeederNum}:=TRUE;
    Set doLiftDown;
    Reset doLiftUp;
    Reset doPusher;
    Reset doScreenOn;
    Set doScreenOff;
    !Wait for all actuators to reach home position
    WaitUntil(diLiftDown=1 AND diPushBackward=1 AND diScreenOff=1)\MaxTime:=nMaxTimeToHome;
    nTime:=ClkRead(ckFeeder);
    ClkStart ckFeeder;
    !Bring Lift Up
    Reset doLiftDown;
    Set doLiftUp;
    !Wait for Lift
    WaitDI diLiftUp,1\MaxTime:=nWaitTimeLiftUp;
    IF bDebugRun{nFeederNum}=TRUE MsgGUI\Msg:="LiftUp time = "+NumToStr(ClkRead(ckFeeder)-nTime,3)+" | F";
    !
    !Wait time for safety reason: do not clamp parts that are falling from the lift
    WaitTime nWaitTimeForSafty;
    !
    !Bring pusher sectional out
    IF bActPushSectional{nFeederNum}=TRUE THEN
      Set doSlowPushSpeed;
      Set doPusher;
      !Move pusher time based (alternative to half way, if parts are dropping to early) 
      WaitTime nTimePushSectional{nFeederNum};
    ENDIF
    !Retract Pusher, bring out screen
    nTime:=ClkRead(ckFeeder);
    Reset doScreenOff;
    Set doScreenOn;
    Reset doPusher;
    !
    !Wait for pusher home position and screen out position
    WaitDI diPushBackward,1\MaxTime:=nWaitTimePusherBack;
    WaitDI diScreenOn,1\MaxTime:=nWaitTimeScreenOn;
    IF bDebugRun{nFeederNum}=TRUE MsgGUI\Msg:="SceenOn time = "+NumToStr(ClkRead(ckFeeder)-nTime,3)+" | F";
    !
    nTime:=ClkRead(ckFeeder);
    !Set speed fast and bring out pusher
    Reset doSlowPushSpeed;
    Set doPusher;
    WaitDI diPushForward,1\MaxTime:=nWaitTimePusherForw;
    IF bDebugRun{nFeederNum}=TRUE MsgGUI\Msg:="PushOut time = "+NumToStr(ClkRead(ckFeeder)-nTime,3)+" | F";
    !
    nTime:=ClkRead(ckFeeder);
    !Retract Pusher and set FeederReady
    Reset doPusher;
    WaitDI diPushBackward,1\MaxTime:=nWaitTimePusherBack;
    IF bDebugRun{nFeederNum}=TRUE MsgGUI\Msg:="PushBack time = "+NumToStr(ClkRead(ckFeeder)-nTime,3)+" | F";
    !
    !First screen shake
    IF bFirstScreenShake{nFeederNum}=TRUE THEN
      nTime:=ClkRead(ckFeeder);
      Reset doScreenOn;
      Set doScreenOff;
      WaitTime nTimeFirstScreenShake{nFeederNum};
      Set doScreenOn;
      Reset doScreenOff;
      WaitDI diScreenOn,1\MaxTime:=nWaitTimeScreenOn;
      !If the second screen shake is enable, move screen again
      IF bSecondScreenShake{nFeederNum}=TRUE THEN
        Reset doScreenOn;
        Set doScreenOff;
        WaitTime nTimeSecondScreenShake{nFeederNum};
        Set doScreenOn;
        Reset doScreenOff;
        WaitDI diScreenOn,1\MaxTime:=nWaitTimeScreenOn;
      ENDIF
      IF bDebugRun{nFeederNum}=TRUE MsgGUI\Msg:="Screen Shake time = "+NumToStr(ClkRead(ckFeeder)-nTime,3)+" | F";
    ENDIF
    ! 
    nTime:=ClkRead(ckFeeder);
    !Retract Lift (to save time next cycle)
    Set doLiftDown;
    Reset doLiftUp;
    bFeederCycle{nFeederNum}:=FALSE;
    !Wait time on parts stop with swing on screen 
    WaitTime nWaitTimeStopSwing{nFeederNum};
    WaitTime 0.001;
    Set doReady;
    !Wait for Lift down
    WaitDI diLiftDown,1\MaxTime:=nWaitTimeLiftDown;
    IF bDebugRun{nFeederNum}=TRUE MsgGUI\Msg:="LiftDown time = "+NumToStr(ClkRead(ckFeeder)-(nTime+nWaitTimeStopSwing{nFeederNum}),3)+" | F";
    !Blow parts in bunker 
    IF bBlowAtCycleEnd{nFeederNum}=TRUE THEN
      PulseDO\PLength:=nTimeBlow{nFeederNum},doBlow;
      WaitTime nWaitTimeAfterBlow{nFeederNum};
    ENDIF
    !Wait time if feeder run continuous
    IF bFeederCycleContinuous{nFeederNum}=TRUE WaitTime nWaitTimeRunContinuous{nFeederNum};
    !Set Feeder status
    bFeederRunning{nFeederNum}:=FALSE;
    ClkStop ckFeeder;
    !
  ERROR
    IF ERRNO=ERR_WAIT_MAXTIME THEN
      SkipWarn;
      bFeederJamResolution:=TRUE;
      RAISE ERR_RESTART_FEED;
    ELSEIF ERRNO=ERR_WORK_AREA THEN
      RAISE ERR_RESTART_FEED;
    ELSE
      RAISE ;
    ENDIF
  ENDPROC

  !**********************************************************
  !*  Procedure FeederLoad                                  *
  !*                                                        *
  !*  Description:                                          *
  !*                                                        *
  !*  Routine to bring the feeder in load position.         *
  !*                                                        *
  !*  Date:          Version:    Programmer:       Reason:  *
  !*  ??.03.2015       0.1       I. Lundberg       created  *
  !*  02.02.2016       0.2       W. Tyroke         adapted  *
  !*  24.04.2017       1.03      T. Hornung        err hand *
  !**********************************************************
  PROC FeederLoad()
    !
    Reset doReady;
    MsgGUI\Msg:="Load Feeder ";
    !Retract all actuators
    bFeederRunning{nFeederNum}:=TRUE;
    Reset doPusher;
    WaitDI diPushBackward,1\MaxTime:=nWaitTimePusherBack;
    Reset doLiftDown;
    Set doLiftUp;
    WaitDI diLiftUp,1\MaxTime:=nWaitTimeLiftUp;
    Reset doScreenOn;
    Set doScreenOff;
    WaitDI diScreenOff,1\MaxTime:=nWaitTimeScreenOff;
    Set doSlowPushSpeed;
    Set doPusher;
    WaitDI diPushForward,1\MaxTime:=nWaitTimePusherForw;
    !            1234567890123456789012345678901234567890
    MsgGUI\Msg:="Please confirm the loading process! | F";
    WaitUntil bFeederLoad{nFeederNum}=FALSE;
    !Set Feeder status
    bFeederRunning{nFeederNum}:=FALSE;
    WaitTime 0.001;
    Set doReady;
    !
  ERROR
    IF ERRNO=ERR_WAIT_MAXTIME THEN
      SkipWarn;
      bFeederJamResolution:=TRUE;
      RAISE ERR_RESTART_FEED;
    ELSE
      RAISE ;
    ENDIF
  ENDPROC

  !**********************************************************
  !*  Procedure FeederUnload                                *
  !*                                                        *
  !*  Description:                                          *
  !*                                                        *
  !*  Routine to bring the feeder in unload position.       *
  !*                                                        *
  !*  Date:          Version:    Programmer:       Reason:  *
  !*  27.11.2015       0.1       W. Tyroke         created  *
  !*  02.02.2016       0.2       W. Tyroke         adaptet  *
  !*  24.04.2017       1.03      T. Hornung        err hand *
  !**********************************************************
  PROC FeederUnload()
    !
    Reset doReady;
    MsgGUI\Msg:="Unoad Feeder ";
    !Set all actuators
    bFeederRunning{nFeederNum}:=TRUE;
    Reset doPusher;
    WaitDI diPushBackward,1\MaxTime:=nWaitTimePusherBack;
    Reset doLiftDown;
    Set doLiftUp;
    WaitDI diLiftUp,1\MaxTime:=nWaitTimeLiftUp;
    Reset doScreenOn;
    Set doScreenOff;
    WaitDI diScreenOff,1\MaxTime:=nWaitTimeScreenOff;
    Set doSlowPushSpeed;
    Set doPusher;
    WaitDI diPushForward,1\MaxTime:=nWaitTimePusherForw;
    !
    WHILE bFeederUnload{nFeederNum}=TRUE DO
      PulseDO\PLength:=nTimeBlow{nFeederNum},doBlow;
      !            1234567890123456789012345678901234567890
      MsgGUI\Msg:="Please confirm the unloading process! F";
      WaitTime 4;
    ENDWHILE
    !Set Feeder status
    bFeederRunning{nFeederNum}:=FALSE;
    WaitTime 0.001;
    Set doReady;
    !
  ERROR
    IF ERRNO=ERR_WAIT_MAXTIME THEN
      SkipWarn;
      bFeederJamResolution:=TRUE;
      RAISE ERR_RESTART_FEED;
    ELSE
      RAISE ;
    ENDIF
  ENDPROC

  !**********************************************************
  !*  Procedure FeederScreenShake                           *
  !*                                                        *
  !*  Description:                                          *
  !*                                                        *
  !*  Routine for screen shake to move the parts on screen. *
  !*                                                        *
  !*  Date:          Version:    Programmer:       Reason:  *
  !*  02.12.2015       0.1       W. Tyroke         created  *
  !*  16.12.2015       0.2       W. Tyroke      add WaitTime*
  !*  07.01.2016       0.3       W. Tyroke      Working area*
  !*  25.01.2016       0.4       W. Tyroke      add ERR area*
  !*  24.04.2017       1.03      T. Hornung        err hand *
  !**********************************************************
  PROC FeederScreenShake()
    VAR bool bWorkAreaBlock;

    !Wait until Robot out of Feeder working area
    WaitDO doWorkAreaFree,1\MaxTime:=nMaxTimeWorkArea\TimeFlag:=bWorkAreaBlock;
    IF bWorkAreaBlock=TRUE RAISE ERR_WORK_AREA;
    MsgGUI\Msg:="Screen Shake Feeder ";
    bFeederRunning{nFeederNum}:=TRUE;
    nTime:=ClkRead(ckFeeder);
    ClkStart ckFeeder;
    !Set all actuators
    Reset doScreenOn;
    Set doScreenOff;
    WaitTime nTimeFirstScreenShake{nFeederNum};
    Set doScreenOn;
    Reset doScreenOff;
    WaitDI diScreenOn,1\MaxTime:=nWaitTimeScreenOn;
    !If the second screen shake is enable, move screen again
    IF bSecondScreenShake{nFeederNum}=TRUE THEN
      Reset doScreenOn;
      Set doScreenOff;
      WaitTime nTimeSecondScreenShake{nFeederNum};
      Set doScreenOn;
      Reset doScreenOff;
      WaitDI diScreenOn,1\MaxTime:=nWaitTimeScreenOn;
    ENDIF
    IF bDebugRun{nFeederNum}=TRUE MsgGUI\Msg:="Screen Shake time = "+NumToStr(ClkRead(ckFeeder)-nTime,3)+" | F";
    !Wait time on parts stop with swing on screen 
    WaitTime nWaitTimeStopSwing{nFeederNum};
    bFeederScreenShake{nFeederNum}:=FALSE;
    !Set Feeder status
    WaitTime 0.001;
    Set doReady;
    bFeederRunning{nFeederNum}:=FALSE;
    !
  ERROR
    IF ERRNO=ERR_WAIT_MAXTIME THEN
      SkipWarn;
      bFeederScreenShake{nFeederNum}:=TRUE;
      Incr nCountScreenShakeError;
      IF nCountScreenShakeError>4 bFeederJamResolution:=TRUE;
      RAISE ERR_RESTART_FEED;
    ELSEIF ERRNO=ERR_WORK_AREA THEN
      RAISE ERR_RESTART_FEED;
    ELSE
      RAISE ;
    ENDIF
  ENDPROC

  !**********************************************************
  !*  Procedure FeederJamResolution                         *
  !*                                                        *
  !*  Description:                                          *
  !*                                                        *
  !*  Routine for case of feeder jam resolution.            *
  !*                                                        *
  !*  Date:          Version:    Programmer:       Reason:  *
  !*  ??.03.2015       0.1       I. Lundberg       created  *
  !*  25.11.2015       0.2       C. Goy            adapted  *
  !*  26.11.2015       0.3       W. Tyroke      Remove GoTo *
  !*  01.12.2015                                Add ErrorMsg*
  !*  07.01.2016       0.4       W. Tyroke      Working area* 
  !*  25.01.2016       0.5       W. Tyroke      add ERR area*
  !*  27.07.2016       1.0       W. Tyroke      adapted Msg *
  !*  24.04.2017       1.03      T. Hornung        err hand *
  !**********************************************************
  PROC FeederJamResolution()
    VAR bool bTimeOut;
    VAR bool bFeederIO;
    VAR bool bWorkAreaBlock;
    VAR num nCounter;
    VAR num nCASE;

    MsgGUI\Msg:="Jam Resolution! Feeder ";
    bFeederRunning{nFeederNum}:=TRUE;
    nCounter:=0;
    nCASE:=1;
    !Retract all actuators
    WHILE bFeederIO=FALSE DO
      !Wait until Robot out of Feeder working area
      WaitDO doWorkAreaFree,1\MaxTime:=nMaxTimeWorkArea\TimeFlag:=bWorkAreaBlock;
      IF bWorkAreaBlock=TRUE RAISE ERR_WORK_AREA;
      !
      TEST nCASE
      CASE 1:
        !Reset all
        Set doLiftDown;
        Reset doLiftUp;
        Reset doPusher;
        Reset doScreenOn;
        Set doScreenOff;
      CASE 2:
        !Cycle pusher
        Reset doSlowPushSpeed;
        Set doPusher;
        WaitTime nWaitTimePusherForw;
        Reset doPusher;
      CASE 3:
        !Cycle screen
        Set doScreenOn;
        Reset doScreenOff;
        WaitTime nWaitTimeScreenOn;
        Reset doScreenOn;
        Set doScreenOff;
      CASE 4:
        !Cycle Lift
        IF diPushBackward=1 THEN
          Reset doLiftDown;
          Set doLiftUp;
        ENDIF
        WaitTime nWaitTimeLiftUp;
        Set doLiftDown;
        Reset doLiftUp;
      CASE 5:
        !Increas try counter
        Incr nCounter;
        IF nCounter>2 THEN
          ErrWrite "Feeder "+NumToStr(nFeederNum,0)+" Jam!","Unable to resolve Jam in Feeder "+NumToStr(nFeederNum,0);
          resOPAnswer:=UIMessageBox(\Header:="Feeder "+NumToStr(nFeederNum,0)+" Jam!"\MsgArray:=stFeederJam\Buttons:=btnOK\Icon:=iconError\Image:="FlexFeeder.jpg");
          bFeederCycle{nFeederNum}:=FALSE;
          Reset doReady;
          bFeederCycleContinuous{nFeederNum}:=FALSE;
          bFeederLoad{nFeederNum}:=FALSE;
          bFeederUnload{nFeederNum}:=FALSE;
          bFeederScreenShake{nFeederNum}:=FALSE;
          bFeederJamResolution:=FALSE;
          bFeederRunning{nFeederNum}:=FALSE;
          RETURN ;
        ENDIF
        nCASE:=0;
      ENDTEST
      Incr nCASE;
      !Wait for all actuators to reach home position
      WaitUntil(diLiftDown=1 AND diPushBackward=1 AND diScreenOff=1)\MaxTime:=nMaxTimeToHome\TimeFlag:=bTimeOut;
      IF bTimeOut=FALSE THEN
        bFeederIO:=TRUE;
        nCountScreenShakeError:=0;
      ELSE
        !Write Error Text in string to show on GUI 
        IF doLiftDown=1 AND diLiftDown=0 MsgGUI\ErrorNum:=nErrorLiftDown;
        IF doPusher=0 AND diPushBackward=0 MsgGUI\ErrorNum:=nErrorPushBackward;
        IF doScreenOff=1 AND diScreenOff=0 MsgGUI\ErrorNum:=nErrorScreenOff;
      ENDIF
    ENDWHILE
    !
    bFeederJamResolution:=FALSE;
    !Set Feeder status
    bFeederRunning{nFeederNum}:=FALSE;
    !Reset Error Text and number
    MsgGUI\Msg:="No Error on Feeder ";
    RETURN ;
    !
  ERROR
    IF ERRNO=ERR_WAIT_MAXTIME THEN
      SkipWarn;
      bFeederJamResolution:=TRUE;
      RAISE ERR_RESTART_FEED;
    ELSEIF ERRNO=ERR_WORK_AREA THEN
      RAISE ERR_RESTART_FEED;
    ELSE
      RAISE ;
    ENDIF
  ENDPROC

  !**********************************************************
  !*  Procedure FeederManControl                            *
  !*                                                        *
  !*  Description:                                          *
  !*                                                        *
  !*  Routine for manual feeder control activated by GUI.   *
  !*                                                        *
  !*  Date:          Version:    Programmer:       Reason:  *
  !*  03.08.2016       1.0       W. Tyroke         created  *
  !**********************************************************
  PROC FeederManControl()
    !
    !Feeder manual control loop
    WHILE bFeederManControl=TRUE AND nFeederActTap=nFeederNum DO
      Reset doReady;
      !Setup or Remove CycleBool
      SetCycleBool;
      bFeederRunning{nFeederNum}:=TRUE;
      !Set and Reset IO depending on GUI input
      IF bDOLiftUp{nFeederNum}=TRUE SetDo doLiftUp,1;
      IF bDOLiftUp{nFeederNum}=FALSE SetDo doLiftUp,0;
      IF bDOLiftDown{nFeederNum}=TRUE SetDo doLiftDown,1;
      IF bDOLiftDown{nFeederNum}=FALSE SetDo doLiftDown,0;
      IF bDOSlowPushSpeed{nFeederNum}=TRUE SetDo doSlowPushSpeed,1;
      IF bDOSlowPushSpeed{nFeederNum}=FALSE SetDo doSlowPushSpeed,0;
      IF bDOPush{nFeederNum}=TRUE SetDo doPusher,1;
      IF bDOPush{nFeederNum}=FALSE SetDo doPusher,0;
      IF bDOScreenOn{nFeederNum}=TRUE SetDo doScreenOn,1;
      IF bDOScreenOn{nFeederNum}=FALSE SetDo doScreenOn,0;
      IF bDOScreenOff{nFeederNum}=TRUE SetDo doScreenOff,1;
      IF bDOScreenOff{nFeederNum}=FALSE SetDo doScreenOff,0;
      IF bDOBlow{nFeederNum}=TRUE SetDo doBlow,1;
      IF bDOBlow{nFeederNum}=FALSE SetDo doBlow,0;
      WaitTime 0.01;
      !
    ENDWHILE
    !Set Feeder status
    bFeederRunning{nFeederNum}:=FALSE;
    !
  ERROR
    RAISE ;
  ENDPROC

  !**********************************************************
  !*  Procedure MsgGUI                                      *
  !*                                                        *
  !*  Description:                                          *
  !*                                                        *
  !*  Routine to write message in GUI and show the last 3   *
  !*  showed message on GUI.                                *
  !*                                                        *
  !*  Date:          Version:    Programmer:       Reason:  *
  !*  27.07.2016       1.0       W. Tyroke         created  *
  !**********************************************************
  PROC MsgGUI(\string Msg|num ErrorNum)
    VAR string stTemp;

    !Check first if this is a new message or if they is already displayed in the GUI
    IF Present(ErrorNum) THEN
      stTemp:=stErrorText{ErrorNum}+NumToStr(nFeederNum,0);
      IF stMsg1GUI{nFeederNum}=stTemp RETURN ;
    ELSEIF Present(Msg) THEN
      stTemp:=Msg+NumToStr(nFeederNum,0);
      IF stMsg1GUI{nFeederNum}=stTemp RETURN ;
    ENDIF
    !Write last massege in buffer and transfer textcolor flag
    bMsg4Error{nFeederNum}:=FALSE;
    stMsg4GUI{nFeederNum}:=stMsg3GUI{nFeederNum};
    IF bMsg3Error{nFeederNum}=TRUE bMsg4Error{nFeederNum}:=TRUE;
    bMsg3Error{nFeederNum}:=FALSE;
    stMsg3GUI{nFeederNum}:=stMsg2GUI{nFeederNum};
    IF bMsg2Error{nFeederNum}=TRUE bMsg3Error{nFeederNum}:=TRUE;
    bMsg2Error{nFeederNum}:=FALSE;
    stMsg2GUI{nFeederNum}:=stMsg1GUI{nFeederNum};
    IF bMsg1Error{nFeederNum}=TRUE bMsg2Error{nFeederNum}:=TRUE;
    !Write new massage
    IF Present(ErrorNum) THEN
      bMsg1Error{nFeederNum}:=TRUE;
      stMsg1GUI{nFeederNum}:=stErrorText{ErrorNum}+NumToStr(nFeederNum,0);
    ELSEIF Present(Msg) THEN
      bMsg1Error{nFeederNum}:=FALSE;
      stMsg1GUI{nFeederNum}:=Msg+NumToStr(nFeederNum,0);
    ENDIF
    !
  ERROR
    Stop;
    RAISE ;
  ENDPROC

  !**********************************************************
  !*  Procedure FeederWorkArea                              *
  !*                                                        *
  !*  Description:                                          *
  !*                                                        *
  !*  Call from GUI command. Change state depending on      *
  !*  virtual output and bool.                              *
  !*                                                        *
  !*  Date:          Version:    Programmer:       Reason:  *
  !*  05.08.2016       1.0       W. Tyroke         created  *
  !**********************************************************
  PROC FeederWorkArea()
    !
    IF DOutput(doWorkAreaFree)=0 AND bFeederGUIWorkArea{nFeederNum}=TRUE SetDo doWorkAreaFree,1;
    bFeederGUIWorkArea{nFeederNum}:=FALSE;
    !
  ERROR
    RAISE ;
  ENDPROC
ENDMODULE
