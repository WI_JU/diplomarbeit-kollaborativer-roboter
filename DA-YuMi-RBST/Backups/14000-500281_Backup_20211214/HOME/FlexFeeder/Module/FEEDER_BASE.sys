MODULE FEEDER_BASE(SYSMODULE,NOVIEW)
  !********************************************************************
  !
  ! Copyright (c) ABB Automation GmbH 2017. All rights reserved
  !
  !********************************************************************
  !
  ! Module:       FEEDER_BASE.sys
  !
  ! Description:  Software to implement on FlexFeeder
  !
  ! Version:      See below data definitions
  !
  ! History:      Version  Author         Date        Reason
  !               -------------------------------------------
  !               1.0      Willi Tyroke   2016-08-09  Created
  !               1.01     Taylor Hornung 2017-04-24  Added error handling for no communication with feeder
  !               1.02     Willi Tyroke   2016-05-03  Adapting error handling by using non instaled FlexFeeder
  !
  !*********************************************************************
  !
  !TODO: TOP
  !Version information
  CONST string stVerFeederBase{2}:=["V 1.02","2017-05-03"];
  !
  !###################################################################
  !
  !                  D A T A  -  D E C L A R A T I O N S
  !
  !###################################################################
  !
  !**********************************************************
  !*                   Boolean variables                    *
  !**********************************************************
  !Flag to enable FeederCycle
  PERS bool bFeederCycle{4};
  !Flag to enable FeederLoad
  PERS bool bFeederLoad{4};
  !Flag to enable FeederUnload
  PERS bool bFeederUnload{4};
  !Flag to enable FeederScreenShake
  PERS bool bFeederScreenShake{4};
  !Flag to lock that feeder is running  
  PERS bool bFeederRunning{4};
  !
  !**********************************************************
  !*                  signaldo declarations                 *
  !**********************************************************
  !Virtual digital output Signals of the feeder
  VAR signaldo doReady;
  VAR signaldo doWorkAreaFree;
  VAR signaldo doTaskRun;
  !
  !**********************************************************
  !*                  string declarations                   *
  !**********************************************************
  !Message on TASK Windows. Operator interaction
  CONST string stSignalMissing{5}:=["The selected feeder is not installed!"," ","Please control your feeder selection!","",""];
  CONST string stFeederSelection{5}:=["By using the Feeder instruction,","the feeder selection is missing/wrong!"," ","Please select F1, F2, F3 or F4!",""];
  CONST string stFeederJob{5}:=["By using the Feeder instruction,","the feeder job is missing!"," ","Please select Cycle, ScreenShake,","Load, Unload or Ready!"];
  CONST string stFeederTaskStop{5}:=["The feeder TASK is not running!"," ","Signal Feeder TASK run is 0!"," ","Please start feeder TASK!"];
  CONST string stFeederWorkArea{5}:=["Feeder working area blocked!","Make sure that the robot is outside","of feeder working area!"," ","You can confirm this from the GUI!"];
  CONST string stFeederReady{5}:=["Waiting on feedback from Feeder!"," ","Signal Feeder Ready is 0!"," ","Please read Error Massage at Feeder GUI!"];
  CONST string stFeederLoad{5}:=["Feeder ready to Load!"," ","Please confirm when the feeder","loading process is finished!",""];
  CONST string stFeederUnload{5}:=["Feeder ready to Unload!"," ","Please confirm when the feeder","unloading process is finished!",""];
  !
  !**********************************************************
  !*                Button result variables                 *
  !**********************************************************
  !Button result for UIMessageBox
  VAR btnres resOPAnswer;
  !
  !**********************************************************
  !*                  errnum declarations                   *
  !**********************************************************
  VAR errnum ERR_FEEDER_SELECTION:=-1;
  VAR errnum ERR_FEEDER_JOB:=2;
  VAR errnum ERR_NO_FEEDER:=-1;
  !
  !###################################################################
  !
  !                        ROUTINES
  !
  !###################################################################

  !**********************************************************
  !*  Procedure Feeder                                      *
  !*                                                        *
  !*  Description:                                          *
  !*                                                        *
  !*  First step: select FlexFeeder -> F1, F2, F3 or F4     *
  !*  Second step: select feeder job -> Cycle,              *
  !*                                    ScreenShake,        *
  !*                                    Load, Unload,       *
  !*                                    or Ready.           *
  !*  Third step: optional selection -> noWait              *
  !*                                                        *
  !*  Date:          Version:    Programmer:       Reason:  *
  !*  09.08.2016       1.0       W. Tyroke         created  *
  !*  24.04.2017       1.01      T. Hornung        adapted  * 
  !*  03.05.2017       1.02      W. Tyroke   IO_UNCONFIGURED*
  !**********************************************************
  PROC Feeder(\switch F1|switch F2|switch F3|switch F4,\switch Cycle|switch ScreenShake|switch Load|switch Unload|switch Ready,\switch noWait)
    VAR string stHeader;
    VAR string stIOName;
    VAR num nFeederNum;
    BookErrNo ERR_NO_FEEDER;
    BookErrNo ERR_FEEDER_SELECTION;

    !Set Feeder number
    IF (NOT Present(F1)) AND (NOT Present(F2)) AND (NOT Present(F3)) AND (NOT Present(F4)) RAISE ERR_FEEDER_SELECTION;
    IF Present(F1) nFeederNum:=1;
    IF Present(F2) nFeederNum:=2;
    IF Present(F3) nFeederNum:=3;
    IF Present(F4) nFeederNum:=4;
    !Check if feeder is configured
    IF (IOUnitState("Feeder"+NumToStr(nFeederNum,0)+"_Device"\Phys)=IOUNIT_PHYS_STATE_UNCONFIGURED) THEN
      RAISE ERR_FEEDER_SELECTION;
    ENDIF
    !Check if feeder is connected
    IF NOT (IOUnitState("Feeder"+NumToStr(nFeederNum,0)+"_Device"\Phys)=IOUNIT_PHYS_STATE_RUNNING) THEN
      ErrRaise "ERR_NO_FEEDER",4800,"No Connection to FlexFeeder",
               "Feeder"+NumToStr(nFeederNum,0)+"_Device is not running",
               " ",
               "Actions: Check that Feeder"+NumToStr(nFeederNum,0)+" has the correct address, is connected and running",
               "Recovery: Handle ERR_NO_FEEDER in your error handler";
    ENDIF
    !Start feeder job
    IF (NOT Present(Cycle)) AND (NOT Present(ScreenShake)) AND (NOT Present(Load)) AND (NOT Present(Unload)) AND (NOT Present(Ready)) RAISE ERR_FEEDER_JOB;
    IF Present(Cycle) bFeederCycle{nFeederNum}:=TRUE;
    IF Present(ScreenShake) bFeederScreenShake{nFeederNum}:=TRUE;
    IF Present(Load) bFeederLoad{nFeederNum}:=TRUE;
    IF Present(Unload) bFeederUnload{nFeederNum}:=TRUE;
    !
    !Create message header
    stHeader:="Feeder "+NumToStr(nFeederNum,0)+":";
    !Connect alias with virtual digital output
    stIOName:="doFeeder"+NumToStr(nFeederNum,0)+"_Ready";
    AliasIO stIOName,doReady;
    stIOName:="doFeeder"+NumToStr(nFeederNum,0)+"_WorkAreaFree";
    AliasIO stIOName,doWorkAreaFree;
    stIOName:="doFeeder"+NumToStr(nFeederNum,0)+"_TaskRun";
    AliasIO stIOName,doTaskRun;
    IF (NOT Present(Ready)) SetDO doReady,0;
    !Check if selected feeder is running!
    WHILE doTaskRun=0 DO
      resOPAnswer:=UIMessageBox(\Header:=stHeader\MsgArray:=stFeederTaskStop\Buttons:=btnRetryCancel\Icon:=iconError\Image:="FlexFeeder.jpg"\DOBreak:=doTaskRun);
      IF resOPAnswer=resCancel RETURN ;
    ENDWHILE
    IF Present(Cycle) OR Present(ScreenShake) WaitDO doWorkAreaFree,1\Visualize\Header:=stHeader\MsgArray:=stFeederWorkArea\Icon:=iconInfo\Image:="FlexFeeder.jpg";
    !If noWait was selected skip the control. (The combination Ready and noWait is not allowed!) 
    IF (NOT Present(noWait)) OR Present(Ready) OR Present(Load) OR Present(Unload) THEN
      !If feeder load or unload show massage and wait for operator action
      IF Present(Load) THEN
        resOPAnswer:=UIMessageBox(\Header:=stHeader\MsgArray:=stFeederLoad\Buttons:=btnOK\Icon:=iconInfo\Image:="FlexFeeder.jpg");
        bFeederLoad{nFeederNum}:=FALSE;
      ELSEIF Present(Unload) THEN
        resOPAnswer:=UIMessageBox(\Header:=stHeader\MsgArray:=stFeederUnload\Buttons:=btnOK\Icon:=iconInfo\Image:="FlexFeeder.jpg");
        bFeederUnload{nFeederNum}:=FALSE;
      ENDIF
      !Wait for feedback from Feeder
      WaitDO doReady,1\Visualize\Header:=stHeader\MsgArray:=stFeederReady\Icon:=iconInfo\Image:="FlexFeeder.jpg"\VisualizeTime:=20;
    ENDIF
    !Reset Alais conection
    AliasIOReset doReady;
    AliasIOReset doWorkAreaFree;
    AliasIOReset doTaskRun;
    !
  ERROR
    IF ERRNO=ERR_FEEDER_SELECTION OR ERRNO=ERR_NAME_INVALID THEN
      resOPAnswer:=UIMessageBox(\Header:="Feeder selection?"\MsgArray:=stFeederSelection\Buttons:=btnOK\Icon:=iconError\Image:="FlexFeeder.jpg");
    ELSEIF ERRNO=ERR_FEEDER_JOB THEN
      resOPAnswer:=UIMessageBox(\Header:="Feeder job?"\MsgArray:=stFeederJob\Buttons:=btnOK\Icon:=iconError\Image:="FlexFeeder.jpg");
    ELSEIF ERRNO=ERR_ALIASIO_DEF THEN
      resOPAnswer:=UIMessageBox(\Header:="Missing "+stHeader\MsgArray:=stSignalMissing\Buttons:=btnOK\Icon:=iconError\Image:="FlexFeeder.jpg");
    ELSEIF ERRNO=ERR_TP_DOBREAK THEN
      TRYNEXT;
    ELSEIF ERRNO=ERR_NO_FEEDER THEN
      RAISE ;
    ENDIF
    Stop;
    RETURN ;
  ENDPROC
ENDMODULE