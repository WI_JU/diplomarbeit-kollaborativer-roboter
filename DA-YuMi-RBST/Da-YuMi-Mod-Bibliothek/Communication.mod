MODULE Communication
    !Socket Declatration
    VAR socketdev serverSocket;
    VAR socketdev clientSocket;
    VAR string message;
    VAR rawbytes data;    
    VAR string ip := "127.0.0.1";
    VAR num port := 55000;
    
    
!    PROC communicate()
!        TPWrite "Communication started";
!    ! Create Communication
!        SocketCreate serverSocket;
!        SocketBind serverSocket, ip, port;
!        SocketListen serverSocket;
!        SocketAccept serverSocket,clientSocket;
!    ! Send a message to the client
!        SocketSend clientSocket,\Str:="Hello Client";
        
!    !Receive a message from the client
    
!        SocketReceive clientSocket,\RawData:=data;
!        UnpackRawBytes data,1,message\ASCII:=15;
!        TPWrite message;
!    !Close Communication
!        SocketClose serverSocket;
!        TPWrite "Communication connection close";
!    ENDPROC
    
    PROC communicate()
        TPWrite "Communication started";
    ! Create Communication
        serverCreate;
    ! Send a message to the client
        SocketSend clientSocket,\Str:="Hello Client";
        
    !Receive a message from the client
    
        SocketReceive clientSocket,\Str:=message;
        
        TPWrite message;
    !Close Communication
        SocketClose clientSocket;
        SocketClose serverSocket;
        
        TPWrite "Communication connection close";
        
        ERROR
        
        IF ERRNO = ERR_SOCK_TIMEOUT THEN
            RETRY;
        ELSEIF ERRNO = ERR_SOCK_Closed THEN
            serverCreate;
            RETRY;
        ELSE
            stop;
        ENDIF
    ENDPROC
    
    PROC serverCreate()
        SocketCreate serverSocket;
        SocketBind serverSocket, ip, port;
        SocketListen serverSocket;
        SocketAccept serverSocket,clientSocket,\Time:=WAIT_MAX;    
    ENDPROC
      
ENDMODULE