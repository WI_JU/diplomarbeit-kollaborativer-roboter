MODULE RCS_Communication
    !Socket Declatration
    VAR socketdev RCSserverSocket;
    VAR socketdev RCSclientSocket;  
    VAR string virtuellIP := "127.0.0.1";
    VAR num virtuellPORT := 55000;
    CONST string VALID_RESPOND := "VALID";
    CONST string INVALID_RESPOND := "INVALID";
    CONST string FINISHED_SEQUENCE := "FINISH";
    CONST string handleCases{4} := ["GetSolvedCube", "CloseConnection", "KameraPosition","NextStep"];
    
    !solved Array (dynamic)
    CONST num maxarraysize := 500;
    VAR string solvedArray{500};
    VAR num currentarraysize:=0;
    
    PROC RCSserverCreate()
        SocketCreate RCSserverSocket;
        SocketBind RCSserverSocket, virtuellIP, virtuellPORT;
        SocketListen RCSserverSocket;
        SocketAccept RCSserverSocket,RCSclientSocket,\Time:=WAIT_MAX;    
    ENDPROC
    
    
    PROC RCS_startCommunication()
        
        VAR string respond;
        TPWrite "Communication started";
    ! Create Communication
        RCSserverCreate;
        
    ! Send Kamnerapositions
        TPWrite "Camerahandling activated";
        FOR i FROM 1 TO 6 DO
            RCSsend(handleCases{3}+","+NumToStr(i,0));
        ENDFOR
    
    !Ready for solve Algrotythm
        TPWrite "SolvedArrayhandling activated";
        RCSgetSolvedArrays;
        
    !Send Close Communication 
        TPWrite "Close Connection activated";
        RCSsend(handleCases{2});
        
    !Close Communication
        SocketClose RCSclientSocket;
        SocketClose RCSserverSocket;
        
        TPWrite "Communication connection close";
        
        ERROR
        
        IF ERRNO = ERR_SOCK_TIMEOUT THEN
            RETRY;
        ELSEIF ERRNO = ERR_SOCK_Closed THEN
            RCSserverCreate;
            RETRY;
        ELSE
            stop;
        ENDIF
    ENDPROC

    !Sends to CLIENT and checkt if respond is truthy 
    Proc RCSsend(string message)
        VAR string respond;
        SocketSend RCSclientSocket,\Str:=message;
        waitTime 0.05;
        SocketReceive RCSclientSocket,\Str:=respond;
        TPWrite respond;
    ENDPROC
    
    PROC RCSgetSolvedArrays()
        
        RCSsend(handleCases{1});
        ! get solved Array
        revieveSteps;        
    ENDPROC
    
    PROC revieveSteps()
        VAR string respond;
        VAR num index;
        VAR bool stopHandler := FALSE;
        clearSolvedArray;
        
        waitTime 0.05;
        WHILE not stopHandler DO
            SocketSend RCSclientSocket,\Str:=handleCases{4}; 
            waitTime 0.05;
            SocketReceive RCSclientSocket,\Str:=respond;
            IF respond = FINISHED_SEQUENCE THEN
                stopHandler := TRUE;
            ELSE
                currentarraysize:=currentarraysize+1;
                solvedArray{currentarraysize}:=respond;
            ENDIF
        ENDWHILE                
    ENDPROC

    PROC clearSolvedArray()
        currentarraysize := 0;
        FOR i FROM 1 TO maxarraysize DO
            solvedArray{i} := "";
        ENDFOR
    ENDPROC
ENDMODULE