
import socket
from time import sleep

# configure socket and connect to server
clientSocket = socket.socket()
# port = 1025
port = 55000
# ipAddress = "192.168.1.54"
ipAddress = '127.0.0.1'

# keep track of connection status
connected = False

while True:
    # attempt to send and receive wave, otherwise reconnect
    try:
        message = clientSocket.recv(1024).decode("UTF-8")
        clientSocket.send(bytes("Hello World", "UTF-8"))
        print(message)
    except socket.error:
        # set connection status and recreate socket
        connected = False
        clientSocket = socket.socket()
        print("connection lost... reconnecting")
        while not connected:
            # attempt to reconnect, otherwise sleep for 2 seconds
            try:
                clientSocket.connect((ipAddress, port))
                connected = True
                print("re-connection successful")
            except socket.error:
                sleep(2)

clientSocket.close();