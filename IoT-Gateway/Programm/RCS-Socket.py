import socket
from time import sleep


class Communication:
    """ starts the communication with the Roboter if the MOD is active"""
    # configure socket and connect to server
    clientSocket = socket.socket()
    ipAddress = "192.168.125.1"
    port = 5516
    # ipAddress = '127.0.0.1'
    # port = 55000

    # keep track of connection status
    connected = False
    handleCases = ["GetSolvedCube", "CloseConnection", "CameraPosition", "NextStep"]
    VALID_CASE = "VALID"
    INVALID_CASE = "INVALID"
    FINISHED_SEQUENCE = "FINISH"

    def communicate(self):
        while True:
            # attempt to send and receive wave, otherwise reconnect
            try:
                msg = self.clientSocket.recv(1024).decode("UTF-8")
                if msg != "":
                    self._inputHandler(msg)
            except socket.error:
                # set connection status and recreate socket
                self.connected = False
                self.clientSocket = socket.socket()
                print("\n\nconnection lost... reconnecting")
                while not self.connected:
                    # attempt to reconnect, otherwise sleep for 2 seconds
                    try:
                        self.clientSocket.connect((self.ipAddress, self.port))
                        self.connected = True
                        print("re-connection successful")
                    except socket.error:
                        sleep(2)

    def _inputHandler(self, inputMessage):
        print(f"Received: {inputMessage}")
        if inputMessage == self.handleCases[0]:
            self._solveHandler()

        elif inputMessage == self.handleCases[1]:
            self._clientSend(self.VALID_CASE)
            self.clientSocket.close()

        elif inputMessage == self.handleCases[3]:
            if self.activateSolvedStates and self.currentStepIndex < len(self.solvedArray):
                self._clientSend(str(self.solvedArray[self.currentStepIndex]))
                self.currentStepIndex += 1
            elif self.activateSolvedStates and self.currentStepIndex >= len(self.solvedArray):
                self._clientSend(self.FINISHED_SEQUENCE)
        else:
            inputMessage = inputMessage.split(",")
            useCase = inputMessage[0]
            if useCase == self.handleCases[2]:
                self._cameraHandler(inputMessage)
            else:
                print("Unknown Case")
        return

    def _cameraHandler(self, inputMessage):
        invalid = False
        number = int(inputMessage[1])
        if number == 1:
            print("Safe Bild 1")
        elif number == 2:
            print("Safe Bild 2")
        elif number == 3:
            print("Safe Bild 3")
        elif number == 4:
            print("Safe Bild 4")
        elif number == 5:
            print("Safe Bild 5")
        elif number == 6:
            print("Safe Bild 6")
        else:
            invalid = True
            self._clientSend(self.INVALID_CASE)
        if not invalid:
            self._clientSend(self.VALID_CASE)

    def _solveHandler(self):
        print("generate Array")
        print("generate Solved Array")
        print("send solved Array")
        self.solvedArray = ["U", "Ui", "L", "Li", "F", "Fi", "R", "Ri", "B", "Bi", "D", "Di"]
        self.activateSolvedStates = True
        self.currentStepIndex = 0
        self._clientSend(self.VALID_CASE)

    def _clientSend(self, sendMessage):
        print(f"Sent: {sendMessage}")
        self.clientSocket.send(bytes(sendMessage, "UTF-8"))


CM = Communication()
CM.communicate()
