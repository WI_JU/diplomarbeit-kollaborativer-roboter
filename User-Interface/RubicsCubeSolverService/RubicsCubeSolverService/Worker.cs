namespace RubicsCubeSolverService;

public class Worker : BackgroundService
{
    private readonly ILogger<Worker> _logger;
    private List<string> Urls = new List<string>() {"httpps://google.com"};
    
    
    public Worker(ILogger<Worker> logger)
    {
        _logger = logger;
    }

    protected override async Task ExecuteAsync(CancellationToken stoppingToken)
    {
        while (!stoppingToken.IsCancellationRequested)
        {
            try
            {
                await PollUrls();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "An error occured whiel polling urls");
            }
            finally
            {
                
            }
        }
    }

    private async Task PollUrls()
    {
        List<Task> tasks = new List<Task>();
        foreach (var url in Urls)
        {
                
        }
    }
}